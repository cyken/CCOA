﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.FixAss.EventEntity
{
    public class FixBuyFEE : BP.WF.FlowEventBase
    {
        public override string FlowMark
        {
            get { return "FixBuy"; }
        }

        public override string FlowOverAfter()
        {
            BP.OA.FixAss.API.Fix_Buy(this.GetValStr("WPMC"), this.GetValStr("CGSQDH"), this.GetValStr("GGXH"), this.GetValInt("GMSL"), this.GetValStr("GMDJ"), this.GetValStr("CGR"), this.GetValStr("CGBM"), this.GetValStr("CGRQ"), this.GetValStr("GYS"), this.GetValStr("HTBH"), this.GetValStr("BZ"), this.GetValStr("GLY_Checker"));
            return "写入成功...";
        }
        public override string AfterFlowDel()
        {
            return null;
        }
        public override string BeforeFlowDel()
        {
            return null;
        }
        public override string FlowOverBefore()
        {
            return null;
        }
    }
}
