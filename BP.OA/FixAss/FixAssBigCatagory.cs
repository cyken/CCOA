using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.FixAss
{
    /// <summary>
    /// 固定资产类别属性
    /// </summary>
    public class FixAssBigCatagoryAttr : EntityNoNameAttr
    {
    }
    /// <summary>
    /// 固定资产类别
    /// </summary>
    public class FixAssBigCatagory : EntityNoName
    {
        #region 属性
        #endregion 属性

        #region 权限控制属性.
        #endregion 权限控制属性.

        #region 构造方法
        public FixAssBigCatagory()
        {
        }
        public FixAssBigCatagory(string _No) : base(_No) { }
        #endregion
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// 类别Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_FixAssBigCatagory");
                
                map.EnDesc = "所属大类";

                map.CodeStruct = "3";
                map.IsAutoGenerNo = true;

                map.AddTBStringPK(FixAssBigCatagoryAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(FixAssBigCatagoryAttr.Name, null, "类别名称", true, false, 1, 100, 30, true);
              
                this._enMap = map;
                return this._enMap;
            }
        }

    }
    /// <summary>
    /// 固定资产类别s
    /// </summary>
    public class FixAssBigCatagorys : EntitiesNoName
    {
         /// <summary>
        /// 类别
        /// </summary>
        public FixAssBigCatagorys() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new FixAssBigCatagory();
            }
        }
    }
}
