using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA.Market
{
    public enum FP { 
        yes = 1,
        no = 0
    }
    public enum TBZT {
        a1 = 1,
        a2 = 2,
        a3 = 3
    }
    /// <summary>
    /// 投标
    /// </summary>
    public class TenderInfoAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 项目名称
        /// </summary>
        public const string FK_Xmmc = "FK_Xmmc";
        /// <summary>
        /// 项目类型
        /// </summary>
        public const string FK_Xmlx = "FK_Xmlx";
        /// <summary>
        /// 单位名称
        /// </summary>
        public const string FK_Dwmc = "FK_Dwmc";
        /// <summary>
        /// 投标号
        /// </summary>
        public const string Tbh = "Tbh";
        /// <summary>
        /// 开标日期
        /// </summary>
        public const string Kbrq = "Kbrq";
        /// <summary>
        /// 中标日期
        /// </summary>
        public const string Zbrq = "Zbrq";
        /// <summary>
        /// 投标状态
        /// </summary>
        public const string TBZT = "TBZT";
        /// <summary>
        /// 保证金
        /// </summary>
        public const string Bzj = "Bzj";
        /// <summary>
        /// 服务费
        /// </summary>
        public const string Fwf = "Fwf";
        /// <summary>
        /// 经办人
        /// </summary>
        public const string Jbr = "Jbr";
        /// <summary>
        /// 是否开发票
        /// </summary>
        public const string FP = "FP";
        /// <summary>
        /// 收款人
        /// </summary>
        public const string Skr = "Skr";
        /// <summary>
        /// 退保形式
        /// </summary>
        public const string Tbxs = "Tbxs";
        /// <summary>
        /// 退保金额
        /// </summary>
        public const string Tbje = "Tbje";
        /// <summary>
        /// 退保日期
        /// </summary>
        public const string Tbrq = "Tbrq";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Bz = "Bz"; 
    }
    /// <summary>
    ///  投标实体类
    /// </summary>
    public class TenderInfo : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetValStrByKey(TenderInfoAttr.Title);
            }
            set
            {
                this.SetValByKey(TenderInfoAttr.Title, value);
            }
        }
     
        #endregion 属性

        #region 权限控制属性.
        
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public TenderInfo()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public TenderInfo(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_TenderInfo");
                map.EnDesc = "投标登记与查询";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //四位编号0001开始

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBStringPK(TenderInfoAttr.No, null, "编号", false, true, 3, 3, 3);
                map.AddTBString(TenderInfoAttr.Name,null,"投标名称",true,false,0,100,30);
                map.AddTBString(TenderInfoAttr.Tbh,null,"投标号",true,false,0,10,3);
                map.AddDDLEntities(TenderInfoAttr.FK_Xmmc, null, "项目名称", new Projects(), true);
                map.AddDDLEntities(TenderInfoAttr.FK_Dwmc, null, "单位名称", new Units(), true);
                map.AddTBDate(TenderInfoAttr.Kbrq, null, "开标日期", true, false);
                map.AddTBDate(TenderInfoAttr.Zbrq, null, "中标日期", true, false);
                map.AddDDLSysEnum(TenderInfoAttr.TBZT, 0, "投标状态", true, true, TenderInfoAttr.TBZT, "@1=投标@2=中标@3=未中标");
                map.AddTBMoney(TenderInfoAttr.Bzj, 0, "保证金(人民币)", true, false);
                map.AddTBMoney(TenderInfoAttr.Fwf, 0, "服务费(人民币)", true, false);
                map.AddTBString(TenderInfoAttr.Jbr, null, "经办人", true, false, 0, 10, 3);
                map.AddDDLSysEnum(TenderInfoAttr.FP, 0, "是否开发票", true, true, TenderInfoAttr.FP,"@1=是@0=否");
                map.AddTBString(TenderInfoAttr.Skr, null, "收款人", true, false, 0, 10, 3);
                map.AddTBString(TenderInfoAttr.Tbxs, null, "退保形式", true, false, 0,10, 3);
                map.AddTBString(TenderInfoAttr.Tbje, null, "退保金额", true, false, 0, 10, 3);
                map.AddTBDate(TenderInfoAttr.Tbrq, null, "退保日期", true, false);
                map.AddTBStringDoc(TenderInfoAttr.Bz, null, "备注", true, false,true);
                this._enMap = map;
                return this._enMap;
            }
        }

    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class TenderInfos : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public TenderInfos() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new TenderInfo();
            }
        }
    }
}
