using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA.Market
{
    /// <summary>
    /// 项目登记与维护
    /// </summary>
    public class ProjectWeihuAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 项目名称
        /// </summary>
        public const string Xmmc = "Xmmc";
        /// <summary>
        /// 项目类型
        /// </summary>
        public const string XMLX = "XMLX";
        /// <summary>
        /// 项目编号
        /// </summary>
        public const string Xmbh = "Xmbh";
        /// <summary>
        /// 立项时间
        /// </summary>
        public const string Lxsj = "Lxsj";
        /// <summary>
        /// 所属行业
        /// </summary>
        public const string SSHY = "SSHY";
        /// <summary>
        /// 所在省份
        /// </summary>
        public const string Szsf = "Szsf";
        /// <summary>
        /// 所在城市
        /// </summary>
        public const string Szcs = "Szcs";
        /// <summary>
        /// 客户名称
        /// </summary>
        public const string Khmc = "Khmc";
        /// <summary>
        /// 工程类别
        /// </summary>
        public const string GCLB = "GCLB";
        /// <summary>
        /// 工程规模
        /// </summary>
        public const string Gcgm = "Gcgm";
        /// <summary>
        /// 建设性质
        /// </summary>
        public const string JSXZ = "JSXZ";
        /// <summary>
        /// 设总
        /// </summary>
        public const string Sz = "Sz";
        /// <summary>
        /// 投标情况
        /// </summary>
        public const string Tbqk = "Tbqk";
        /// <summary>
        /// 项目状态
        /// </summary>
        public const string XMZT = "XMZT";
        /// <summary>
        /// 研发项目
        /// </summary>
        public const string YFXM = "YFXM";
        /// <summary>
        /// 专利证书
        /// </summary>
        public const string Zlzs = "Zlzs";
        /// <summary>
        /// 认定号
        /// </summary>
        public const string Rdh = "Rdh";


    }
    /// <summary>
    ///  投标实体类
    /// </summary>
    public class ProjectWeihu : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetValStrByKey(ProjectWeihuAttr.Title);
            }
            set
            {
                this.SetValByKey(ProjectWeihuAttr.Title, value);
            }
        }
     
        #endregion 属性

        #region 权限控制属性.
        
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public ProjectWeihu()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public ProjectWeihu(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_ProjectWeihu");
                map.EnDesc = "投标登记与查询";
                map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //四位编号0001开始
                map.AddTBStringPK(ProjectWeihuAttr.No, null, "项目名称", false, true, 3, 3, 3);
                map.AddDDLSysEnum(ProjectWeihuAttr.XMLX, 0, "项目类型",true,true, ProjectWeihuAttr.XMLX, "@0=1");
                map.AddTBString(ProjectWeihuAttr.Xmbh,null,"项目编号",true,false,0,10,3);
                map.AddTBDate(ProjectWeihuAttr.Lxsj,null,"立项时间",true,false);
                map.AddDDLSysEnum(ProjectWeihuAttr.SSHY, 0, "所属行业", true, true, ProjectWeihuAttr.SSHY, "@1=软件1@2=软件2@3=软件3");
                map.AddDDLEntities(ProjectWeihuAttr.Szsf,null,"所在省份",new Projects(),true);
                map.AddDDLEntities(ProjectWeihuAttr.Szcs, null, "所在城市", new Projects(), true);
                map.AddTBString(ProjectWeihuAttr.Khmc, null, "客户名称", false, true, 0, 100, 3); ;
                map.AddDDLSysEnum(ProjectWeihuAttr.GCLB, 1, "工程类别", true, true, ProjectWeihuAttr.GCLB, "@1=总包@2=设计@3=运营");
                map.AddTBString(ProjectWeihuAttr.Gcgm, null, "工程规模", true, false,0,100,3);
                map.AddDDLSysEnum(ProjectWeihuAttr.JSXZ, 1, "建设性质", true, true, ProjectWeihuAttr.Gcgm, "@1=矿井@2=选煤@3=矿井总体");
                map.AddTBString(ProjectWeihuAttr.Sz, null, "设总", true, false,0,100,3);
                map.AddDDLSysEnum(ProjectWeihuAttr.XMZT, 1, "项目状态", true, true, ProjectWeihuAttr.Gcgm, "@1=矿井@2=选煤@3=矿井总体");
                map.AddDDLSysEnum(ProjectWeihuAttr.YFXM, 2, "研发项目", true, true, ProjectWeihuAttr.Gcgm, "@1=是@2=否");
                map.AddTBString(ProjectWeihuAttr.Zlzs, null, "专利证书", true, false,0,100,3);
                map.AddTBString(ProjectWeihuAttr.Rdh, null, "认定号", true, false,0,100,3);
                
                this._enMap = map;
                return this._enMap;
            }
        }
       
    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class ProjectWeihus : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public ProjectWeihus() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ProjectWeihu();
            }
        }
    }
}
