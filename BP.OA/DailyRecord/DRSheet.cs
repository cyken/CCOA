using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 日志状态
    /// </summary>
    public enum DRSta
    {
        /// <summary>
        /// 私有的
        /// </summary>
        Private,
        /// <summary>
        /// 共享
        /// </summary>
        Share,
        /// <summary>
        /// 公开的
        /// </summary>
        Public
    }

    public enum DRShe
    {
        /// <summary>
        /// 月总结
        /// </summary>
        Mouth,
        /// <summary>
        /// 周总结
        /// </summary>
        Week,
        /// <summary>
        /// 年度总结
        /// </summary>
        Year
    }
    /// <summary>
    /// 日志薄 属性
    /// </summary>
    public class DRSheetAttr :EntityOIDAttr
    {
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 共享类型（0=私有，1=共享, 2=公开的）
        /// </summary>
        public const string DRSta = "DRSta";
        /// <summary>
        /// 日志类型（周总结，月总结）
        /// </summary>
        public const string DRShe = "DRShe";
        /// <summary>
        /// 日志内容
        /// </summary>
        public const string Doc = "Doc";
       
        /// 记录人
        /// </summary>
        public const string Rec = "Rec";
      
        /// <summary>
        /// 记录时间
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 修改时间
        /// </summary>
        public const string EDT = "EDT";
    }
    /// <summary>
    /// 日志薄
    /// </summary>
    public partial class DRSheet : EntityOID
    {
        #region 属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.Title);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.Title, value);
            }
        }
        /// <summary>
        /// 类型
        /// </summary>
        public DRSta DRSta
        {
            get
            {
                return (DRSta)this.GetValIntByKey(DRSheetAttr.DRSta);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.DRSta, (int)value);
            }
        }
       /// <summary>
       /// 日志类型
       /// </summary>
        public DRShe DRShe
        {
            get
            {
                return (DRShe)this.GetValIntByKey(DRSheetAttr.DRShe);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.DRShe, (int)value);
            }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Doc
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.Doc);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.Doc, value);
            }
        }
       
        /// <summary>
        /// 记录人
        /// </summary>
        public string Rec
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.Rec);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.Rec, value);
            }
        }
       
        /// <summary>
        /// 记录时间
        /// </summary>
        public string RDT
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.RDT);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.RDT, value);
            }
        }
        /// <summary>
        /// 编辑时间
        /// </summary>
        public string EDT
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.EDT);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.EDT, value);
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 日志薄
        /// </summary>
        public DRSheet() { }
        /// <summary>
        /// 日志薄
        /// </summary>
        /// <param name="no">编号</param>
        public DRSheet(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();                 
                map.PhysicsTable = "OA_DRSheet"; //物理表名
                map.EnDesc = "日志薄";   // 实体的描述.

                //基本信息
                map.AddTBIntPKOID();

                map.AddTBString(DRSheetAttr.Title, null, "标题", true, false, 0, 300, 30,true);
                map.AddDDLSysEnum(DRSheetAttr.DRSta, 0, "共享类型", true, false,
                    DRSheetAttr.DRSta, "@0=私有@1=共享@2=公开");
                map.AddDDLSysEnum(DRSheetAttr.DRShe, 0, "日志类型", true, false,
                  DRSheetAttr.DRShe, "@3=周总结@4=月总结@5=年度总结");

                map.AddTBStringDoc(DRSheetAttr.Doc, null, "内容", true, false, true);
                //map.AddTBString(DRSheetAttr.Doc, null, "标题", true, false, 0,0, 10000, true);
           
                map.AddTBString(DRSheetAttr.Rec, null, "记录人", true, false, 0, 100, 30);
                map.AddTBDateTime(DRSheetAttr.RDT, null, "记录时间", true, false);
                map.AddTBDateTime(DRSheetAttr.EDT, null, "编辑时间", true, false); 
                map.DTSearchKey = DRSheetAttr.RDT;
                map.DTSearchWay = Sys.DTSearchWay.ByDate;


                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        protected override bool beforeInsert()
        {
            this.Rec = WebUser.No;
            this.RDT = DataType.CurrentDataTime;
            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {
            this.EDT = DataType.CurrentDataTime;
            return base.beforeUpdate();
        }
        
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class DRSheets : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DRSheet();
            }
        }
        /// <summary>
        /// 日志薄集合
        /// </summary>
        public DRSheets()
        {
        }
    }
}
