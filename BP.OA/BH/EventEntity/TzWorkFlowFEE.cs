﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.BH.EventEntity
{
    /// <summary>
    /// 台账业务流转 流程事件实体
    /// </summary>
    public class TzWorkFlowFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 台账业务流转 流程事件实体
        /// </summary>
        public TzWorkFlowFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "TzWorkFlow"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            //把台账信息写入api.
            API.TzWorkFlow(
                this.GetValStr("SJBH"),
               this.GetValStr("Port_Dept"),
               this.GetValStr("PCRXM"),
               this.GetValStr("Port_Dept1"),
               this.GetValStr("PCRDH"),
               this.GetValStr("PaiChaShiJian"),
               this.GetValStr("WTMS"),
                this.GetValStr("TaiZhangLeiXing")           
               );
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
