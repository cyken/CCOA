﻿using BP.Pub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.RCSWGL
{
    /// <summary>
    /// 后勤设施维修
    /// </summary>
   public class API
    {
        #region 后勤维修管理 api.
        /// <summary>
        /// 后勤维修管理流程是否启用?
        /// </summary>
        public static bool RCSWGL_HQGL_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("RCSWGL_HQGL_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 后勤维修管理 流程标记
        /// </summary>
        public static string RCSWGL_HQGL_FlowMark
        {
            get
            {
                return "RCSWGLHQGL";
            }
        }

        /// <summary>
        /// 后勤管理
        /// </summary>
        /// <param name="LB">类别</param>
        /// <param name="SBR">上报人</param>
        /// <param name="SBDate">上报日期</param>
        /// <param name="YCQK">异常情况</param>
        /// <param name="Place">地点</param>
        /// <param name="Num">单号</param>
        /// <param name="DJDate">登记日期</param>
        /// <param name="HQSName">后勤名称</param>
        /// <param name="HQSNo">后勤编号</param>
        /// <param name="WXFZR">维修负责人</param>
        /// <param name="YY">原因</param>
        /// <param name="FZHF">责任划分</param>
        /// <param name="CLYJ">处理意见</param>
        /// <param name="VXSC">维修所需时长</param>
        /// <param name="CSFY">产生费用</param>
        /// <param name="WCDate">完成时间</param>
        /// <param name="CLZT">处理状态</param>
        /// <param name="Note">备注</param>
        /// <param name="SBBZ">上报备注</param>
        /// <param name="workid"></param>
        /// <returns></returns>
        public static string RCSWGL_HQGL(int LB, string SBR, string SBDate, string YCQK,
             string Place, string Num, string DJDate, string HQSName, string HQSNo, string WXFZR,
            string YY, string FZHF, string CLYJ, decimal VXSC, decimal CSFY, string WCDate,
            int CLZT, string Note, string SBBZ, Int64 workid)
        {
            HQGL en = new HQGL();
            try
            {
                en.LB = LB;
                en.CSFY = CSFY;
                en.Num = Num;
                en.SBDate = SBDate;
                en.YCQK = YCQK;
                en.Place = Place;
                en.SBR = SBR;
                en.DJDate = DJDate;
                en.HQSName = HQSName;
                en.HQSNo = HQSNo;
                en.WXFZR = WXFZR;
                en.YY = YY;
                en.FZHF = FZHF;
                en.CLYJ = CLYJ;
                en.VXSC = VXSC;
                en.WCDate = WCDate;
                en.CLZT = CLZT;
                en.Note = Note;
                en.SBBZ = SBBZ;
                
                en.WorkId = workid;
                
                en.Insert();
                //RCSWGLInfo info = new RCSWGLInfo(fk_RCSWGL);
                ////  info.FY_HQGL = nextHQGLDT;
                //info.Update();

                return "后勤维修管理成功...";
            }
            catch (Exception ex)
            {
                en.Delete();
                return "@后勤维修管理失败:" + ex.Message;
            }
        }
        #endregion 采购api.

        #region 票务管理 api.
        /// <summary>
        /// 票务流程是否启用?
        /// </summary>
        public static bool RCSWGL_PWGL_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("RCSWGL_PWGL_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 票务 流程标记
        /// </summary>
        public static string RCSWGL_PWGL_FlowMark
        {
            get
            {
                return "RCSWGLPWGL";
            }
        }

        /// <summary>
        /// 票务管理   api
        /// </summary>
        /// <param name="SQR">申请人</param>
        /// <param name="CCRQ">出差日期</param>
        /// <param name="DPLX">的票类型</param>
        /// <param name="MDD">目的地</param>
        /// <param name="SJLD">上级领导</param>
        /// <param name="DPGLY">的票管理员</param>
        /// <param name="HBH">航班号</param>
        /// <param name="JG">价格</param>
        /// <param name="QFRQ">起飞日期</param>
        /// <param name="DPRQ">订票日期</param>
        /// <param name="BZ">备注</param>
        /// <param name="CZ">操作</param>
        /// <param name="CKLC">查看流程</param>
        /// <param name="workid"></param>
        /// <returns>订票结果 错误抛出异常  </returns>
        public static string RCSWGL_PWGL(string SQR, string CCRQ, int
            DPLX, string MDD, string SJLD, string DPGLY, string HBH,decimal JG,
            string QFRQ, string DPRQ, string BZ, string SSNY,
            Int64 workid)
        {

            PWGL en = new PWGL();
            
            //string guid = Guid.NewGuid().ToString();
            try
            {
                en.Name = SQR;
                en.CHDate = CCRQ;
                en.DPType = DPLX;
                en.MDD = MDD;
                en.SJLD = SJLD;
                en.DPAdmin = DPGLY;
                en.HBH = HBH;
                en.JG = JG;
                en.QFDate = QFRQ;
                en.DPDate = DPRQ;
                en.Note = BZ;
                
                en.WorkId = workid;
                en.FK_NY = DPRQ.Substring(0,7);

                NY ny = new NY();
                ny.No = en.FK_NY;
                ny.Name = en.FK_NY;
                ny.Save();

                en.Insert();

                return "订票成功...";
            }
            catch (Exception ex)
            {
                en.Delete();
                return "@订票失败:" + ex.Message;
            }
        }
        #endregion 采购api.

      }
}
