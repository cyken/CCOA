﻿using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;
using BP.Sys;
using System.Web;
using BP.Tools;

namespace BP.OA
{
    /// <summary>
    /// 通知属性
    /// </summary>
    public class MyNoticeAttr : EntityOIDAttr
    {
        #region 1.基本信息(No,Title,Doc,File,FK_NoticeCategory)
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 内容
        /// </summary>
        public const string Doc = "Doc";
        /// <summary>
        /// 上传文件
        /// </summary>
        public const string AttachFile = "AttachFile";
        /// <summary>
        /// 类别
        /// </summary>
        public const string FK_NoticeCategory = "FK_NoticeCategory";
        #endregion

        #region 2.创建信息(FK_UserNo,RDT,FK_Dept)
        /// <summary>
        /// 创建者
        /// </summary>
        public const string FK_UserNo = "FK_UserNo";
        /// <summary>
        /// 发布日期
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 发布部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 发给部分人
        /// </summary>
        public const string SendToPart = "SendToParts";
        /// <summary>
        /// 发往人员
        /// </summary>
        public const string SendToUsers = "SendToUsers";
        /// <summary>
        /// 发往部门
        /// </summary>
        public const string SendToDepts = "SendToDepts";
        /// <summary>
        /// 发往职位
        /// </summary>
        public const string SendToStations = "SendToStations";
        #endregion

        #region 3.展示设置(SetTop,Importance)
        /// <summary>
        /// 置顶时间
        /// </summary>
        public const string SetTop = "SetTop";
        /// <summary>
        /// 重要级别
        /// </summary>
        public const string Importance = "Importance";
        #endregion

        #region 4.反馈意见(GetAdvices,AdviceDesc,AdviceItems)
        /// <summary>
        /// 是否接受反馈意见
        /// </summary>
        public const string GetAdvices = "GetAdvices";
        /// <summary>
        /// 反馈意见说明
        /// </summary>
        public const string AdviceDesc = "AdviceDesc";
        /// <summary>
        /// 意见项
        /// </summary>
        public const string AdviceItems = "AdviceItems";
        #endregion

        #region 5.其它信息(NoticeSta,Starttime,Stoptime,SetMes)
        /// <summary>
        /// 状态(自动，开启，关闭)
        /// </summary>
        public const string NoticeSta = "NoticeSta";
        /// <summary>
        /// 开始时间
        /// </summary>
        public const string StartTime = "StartTime";
        /// <summary>
        /// 结束时间
        /// </summary>
        public const string StopTime = "StopTime";
        /// <summary>
        /// 是否设置消息推送
        /// </summary>
        public const string SetMes = "SetMes";
        #endregion
    }
    /// <summary>
    /// 通知
    /// </summary>
    public partial class MyNotice : EntityOID
    {
        #region 1.基本属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get { return this.GetValStrByKey(MyNoticeAttr.Title); }
            set { this.SetValByKey(MyNoticeAttr.Title, value); }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Doc
        {
            get
            {
                try
                {//公告内容不许为空 前提
                    string doc = this.GetValStrByKey(MyNoticeAttr.Doc);
                    if (doc == "" || doc.Length == 0)
                        return this.GetBigTextFromDB("DocFile");
                    else
                        return doc;
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(MyNoticeAttr.Doc, "");
                }
                else
                {
                    this.SetValByKey(MyNoticeAttr.Doc, value);
                }
            }
        }
        /// <summary>
        /// 文件
        /// </summary>
        public string AttachFile
        {
            get { return this.GetValStringByKey(MyNoticeAttr.AttachFile); }
            set { this.SetValByKey(MyNoticeAttr.AttachFile, value); }
        }
        /// <summary>
        /// 类型
        /// </summary>
        public String FK_NoticeCategory
        {
            get { return this.GetValStringByKey(MyNoticeAttr.FK_NoticeCategory); }
            set { this.SetValByKey(MyNoticeAttr.FK_NoticeCategory, value); }
        }

        #endregion

        #region 2.创建信息
        /// <summary>
        /// 用户编号
        /// </summary>
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(MyNoticeAttr.FK_UserNo); }
            set { this.SetValByKey(MyNoticeAttr.FK_UserNo, value); }
        }
        /// <summary>
        /// 发布日期
        /// </summary>
        public DateTime RDT
        {
            get { return this.GetValDateTime(MyNoticeAttr.RDT); }
            set { this.SetValByKey(MyNoticeAttr.RDT, value); }
        }
        /// <summary>
        /// 发布部门
        /// </summary>
        public string FK_Dept
        {
            get { return this.GetValStrByKey(MyNoticeAttr.FK_Dept); }
            set { this.SetValByKey(MyNoticeAttr.FK_Dept, value); }
        }
        /// <summary>
        /// 发往用户
        /// </summary>
        public bool SendToPart
        {
            get { return this.GetValBooleanByKey(MyNoticeAttr.SendToPart); }
            set { this.SetValByKey(MyNoticeAttr.SendToPart, value); }
        }
        /// <summary>
        /// 发往用户
        /// </summary>
        public string SendToUsers
        {
            get { return this.GetValStrByKey(MyNoticeAttr.SendToUsers); }
            set { this.SetValByKey(MyNoticeAttr.SendToUsers, value); }
        }
        /// <summary>
        /// 发往部门
        /// </summary>
        public string SendToDepts
        {
            get { return this.GetValStrByKey(MyNoticeAttr.SendToDepts); }
            set { this.SetValByKey(MyNoticeAttr.SendToDepts, value); }
        }        /// <summary>
        /// 发往职位
        /// </summary>
        public string SendToStations
        {
            get { return this.GetValStrByKey(MyNoticeAttr.SendToStations); }
            set { this.SetValByKey(MyNoticeAttr.SendToStations, value); }
        }
        #endregion

        #region 3.显示设置
        public DateTime SetTop
        {
            get { return this.GetValDateTime(MyNoticeAttr.SetTop); }
            set { this.SetValByKey(MyNoticeAttr.SetTop, value); }
        }
        public int Importance
        {
            get { return this.GetValIntByKey(MyNoticeAttr.Importance); }
            set { this.SetValByKey(MyNoticeAttr.Importance, value); }
        }
        #endregion

        #region 4.反馈设置
        public bool GetAdvices
        {
            get { return this.GetValBooleanByKey(MyNoticeAttr.GetAdvices); }
            set { this.SetValByKey(MyNoticeAttr.GetAdvices, value); }
        }
        public String AdviceDesc
        {
            get { return this.GetValStrByKey(MyNoticeAttr.AdviceDesc); }
            set { this.SetValByKey(MyNoticeAttr.AdviceDesc, value); }
        }
        public String AdviceItems
        {
            get { return this.GetValStrByKey(MyNoticeAttr.AdviceItems); }
            set { this.SetValByKey(MyNoticeAttr.AdviceItems, value); }
        }
        #endregion

        #region 5.其它信息
        public int NoticeSta
        {
            get { return this.GetValIntByKey(MyNoticeAttr.NoticeSta); }
            set { this.SetValByKey(MyNoticeAttr.NoticeSta, value); }
        }
        public DateTime StartTime
        {
            get { return this.GetValDateTime(MyNoticeAttr.StartTime); }
            set { this.SetValByKey(MyNoticeAttr.StartTime, value); }
        }
        public DateTime StopTime
        {
            get { return this.GetValDateTime(MyNoticeAttr.StopTime); }
            set { this.SetValByKey(MyNoticeAttr.StopTime, value); }
        }
        public int SetMes
        {
            get { return this.GetValIntByKey(MyNoticeAttr.SetMes); }
            set { this.SetValByKey(MyNoticeAttr.SetMes, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知
        /// </summary>
        public MyNotice() { }
        /// <summary>
        /// 通知
        /// </summary>
        /// <param name="no">编号</param>
        public MyNotice(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_Notice";
                map.EnDesc = "通知";
                // 实体的描述.
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.

                //基本信息
                map.AddTBIntPKOID();
                map.AddTBString(MyNoticeAttr.Title, null, "标题", true, false, 0, 300, 220, true);
                map.AddTBStringDoc(MyNoticeAttr.Doc, null, "内容", true, false, true);
                map.AddTBString(MyNoticeAttr.AttachFile, null, "上传附件", false, false, 0, 4000, 30, true);
                //map.AddMyFile("资料","rar");
                map.AddDDLEntities(MyNoticeAttr.FK_NoticeCategory, null, "类别", new NoticeCategorys(), true);

                //创建信息
               
                map.AddDDLEntities(MyNoticeAttr.FK_Dept, null, "部门", new BP.Port.Depts(), true);

                map.AddDDLEntities(MyNoticeAttr.FK_UserNo, null, "用户", new BP.Port.Emps(), true);
                map.AddBoolean(MyNoticeAttr.SendToPart, true, "是否发往部门个人", false, false);

                //展示设置
                map.AddTBStringDoc(MyNoticeAttr.SendToUsers, null, "发往用户", false, false);
                map.AddTBDateTime(MyNoticeAttr.SetTop, "置顶", false, false);
                map.AddDDLSysEnum(MyNoticeAttr.Importance, 0, "重要程度", true, true, MyNoticeAttr.Importance,
                    "0=普通@1=比较重要@2=很重要");
                map.AddTBDate(MyNoticeAttr.RDT, null, "发布日期", true, false);
                map.AddDDLSysEnum(MyNoticeAttr.NoticeSta, 0, "状态", false, true, NoticeAttr.NoticeSta, "@0=自动@1=打开@2=关闭");
             

                map.AddSearchAttr(MyNoticeAttr.FK_Dept);
                map.AddSearchAttr(MyNoticeAttr.FK_NoticeCategory);
                map.AddSearchAttr(MyNoticeAttr.NoticeSta);

                //查询详情
                RefMethod rm = new RefMethod();
                rm.Title = "附件";
                rm.ClassMethodName = this.ToString() + ".DoOpen";
                rm.RefMethodType = RefMethodType.LinkModel;
                rm.IsForEns = true;
                map.AddRefMethod(rm);
                
                //获取 发送给当前人员所在的部门 + 岗位 +指定当前人员的通知 hzm
                AttrOfSearch search = new AttrOfSearch(MyNoticeAttr.SendToUsers, "发往用户",
                   MyNoticeAttr.SendToUsers, " like ", '%' + BP.Web.WebUser.No + '%', 0, true);
                map.AttrsOfSearch.Add(search);



                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
        /// <summary>
        /// 打开这个通知.
        /// </summary>
        /// <returns></returns>
        public string DoOpen()
        {
            if (string.IsNullOrWhiteSpace(this.AttachFile))
                return "当前公告没有附件。";

            return "/App/Notice/FileDown.htm?OID="+this.OID;
        }
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class MyNotices : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new MyNotice();
            }
        }
        /// <summary>
        /// 通知集合
        /// </summary>
        public MyNotices()
        {

        }
    }
}
