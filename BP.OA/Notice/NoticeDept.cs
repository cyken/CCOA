﻿using System;
using System.Collections;
using BP.DA;
using BP.En;
using BP.WF.Port;

namespace BP.OA
{
	/// <summary>
	/// 节点部门属性	  
	/// </summary>
	public class NoticeDeptAttr
	{
		/// <summary>
		/// 节点
		/// </summary>
		public const string FK_Notice="FK_Notice";
		/// <summary>
		/// 工作部门
		/// </summary>
		public const string FK_Dept="FK_Dept";
	}
	/// <summary>
	/// 节点部门
	/// 节点的工作部门有两部分组成.	 
	/// 记录了从一个节点到其他的多个节点.
	/// 也记录了到这个节点的其他的节点.
	/// </summary>
	public class NoticeDept :EntityMM
	{
		#region 基本属性
		/// <summary>
		///节点
		/// </summary>
		public int  FK_Notice
		{
			get
			{
				return this.GetValIntByKey(NoticeDeptAttr.FK_Notice);
			}
			set
			{
				this.SetValByKey(NoticeDeptAttr.FK_Notice,value);
			}
		}
		/// <summary>
		/// 工作部门
		/// </summary>
		public string FK_Dept
		{
			get
			{
				return this.GetValStringByKey(NoticeDeptAttr.FK_Dept);
			}
			set
			{
				this.SetValByKey(NoticeDeptAttr.FK_Dept,value);
			}
		}
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return base.HisUAC;
            }
        }
		#endregion 

		#region 构造方法
		/// <summary>
		/// 节点部门
		/// </summary>
		public NoticeDept(){}
		/// <summary>
		/// 重写基类方法
		/// </summary>
		public override Map EnMap
		{
			get
			{
				if (this._enMap!=null) 
					return this._enMap;

                Map map = new Map("OA_NoticeDept", "节点部门");			 

				map.DepositaryOfEntity=Depositary.None;
				map.DepositaryOfMap=Depositary.Application;

                map.AddTBIntPK(NoticeDeptAttr.FK_Notice, 0, "公告", false, false);
				map.AddDDLEntitiesPK( NoticeDeptAttr.FK_Dept,null,"部门",new BP.Port.Depts(),true);

				this._enMap=map;
				 
				return this._enMap;
			}
		}
		#endregion

	}
	/// <summary>
	/// 节点部门
	/// </summary>
    public class NoticeDepts : EntitiesMM
    {
        /// <summary>
        /// 他的工作部门
        /// </summary>
        public Stations HisStations
        {
            get
            {
                Stations ens = new Stations();
                foreach (NoticeDept ns in this)
                {
                    ens.AddEntity(new Station(ns.FK_Dept));
                }
                return ens;
            }
        }
        /// <summary>
        /// 他的工作节点
        /// </summary>
        public Notices HisNotices
        {
            get
            {
                Notices ens = new Notices();
                foreach (NoticeDept ns in this)
                {
                    ens.AddEntity(new Notice(ns.FK_Notice));
                }
                return ens;

            }
        }
        /// <summary>
        /// 节点部门
        /// </summary>
        public NoticeDepts() { }
        /// <summary>
        /// 节点部门
        /// </summary>
        /// <param name="NoticeID">节点ID</param>
        public NoticeDepts(int NoticeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(NoticeDeptAttr.FK_Notice, NoticeID);
            qo.DoQuery();
        }
        /// <summary>
        /// 节点部门
        /// </summary>
        /// <param name="StationNo">StationNo </param>
        public NoticeDepts(string StationNo)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(NoticeDeptAttr.FK_Dept, StationNo);
            qo.DoQuery();
        }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new NoticeDept();
            }
        }
        /// <summary>
        /// 取到一个工作部门集合能够访问到的节点s
        /// </summary>
        /// <param name="sts">工作部门集合</param>
        /// <returns></returns>
        public Notices GetHisNotices(Stations sts)
        {
            Notices nds = new Notices();
            Notices tmp = new Notices();
            foreach (Station st in sts)
            {
                tmp = this.GetHisNotices(st.No);
                foreach (Notice nd in tmp)
                {
                    if (nds.Contains(nd))
                        continue;
                    nds.AddEntity(nd);
                }
            }
            return nds;
        }
        /// <summary>
        /// 工作部门对应的节点
        /// </summary>
        /// <param name="stationNo">工作部门编号</param>
        /// <returns>节点s</returns>
        public Notices GetHisNotices(string stationNo)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(NoticeDeptAttr.FK_Dept, stationNo);
            qo.DoQuery();

            Notices ens = new Notices();
            foreach (NoticeDept en in this)
            {
                ens.AddEntity(new Notice(en.FK_Notice));
            }
            return ens;
        }
        /// <summary>
        /// 转向此节点的集合的Notices
        /// </summary>
        /// <param name="NoticeID">此节点的ID</param>
        /// <returns>转向此节点的集合的Notices (FromNotices)</returns> 
        public Stations GetHisStations(int NoticeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(NoticeDeptAttr.FK_Notice, NoticeID);
            qo.DoQuery();

            Stations ens = new Stations();
            foreach (NoticeDept en in this)
            {
                ens.AddEntity(new Station(en.FK_Dept));
            }
            return ens;
        }

        #region 为了适应自动翻译成java的需要,把实体转换成List.
        /// <summary>
        /// 转化成 java list,C#不能调用.
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.IList<NoticeDept> ToJavaList()
        {
            return (System.Collections.Generic.IList<NoticeDept>)this;
        }
        /// <summary>
        /// 转化成list
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.List<NoticeDept> Tolist()
        {
            System.Collections.Generic.List<NoticeDept> list = new System.Collections.Generic.List<NoticeDept>();
            for (int i = 0; i < this.Count; i++)
            {
                list.Add((NoticeDept)this[i]);
            }
            return list;
        }
        #endregion 为了适应自动翻译成java的需要,把实体转换成List.

    }
}
