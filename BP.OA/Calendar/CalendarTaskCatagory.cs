﻿using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Calendar
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class CalendarTaskCatagoryAttr : EntityNoNameAttr
    {
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public class CalendarTaskCatagory : EntityNoName
    {
        #region 属性
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public CalendarTaskCatagory() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public CalendarTaskCatagory(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_CalendarTaskCatagory";
                map.EnDesc = "通知类别";   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.

                map.AddTBStringPK(CalendarTaskCatagoryAttr.No, null, "编号", true, true, 2, 2, 20);
                map.AddTBString(CalendarTaskCatagoryAttr.Name, null, "名称", true, false, 0, 100, 30);

                //map.AddTBString(CalendarTaskCatagoryAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(CalendarTaskCatagoryAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class CalendarTaskCatagorys : EntitiesNoName
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new CalendarTaskCatagory();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public CalendarTaskCatagorys()
        {
        }
    }
}
