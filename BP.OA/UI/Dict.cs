﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BP.En;
using BP.DA;

namespace BP.OA.UI
{
    public class Dict
    {

         

        #region //3.BindListCtrl
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String sSql, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(sSql), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="Cn">数据连接</param>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String cn, String sSql, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(cn, sSql), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="dt">DataTable数据类型</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(System.Data.DataTable dt, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(dt), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将BP.Entitis（格式：id+name，两个字段）
        /// </summary>
        /// <param name="ens">实体集合数据</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(Entities ens, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(ens, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String sSql, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(sSql, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="Cn">数据连接</param>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String cn, String sSql, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(cn, sSql, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="dt">DataTable数据类型</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(System.Data.DataTable dt, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(dt, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="sSql">listItems对象数组</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(ListItem[] listItems, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            if (String.IsNullOrEmpty(textFormat))
            {
                textFormat = "{0}";
            }
            if (listCtrl is ListControl)
            {
                ListControl lc = (ListControl)listCtrl;
                lc.Items.Clear();
                if (!String.IsNullOrEmpty(defaultValue))
                {
                    string[] arr_d = defaultValue.Split('#');
                    lc.Items.Add(new ListItem(arr_d[1], arr_d[0]));
                }
                foreach (ListItem li in listItems)
                {
                    li.Text = String.Format(textFormat, li.Text);
                }
                lc.Items.AddRange(listItems);
                if (!String.IsNullOrEmpty(SelectedValue))
                {
                    lc.SelectedValue = SelectedValue;
                }
            }
            else if (listCtrl is HtmlSelect)
            {
                HtmlSelect lc = (HtmlSelect)listCtrl;
                lc.Items.Clear();
                if (!String.IsNullOrEmpty(defaultValue))
                {
                    string[] arr_d = defaultValue.Split('#');
                    lc.Items.Add(new ListItem(arr_d[1], arr_d[0]));
                }
                foreach (ListItem li in listItems)
                {
                    li.Text = String.Format(textFormat, li.Text);
                }
                lc.Items.AddRange(listItems);
                if (!String.IsNullOrEmpty(SelectedValue))
                {
                    lc.Value = SelectedValue;
                }
            }
        }
        #endregion
    }
}
