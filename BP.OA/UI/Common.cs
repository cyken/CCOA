﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.UI;
using System.IO;
using System.Data;
using BP.Web;
using System.Text.RegularExpressions;
using System.Web.Caching;
using BP.OA.Meeting;
using BP.Port;

namespace CCOA
{
    public class Common
    {
        public static void VirtualLogin()
        {
            BP.Web.WebUser.FK_DeptName = "集团总部";
            WebUser.FK_Dept = "100";
            WebUser.Name = "周朋";
            WebUser.No = "zhoupeng";
        }

        public static void VLogin(DataTable dt)
        {

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    WebUser.FK_Dept = dr["FK_dept"] as string;
                    string temp = dr["No"] as string;

                    WebUser.Name = dr["Name"] as string;
                    //WebUser.Pass = dr["Pass"] as string;
                    WebUser.SID = dr["sid"] as string;
                    Emp emp = new Emp(temp);
                   // WebUser.SignInOfGenerLang(emp, WebUser.SysLang);

                    temp = WebUser.No;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        //public static T GetValue<T>(Page page, string[] ignores = null)
        //{
        //    Type t = typeof(T);
        //    T instance = (T)Activator.CreateInstance(t);
        //    PropertyInfo[] infos = t.GetProperties();
        //    foreach (PropertyInfo info in infos)
        //    {
        //        if (ignores == null || !ignores.Contains(info.Name))
        //        {
        //            if (page.Request.Form[info.Name] != null)
        //            {
        //                if (info.PropertyType.Name == "Int32")
        //                {
        //                    info.SetValue(instance, Convert.ToInt32(page.Request.Form[info.Name]), null);
        //                }
        //                else if (info.PropertyType.Name == "DateTime")
        //                {
        //                    if (page.Request.Form[info.Name] != string.Empty)
        //                    {
        //                        info.SetValue(instance, Convert.ToDateTime(page.Request.Form[info.Name]), null);
        //                    }
        //                }
        //                else
        //                {
        //                    info.SetValue(instance, page.Request.Form[info.Name], null);
        //                }
        //            }
        //        }
        //    }
        //    return instance;
        //}
        public static T GetValue<T>(DataRow dr)
        {
            //Type t = typeof(T);
            //T instance = (T)Activator.CreateInstance(t);
            //PropertyInfo[] infos = t.GetProperties();
            //foreach (PropertyInfo info in infos)
            //{
            //    if (info.PropertyType.Name == "Int32")
            //    {
            //        info.SetValue(instance, Convert.ToInt32(dr[info.Name]), null);
            //    }
            //    else if (info.PropertyType.Name == "DateTime")
            //    {
            //        info.SetValue(instance, Convert.ToDateTime(dr[info.Name]), null);
            //    }
            //    else
            //    {
            //        info.SetValue(instance, dr[info.Name].ToString(), null);
            //    }
            //}
            //return instance;



            Type t = typeof(T);
            T instance = (T)Activator.CreateInstance(t);
            PropertyInfo[] infos = t.GetProperties();
            foreach (DataColumn dc in dr.Table.Columns)
            {
                foreach (PropertyInfo info in infos)
                {
                    if (info.Name == dc.ColumnName)
                    {
                        if (info.PropertyType.Name == "Int32")
                        {
                            info.SetValue(instance, Convert.ToInt32(dr[info.Name]), null);
                        }
                        else if (info.PropertyType.Name == "DateTime")
                        {
                            info.SetValue(instance, Convert.ToDateTime(dr[info.Name]), null);
                        }
                        else
                        {
                            info.SetValue(instance, dr[info.Name].ToString(), null);
                        }
                    }
                }
            }
            return instance;
        }
        /// <summary>
        /// 插入
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="en"></param>
        /// <param name="ignores"></param>
        /// <returns></returns>
        public static int Insert<T>(T en, string[] ignores = null)
        {
            Type t = en.GetType();
            string strSql1 = "insert into " + t.Name + "({0}) values({1})";
            string strSql2 = string.Empty;
            string strSql3 = string.Empty;
            PropertyInfo[] infos = t.GetProperties();
            foreach (PropertyInfo info in infos)
            {
                if (ignores == null || !ignores.Contains(info.Name))
                {
                    strSql2 += info.Name + ",";
                    strSql3 += "'" + Convert.ToString(info.GetValue(en, null)) + "'" + ",";
                }
            }
            strSql1 = string.Format(strSql1, strSql2.TrimEnd(','), strSql3.TrimEnd(','));
            return BP.DA.DBAccess.RunSQL(strSql1);
        }
        public static int Update<T>(T en, string Key, string Value, string[] ignores = null)
        {
            Type t = en.GetType();
            string strSql1 = "UPDATE " + t.Name + " SET {0} WHERE {1}";
            string strSql2 = string.Empty;
            string strSql3 = string.Empty;
            PropertyInfo[] infos = t.GetProperties();
            foreach (PropertyInfo info in infos)
            {
                if (ignores == null || !ignores.Contains(info.Name))
                {
                    string strValue = Convert.ToString(info.GetValue(en, null));
                    strSql2 += info.Name + "=" + "'" + strValue + "'" + ",";
                }
            }
            strSql1 = string.Format(strSql1, strSql2.TrimEnd(','), Key + "=" + Value);
            return BP.DA.DBAccess.RunSQL(strSql1);
        }
        /// <summary>
        /// 上传
        /// </summary>
        /// <param name="o"></param>
        /// <returns>文件保存路径</returns>
        public static string Upload(object o)
        {
            HttpPostedFile file = o as HttpPostedFile;
            if (file != null && file.ContentLength > 0)
            {
                string guid = Guid.NewGuid().ToString().Replace("-", string.Empty);
                string exten = Path.GetExtension(file.FileName);
                string filename = guid + exten;
                Stream fileStream = file.InputStream;
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DataUser/UploadFile/Meeting");
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                if (dirInfo.Exists == false)
                {
                    dirInfo.Create();
                }
                file.SaveAs(Path.Combine(path, filename));
                return "/DataUser/UploadFile/Meeting/" + filename;
            }
            return string.Empty;
        }
        public static int Insert(Page page, string[] names, string table)
        {

            string strSql1 = "insert into " + table + "({0}) values({1})";
            string strSql2 = string.Empty;
            string strSql3 = string.Empty;
            foreach (string name in names)
            {
                strSql2 += name + ",";
                strSql3 += "'" + Convert.ToString(page.Request.Form[name]) + "'" + ",";
            }
            strSql1 = string.Format(strSql1, strSql2.TrimEnd(','), strSql3.TrimEnd(','));
            return BP.DA.DBAccess.RunSQL(strSql1);
        }
        public static int Update(Page page, string Key, string Value, string[] names, string table)
        {
            string strSql1 = "UPDATE " + table + " SET {0} WHERE {1}";
            string strSql2 = string.Empty;
            string strSql3 = string.Empty;
            foreach (string name in names)
            {
                string strValue = Convert.ToString(page.Request.Form[name]);
                strSql2 += name + "=" + "'" + strValue + "'" + ",";
            }
            strSql1 = string.Format(strSql1, strSql2.TrimEnd(','), Key + "=" + Value);
            return BP.DA.DBAccess.RunSQL(strSql1);
        }

        public static void Alert(string strMsg, Page page)
        {
            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "Alert", "alert('" + strMsg + "')", true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public static bool IsDatetime(string strValue)
        {
            string strReg = @"([1-2][0-9][0-9][0-9])-(0*[1-9]|1[0-2])-(0*[1-9]|[12][0-9]|3[01])\ (0*[0-9]|1[0-9]|2[0-3]):(0*[0-9]|[1-5][0-9]):(0[0-9]|[1-5][0-9])";
            if (strValue == string.Empty)
            {
                return false;
            }
            else
            {
                Regex re = new Regex(strReg);
                MatchCollection mc = re.Matches(strValue);
                if (mc.Count == 1)
                    foreach (Match m in mc)
                    {
                        if (m.Value == strValue)
                            return true;
                    }
            }
            return false;
        }
    }
    public class MyStringComparer : StringComparer
    {
        public override int Compare(string x, string y)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(string x, string y)
        {
            return x.ToLower() == y.ToLower();
        }

        public override int GetHashCode(string obj)
        {
            throw new NotImplementedException();
        }
    }
}