﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA
{
   public class API
    {
        #region 生产工艺管理 api.
        /// <summary>
        /// 生产工艺流程是否启用?
        /// </summary>
        public static bool SCGY_SCGY_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("SCGY_SCGY_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 生产工艺 流程标记
        /// </summary>
        public static string SCGY_SCGY_FlowMark
        {
            get
            {
                return "SCGYSCGY";
            }
        }

       /// <summary>
       /// 生产工艺 api
       /// </summary>
       /// <param name="bt">标题</param>
       /// <param name="qqr">请求人</param>
       /// <param name="gylx">工艺类型</param>
       /// <param name="fgs">分公司</param>
       /// <param name="workid"></param>
       /// <returns></returns>
        public static string SCGY_SCGY(string bt, string qqr, string fgs,int lczt,
            Int64 workid) {

                SCGY en = new SCGY();

                en.OID = int.Parse(workid + "");
                en.Retrieve();

                en.BT = bt;
                en.QQR = qqr;
                en.FK_FGS = fgs;
                en.LCZT = lczt;
                
                en.Update();
                return "...";
            
        }
        
        #endregion 采购api.
    }
}
