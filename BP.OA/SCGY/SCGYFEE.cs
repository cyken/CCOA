﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.OA;

namespace BP.OA
{
   public class SCGYFEE:BP.WF.FlowEventBase
    {
          #region 构造.
        /// <summary>
        /// 保养 流程事件实体
        /// </summary>
        public SCGYFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return API.SCGY_SCGY_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            SCGY a = new SCGY();

            a.OID = Int32.Parse(this.WorkID+"");
            a.Retrieve();

             if (a.WFState.Equals("2") && a.FlowEndNode.Equals("2902"))
            {
                // 申请
                a.LCZT = (int)BP.OA.LCZT.SQ;
            }
             else if (a.WFState.Equals("2") && a.FlowEndNode.Equals("2903"))
             {
                 // 流程中
                 a.LCZT = (int)BP.OA.LCZT.LCZ;
             }
             else { 
                //流程结束
                 a.LCZT = (int)BP.OA.LCZT.LCJS;
             }

            int temp = a.LCZT;

            API.SCGY_SCGY(this.GetValStr("BT"), this.GetValStr("QQR"),
                  this.GetValStr("OP_Dept"), temp, this.WorkID);
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {

            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
