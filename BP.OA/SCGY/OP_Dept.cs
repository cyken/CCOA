﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;
using BP.Port;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 生产工艺的属性
    /// </summary>
    public class OP_DeptAttr : EntityNoNameAttr {
        
    }

  /// <summary>
    /// 生产工艺
    /// </summary>
   public class OP_Dept:EntityNoName
   {
       #region 属性
       
       #endregion

       #region 构造方法
       public OP_Dept() { }
       #endregion

       public override Map EnMap
       {
           get {
               if (this._enMap != null)
                   return this._enMap;

               Map map = new Map("OP_Dept");
               map.EnDesc = "生产工艺";

               map.DepositaryOfEntity = BP.DA.Depositary.None;
               map.DepositaryOfMap = BP.DA.Depositary.None;

               map.AddTBStringPK(OP_DeptAttr.No,null,"编号",true,true,0,100,30);
               map.AddTBString(OP_DeptAttr.Name, null, "部门名称", true, true, 0, 100, 30);

               this._enMap = map;
               return this._enMap;



           }
       }

         #region 重写方法
       protected override bool beforeInsert()
       {
            return base.beforeInsert();
       }
       protected override bool beforeUpdate()
       {
            return base.beforeUpdate();
       }
       protected override bool beforeUpdateInsertAction()
       {
           //if(this.WFState.Equals("3")){
           //    // 流程结束
           //    this.LCZT = (int)BP.OA.LCZT.LCJS;
           //}else if (this.WFState.Equals("2") && this.FlowEndNode.Equals("2902"))
           //{
           //    // 申请
           //    this.LCZT = (int)BP.OA.LCZT.SQ;
           //}else {
           //    // 流程中
           //    this.LCZT = (int)BP.OA.LCZT.LCZ;
           //}
           return base.beforeUpdateInsertAction();
       }
      
       #endregion
    }
    /// <summary>
    /// 生产工艺 s
    /// </summary>
   public class OP_Depts : EntitiesNoName
   {
       #region 构造方法
       public OP_Depts() { }
       #endregion
       /// <summary>
       /// 得到它的entity
       /// </summary>
       public override Entity GetNewEntity
       {
           get {
               return new OP_Dept();
           }
       }
   }
}
