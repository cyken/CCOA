using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA
{
    /// <summary>
    /// 上下班记录属性
    /// </summary>
    public class DailyRecordAttr : BP.En.EntityNoNameAttr
    {
        #region 基本属性
        /// <summary>
        /// 上下班记录编号
        /// </summary>
        public const string DailyRecordNo = "DailyRecordNo";
        /// <summary>
        /// 部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 职务
        /// </summary>
        public const string FK_Duty = "FK_Duty";
        /// <summary>
        /// FK_Unit
        /// </summary>
        public const string FK_Unit = "FK_Unit";
        /// <summary>
        /// 密码
        /// </summary>
        public const string Pass = "Pass";
        /// <summary>
        /// sid
        /// </summary>
        public const string SID = "SID";
        /// <summary>
        /// 电话
        /// </summary>
        public const string Tel = "Tel";
        /// <summary>
        /// 邮箱
        /// </summary>
        public const string Email = "Email";
        /// <summary>
        /// 部门数量
        /// </summary>
        public const string NumOfDept = "NumOfDept";
        #endregion
    }
    /// <summary>
    /// 上下班记录 的摘要说明。
    /// </summary>
    public class DailyRecord : EntityOID
    {
        #region 扩展属性
        /// <summary>
        /// 上下班记录编号
        /// </summary>
        public string DailyRecordNo
        {
            get
            {
                return this.GetValStrByKey(DailyRecordAttr.DailyRecordNo);
            }
            set
            {
                this.SetValByKey(DailyRecordAttr.DailyRecordNo, value);
            }
        }
        /// <summary>
        /// 职务
        /// </summary>
        public string FK_Duty
        {
            get
            {
                return this.GetValStrByKey(DailyRecordAttr.FK_Duty);
            }
            set
            {
                this.SetValByKey(DailyRecordAttr.FK_Duty, value);
            }
        }
        
        public string FK_DeptText
        {
            get
            {
                return this.GetValRefTextByKey(DailyRecordAttr.FK_Dept);
            }
        }
        /// <summary>
        /// 密码
        /// </summary>
        public string Pass
        {
            get
            {
                return this.GetValStrByKey(DailyRecordAttr.Pass);
            }
            set
            {
                this.SetValByKey(DailyRecordAttr.Pass, value);
            }
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 检查密码(可以重写此方法)
        /// </summary>
        /// <param name="pass">密码</param>
        /// <returns>是否匹配成功</returns>
        public bool CheckPass(string pass)
        {
            if (this.Pass == pass)
                return true;
            return false;
        }
        #endregion 公共方法

        #region 构造函数
        /// <summary>
        /// 上下班记录
        /// </summary>
        public DailyRecord()
        {
        }
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForAppAdmin();
                return uac;
            }
        }
        /// <summary>
        /// 重写基类方法
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();

                #region 基本属性
                map.EnDBUrl =
                    new DBUrl(DBUrlType.AppCenterDSN); //要连接的数据源（表示要连接到的那个系统数据库）。
                map.PhysicsTable = "OA_DailyRecord";  
                map.DepositaryOfMap = Depositary.Application;    //实体map的存放位置.
                map.DepositaryOfEntity = Depositary.Application; //实体存放位置
                map.EnDesc = "上下班记录通讯录"; // "用户"; // 实体的描述.
                map.EnType = EnType.App;   //实体类型。
                #endregion

                #region 字段
                /*关于字段属性的增加 */
                map.AddTBStringPK(DailyRecordAttr.No, null, "编号", true, false, 1, 20, 30);
                map.AddTBString(DailyRecordAttr.DailyRecordNo, null, "职工编号", true, false, 0, 20, 30);
                map.AddTBString(DailyRecordAttr.Name, null, "名称", true, false, 0, 100, 30);
                map.AddTBString(DailyRecordAttr.Pass, "123", "密码", false, false, 0, 20, 10);

                map.AddTBString(DailyRecordAttr.FK_Dept, null, "当前部门", false, false, 0, 20, 10);
                map.AddTBString(DailyRecordAttr.FK_Duty, null, "当前职务", false, false, 0, 20, 10);

                map.AddTBString(DailyRecordAttr.SID, null, "安全校验码", false, false, 0, 32, 32);
                map.AddTBString(DailyRecordAttr.Tel, null, "电话", true, false, 0, 20, 130);
                map.AddTBString(DailyRecordAttr.Email, null, "邮箱", true, false, 0, 100, 132);
                #endregion 字段

                 map.AddSearchAttr(DailyRecordAttr.FK_Dept);
  
                this._enMap = map;
                return this._enMap;
            }
        }

        /// <summary>
        /// 获取集合
        /// </summary>
        public override Entities GetNewEntities
        {
            get { return new DailyRecords(); }
        }
        #endregion 构造函数
    }
    /// <summary>
    /// 上下班记录s
    // </summary>
    public class DailyRecords : EntitiesOID
    {
        #region 构造方法
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DailyRecord();
            }
        }
        /// <summary>
        /// 上下班记录s
        /// </summary>
        public DailyRecords()
        {
        }
        public override int RetrieveAll()
        {
            return base.RetrieveAll("Name");
        }
        #endregion 构造方法
    }
}
