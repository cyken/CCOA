﻿using BP.En;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.Operate
{
    public class GoodAttr : EntityOIDAttr
    {
        public const string Name = "Name";
        public const string FK_PP = "FK_PP";
        public const string FK_GT = "FK_GT";
        public const string FK_GS = "FK_GS";
        public const string GGXH = "GGXH";
        public const string GoodNumber = "GoodNumber";
        public const string Sale = "Sale";
        public const string Note = "Note";
        
    }
    public class Good : EntityOID
    {
        #region 构造方法
        public Good() { }
        public Good(int OID) : base(OID) { }
        #endregion


        public override Map EnMap
        {
            get 
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Goods");

                map.EnDesc = "商品";
                map.CodeStruct = "3";

                map.DepositaryOfEntity = BP.DA.Depositary.None;

                map.AddTBIntPKOID();
                map.AddTBString(GoodAttr.Name,null,"商品名称",true,false,0,100,30);
                map.AddDDLEntities(GoodAttr.FK_GT,null,"商品类型",new GoodTypes(),true);
                map.AddDDLEntities(GoodAttr.FK_PP,null,"计划名称",new ProcurementPlans(),true);
                map.AddDDLEntities(GoodAttr.FK_GS, null, "供应商", new Suppliers(), true);
                map.AddTBString(GoodAttr.GGXH,null,"规格型号",true,false,0,200,30);
                map.AddTBInt(GoodAttr.GoodNumber,0,"数量",true,false);
                map.AddTBMoney(GoodAttr.Sale,0,"单价",true,false);
                map.AddTBStringDoc(GoodAttr.Note,null,"备注",true,false);


                map.AddSearchAttr(GoodAttr.FK_PP);
                map.AddSearchAttr(GoodAttr.FK_GT);

                this._enMap = map;
                return this._enMap;
            }
        }
    }
    public class Goods : SimpleNoNames
    {
        public Goods() { }
        public override Entity GetNewEntity
        {
            get 
            {
                return new Good();   
            }
        }
    }
}
