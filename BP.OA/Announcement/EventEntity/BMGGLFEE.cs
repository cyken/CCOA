﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/************************************************************
   Copyright (C), 2005-2014, manstrosoft Co., Ltd.
   FileName: BMGG.cs
   Author:孙成瑞
   Date:2014-5-26
   Description:部门公告
   ***********************************************************/
namespace BP.OA.Announcement.EventEntity
{
    public class BMGGLFEE : BP.WF.FlowEventBase
    {
        public override string FlowMark
        {
            get { return API.RCSWGL_BMGG_FlowMark; }
        }

        public override string FlowOverBefore()
        {
            return null;
        }
        /// <summary>
        /// 流程结束后触发
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {

            API.RCSWGL_BMGG(this.GetValStr("FQR"), this.GetValStr("SSBM"), this.GetValDateTime("FQSJ"), this.GetValStr("BT"), this.GetValStr("GGNR"), this.GetValStr("GS"), this.WorkID);
            return "写入信息成功";
        }

        public override string BeforeFlowDel()
        {
            return null;
        }

        public override string AfterFlowDel()
        {
            return null;
        }
    }
}
