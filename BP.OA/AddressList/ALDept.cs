using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 部门属性
    /// </summary>
    public class ALDeptAttr : EntityTreeAttr
    {
        /// <summary>
        /// 部门类型
        /// </summary>
        public const string FK_ALDeptType = "FK_ALDeptType";
        /// <summary>
        /// 部门负责人
        /// </summary>
        public const string Leader = "Leader";
        /// <summary>
        /// 联系电话
        /// </summary>
        public const string Tel = "Tel";
        /// <summary>
        /// 单位全名
        /// </summary>
        public const string NameOfPath = "NameOfPath";
    }
    /// <summary>
    /// 部门
    /// </summary>
    public class ALDept : EntityTree
    {
        #region 属性
        /// <summary>
        /// 部门负责人
        /// </summary>
        public string Leader
        {
            get
            {
                return this.GetValStrByKey(ALDeptAttr.Leader);
            }
            set
            {
                this.SetValByKey(ALDeptAttr.Leader, value);
            }
 
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Tel
        {
            get
            {
                return this.GetValStrByKey(ALDeptAttr.Tel);
            }
            set
            {
                this.SetValByKey(ALDeptAttr.Tel, value);
            }
 
        }
        /// <summary>
        /// 全名
        /// </summary>
        public  string NameOfPath
        {
            get
            {
                return this.GetValStrByKey(ALDeptAttr.NameOfPath);
            }
            set
            {
                this.SetValByKey(ALDeptAttr.NameOfPath, value);
            }
        }
       
        /// <summary>
        /// 部门类型
        /// </summary>
        public string FK_ALDeptType
        {
            get
            {
                return this.GetValStrByKey(ALDeptAttr.FK_ALDeptType);
            }
            set
            {
                this.SetValByKey(ALDeptAttr.FK_ALDeptType, value);
            }
        }
     
        #endregion

        #region 构造函数
        /// <summary>
        /// 部门
        /// </summary>
        public ALDept() { }
        /// <summary>
        /// 部门
        /// </summary>
        /// <param name="no">编号</param>
        public ALDept(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                //uac.OpenForSysAdmin();
                uac.IsUpdate = true;
                uac.IsDelete = true;
                uac.IsInsert = true;
                uac.IsView = true;
                return uac;  

            }
        }
        protected override bool beforeInsert()
        {
           // ALDept ap = new ALDept();
           

            return base.beforeInsert();
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                //map.EnDBUrl = new DBUrl(DBUrlType.AppCenterDSN); //连接到的那个数据库上. (默认的是: AppCenterDSN )
             
                map.PhysicsTable = "Port_ALDept";
                //map.EnType = EnType.Admin;
                map.EnDesc = "部门"; //  实体的描述.
                map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                //比如xx分公司财务部
                map.AddTBStringPK(ALDeptAttr.No, null, "编号", true, true, 1, 50, 20);     
                map.AddTBString(ALDeptAttr.Name, null, "名称", true, false, 0, 100, 30);
                map.AddTBString(ALDeptAttr.ParentNo, null, "父节点编号", true, false, 0, 100, 30);

                map.AddTBString(ALDeptAttr.FK_ALDeptType, null, "部门类型", true, false, 0, 100, 30);
                map.AddTBString(ALDeptAttr.Leader, null, "部门负责人", true, false, 0, 100, 30);
                map.AddTBString(ALDeptAttr.NameOfPath, null, "单位全名", true, false, 0, 100, 30);
                map.AddTBString(ALDeptAttr.Tel, null, "联系电话", true, false, 0, 100, 30);


                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class ALDepts : EntitiesNoName
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ALDept();
            }
        }
        /// <summary>
        /// 部门集合
        /// </summary>
        public ALDepts()
        {

        }
        /// <summary>
        /// 部门集合
        /// </summary>
        /// <param name="parentNo">父部门No</param>
        public ALDepts(string parentNo)
        {
            this.Retrieve(ALDeptAttr.ParentNo, parentNo);
        }
    }
}
