﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BP.DA;
using BP.En;
using BP.OA;

/************************************************************
   Copyright (C), 2014-2014, manstrosoft Co., Ltd.
   FileName: CWAPI.cs
   Author:黄志敏
   Date:2014-7-24
   Description:考勤模块接口
   ***********************************************************/

namespace BP.OA.CheckWork
{
    /// <summary>
    /// 考勤 接口文档
    /// </summary>
    public class CWAPI
    {
      /// <summary>
        /// 获取考勤数据(所有)
      /// </summary>
      /// <param name="empNo">用户编号</param>
      /// <returns></returns>
        public DataTable GetRegisterWork(string empNo)
        {
            string sql = "select * from CW_RegisterWork where FK_Emp = '" + empNo + "'";
            return BP.DA.DBAccess.RunSQLReturnTable(sql);
        }

    
    }
}
