﻿using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.CheckWork
{
    /// <summary>
    /// 考勤 基本信息表
    /// </summary>
    public class RegisterWorkAttr : EntityNoNameAttr
    {
        #region 1.基本信息(FK_Emp,Type,RegisterTime,FK_RWState,Demo)
        /// <summary>
        /// 签到人
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 登记类型
        /// </summary>
        public const string FK_RWType = "FK_RWType";
        /// <summary>
        /// 规定时间
        /// </summary>
        public const string RegularTime = "RegularTime";
        /// <summary>
        /// 签到时间
        /// </summary>
        public const string RegisterTime = "RegisterTime";
        /// <summary>
        /// 操作时间
        /// </summary>
        public const string NYR = "NYR";
        /// <summary>
        /// 签到状态
        /// </summary>
        public const string FK_RWState = "FK_RWState";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Demo = "Demo";
        /// <summary>
        /// 起始时间
        /// </summary>
        public const string StartTime = "StartTime";
        /// <summary>
        /// 截止时间
        /// </summary>
        public const string EndTime = "EndTime";
        /// <summary>
        /// 天数
        /// </summary>
        public const string DCount = "DCount";
        /// <summary>
        /// 目的地
        /// </summary>
        public const string Addr = "Addr";
        /// <summary>
        /// 记录信息时间
        /// </summary>
        public const string RecordTime = "RecordTime";

        #endregion
    }
    /// <summary>
    /// 考勤
    /// </summary>
    public partial class RegisterWork : EntityOID
    {
        #region 1.基本属性
        /// <summary>
        /// 签到人
        /// </summary>
        public string FK_Emp
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.FK_Emp); }
            set { this.SetValByKey(RegisterWorkAttr.FK_Emp, value); }
        }
        /// <summary>
        /// 类型
        /// </summary>
        public String FK_RWType
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.FK_RWType); }
            set { this.SetValByKey(RegisterWorkAttr.FK_RWType, value); }
        }
        /// <summary>
        /// 规定时间
        /// </summary>
        public string RegularTime
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.RegularTime); }
            set { this.SetValByKey(RegisterWorkAttr.RegularTime, value); }
        }
        /// <summary>
        /// 签到时间
        /// </summary>
        public string RegisterTime
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.RegisterTime); }
            set { this.SetValByKey(RegisterWorkAttr.RegisterTime, value); }
        }  
        /// <summary>
        /// 操作时间
        /// </summary>
        public String NYR
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.NYR); }
            set { this.SetValByKey(RegisterWorkAttr.NYR, value); }
        }
        /// <summary>
        /// 签到状态
        /// </summary>
        public String FK_RWState
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.FK_RWState); }
            set { this.SetValByKey(RegisterWorkAttr.FK_RWState, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Demo
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.Demo); }
            set { this.SetValByKey(RegisterWorkAttr.Demo, value); }
        }

        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartTime
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.StartTime); }
            set { this.SetValByKey(RegisterWorkAttr.StartTime, value); }
        }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndTime
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.EndTime); }
            set { this.SetValByKey(RegisterWorkAttr.EndTime, value); }
        }
        /// <summary>
        /// 记录时间
        /// </summary>
        public String RecordTime
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.RecordTime); }
            set { this.SetValByKey(RegisterWorkAttr.RecordTime, value); }
        }
        /// <summary>
        /// 天数
        /// </summary>
        public String DCount
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.DCount); }
            set { this.SetValByKey(RegisterWorkAttr.DCount, value); }
        }
        /// <summary>
        /// 目的地
        /// </summary>
        public String Addr
        {
            get { return this.GetValStrByKey(RegisterWorkAttr.Addr); }
            set { this.SetValByKey(RegisterWorkAttr.Addr, value); }
        }
        #endregion


        #region 构造函数
        /// <summary>
        /// 考勤
        /// </summary>
        public RegisterWork() { }
        /// <summary>
        /// 考勤
        /// </summary>
        /// <param name="no">编号</param>
        public RegisterWork(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "CW_RegisterWork";
                map.EnDesc = "考勤";   // 实体的描述.

                //基本信息
                map.AddTBIntPKOID();
                map.AddTBString(RegisterWorkAttr.FK_Emp, null, "签到人", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.FK_RWType, null, "登记类型", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.RegisterTime, null, "签到时间", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.NYR, null, "操作时间", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.FK_RWState, null, "签到状态", true, false, 0, 300, 30, true);
                map.AddTBStringDoc(RegisterWorkAttr.Demo, null, "备注", true, false, 0, 4000, 30, 30);
                map.AddTBString(RegisterWorkAttr.StartTime, null, "开始时间", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.EndTime, null, "截止时间", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.RecordTime, null, "记录时间", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.DCount, null, "天数", true, false, 0, 300, 30, true);
                map.AddTBString(RegisterWorkAttr.Addr, null, "目的地", true, false, 0, 300, 30, true);
                //增加查询条件。
                map.AddSearchAttr(RegisterWorkAttr.FK_RWType);//类型
                map.AddSearchAttr(RegisterWorkAttr.FK_RWState); //状态

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        /// <summary>
        ///得到集合
        /// </summary>
        public class RegisterWorks : EntitiesOID
        {
            /// <summary>
            /// 得到一个新实体
            /// </summary>
            public override Entity GetNewEntity
            {
                get
                {
                    return new RegisterWork();
                }
            }
            /// <summary>
            /// 考勤集合
            /// </summary>
            public RegisterWorks()
            {
            }
        }
    }
}
