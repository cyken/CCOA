﻿using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.KM
{
    public enum FileInfoType
    {
        /// <summary>
        /// 文章
        /// </summary>
        Article,
        /// <summary>
        /// word,PDF文档(可以在线预览的)
        /// </summary>
        AthDocPDF,
        /// <summary>
        /// 其他文档，可以下载的.
        /// </summary>
        AthEtc
    }
    /// <summary>
    /// 知识属性
    /// </summary>
    public class FileInfoAttr : EntityMyPKAttr
    {
        /// <summary>
        /// 控制方式
        /// </summary>
        public const string KMTreeCtrlWay = "KMTreeCtrlWay";
        /// <summary>
        /// 树编号(内部)
        /// </summary>
        public const string RefTreeNo = "RefTreeNo";
        /// <summary>
        /// 树形的编号
        /// </summary>
        public const string TreeNo = "TreeNo";
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 阅读次数
        /// </summary>
        public const string ReadTimes = "ReadTimes";
        /// <summary>
        /// 下载次数
        /// </summary>
        public const string DownLoadTimes = "DownLoadTimes";
        /// <summary>
        /// 关键词
        /// </summary>
        public const string KeyWords = "KeyWords";
        /// <summary>
        /// 内容简介
        /// </summary>
        public const string Doc = "Doc";
        /// <summary>
        /// 发布人
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 发布部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 创建日期
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 修改人
        /// </summary>
        public const string EDTER = "EDTER";
        /// <summary>
        /// 修改日期
        /// </summary>
        public const string EDT = "EDT";
        /// <summary>
        /// 顺序号
        /// </summary>
        public const string Idx = "Idx";
        /// <summary>
        /// 是否可以下载
        /// </summary>
        public const string IsDownload = "IsDownload";
        /// <summary>
        /// 存放路径
        /// </summary>
        public const string UploadPath = "UploadPath";
        /// <summary>
        /// 文件扩展名
        /// </summary>
        public const string FileExt = "FileExt";
        /// <summary>
        /// 文件状态
        /// </summary>
        public const string FileStatus = "FileStatus";
        /// <summary>
        /// 文件大小
        /// </summary>
        public const string FileSize = "FileSize";
        /// <summary>
        /// 是否共享
        /// </summary>
        public const string IsShare = "IsShare";
        /// <summary>
        /// 是否启用
        /// </summary>
        public const string IsEnable = "IsEnable";
    }
    /// <summary>
    ///  知识
    /// </summary>
    public class FileInfo : EntityMyPK
    {
        #region 属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.Title);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.Title, value);
            }
        }
        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadTimes
        {
            get
            {
                return this.GetValIntByKey(FileInfoAttr.ReadTimes);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.ReadTimes, value);
            }
        }
        /// <summary>
        /// 下载次数
        /// </summary>
        public int DownLoadTimes
        {
            get
            {
                return this.GetValIntByKey(FileInfoAttr.DownLoadTimes);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.DownLoadTimes, value);
            }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.FK_Dept);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.FK_Dept, value);
            }
        }
        public string FK_DeptText
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FK_Dept))
                {
                    Dept dept = new Dept(this.FK_Dept);
                    return dept.Name;
                }
                return "";
            }
        }
        /// <summary>
        /// 发布人
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.FK_Emp, value);
            }
        }
        public string FK_EmpText
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FK_Emp))
                {
                    Emp emp = new Emp(this.FK_Emp);
                    return emp.Name;
                }
                return "";
            }
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string RDT
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.RDT);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.RDT, value);
            }
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string EDTER
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.EDTER);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.EDTER, value);
            }
        }
        public string EDTERText
        {
            get
            {
                return this.GetValRefTextByKey(FileInfoAttr.EDTER);
            }
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        public string EDT
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.EDT);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.EDT, value);
            }
        }
        /// <summary>
        /// 类别编号
        /// </summary>
        public string RefTreeNo
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.RefTreeNo);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.RefTreeNo, value);
            }
        }
        /// <summary>
        /// 关键词
        /// </summary>
        public string KeyWords
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.KeyWords);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.KeyWords, value);
            }
        }
        /// <summary>
        /// 内容简介
        /// </summary>
        public string Doc
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.Doc);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.Doc, value);
            }
        }
        /// <summary>
        /// 是否启用下载
        /// </summary>
        public string IsDownload
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.IsDownload);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.IsDownload, value);
            }
        }
        /// <summary>
        /// 文件存放路径
        /// </summary>
        public string UploadPath
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.UploadPath);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.UploadPath, value);
            }
        }
        /// <summary>
        /// 文件扩展名
        /// </summary>
        public string FileExt
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.FileExt);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.FileExt, value);
            }
        }
        /// <summary>
        /// 文件大小
        /// </summary>
        public float FileSize
        {
            get
            {
                return this.GetValFloatByKey(FileInfoAttr.FileSize);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.FileSize, value);
            }
        }
        /// <summary>
        /// 文件状态
        /// </summary>
        public FileStatus FileStatus
        {
            get
            {
                return (FileStatus)this.GetValIntByKey(FileInfoAttr.FileStatus);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.FileStatus, (int)value);
            }
        }
        /// <summary>
        /// 是否共享
        /// </summary>
        public string IsShare
        {
            get
            {
                return this.GetValStrByKey(FileInfoAttr.IsShare);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.IsShare, value);
            }
        }
        /// <summary>
        /// 控制方式
        /// </summary>
        public KMTreeCtrlWay KMTreeCtrlWay
        {
            get
            {
                return (KMTreeCtrlWay)this.GetValIntByKey(FileInfoAttr.KMTreeCtrlWay);
            }
            set
            {
                this.SetValByKey(FileInfoAttr.KMTreeCtrlWay, (int)value);
            }
        }
        /// <summary>
        /// 是否启用
        /// </summary>
        public string IsEnable
        {
            get
            {
                return this.GetValStrByKey(TreeAttr.IsEnable);
            }
            set
            {
                this.SetValByKey(TreeAttr.IsEnable, value);
            }
        }
        #endregion 属性

        #region 权限控制属性.
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 知识
        /// </summary>
        public FileInfo()
        {
        }
        /// <summary>
        /// 知识
        /// </summary>
        /// <param name="_No"></param>
        public FileInfo(string _No) : base(_No) { }
        #endregion

        /// <summary>
        /// 知识Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("KM_FileInfo");
                map.EnDesc = "知识";
                map.CodeStruct = "2";

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                //map.IsAllowRepeatNo = false;
                map.AddMyPK();

                map.AddTBString(FileInfoAttr.RefTreeNo, null, "树编号(内部)", false, false, 0, 100, 30);
                //map.AddDDLEntities(FileInfoAttr.RefTreeNo, null, "类别", new Trees(), true);
                map.AddTBString(FileInfoAttr.TreeNo, null, "树编号", false, false, 0, 100, 30);
                map.AddTBString(FileInfoAttr.Title, null, "文件名", true, false, 0, 100, 30, true);
                //内容
                map.AddTBStringDoc();
                map.AddTBString(FileInfoAttr.KeyWords, null, "关键词", true, false, 0, 2000, 300);
                map.AddTBString(FileInfoAttr.FK_Emp, null, "发布人", true, false, 0, 100, 30);
                map.AddTBString(FileInfoAttr.FK_Dept, null, "部门", true, false, 0, 100, 30);
                map.AddTBDateTime(FileInfoAttr.RDT, "创建日期", false, false);
                map.AddTBString(FileInfoAttr.EDTER, null, "修改人", true, false, 0, 100, 30);
                map.AddTBDateTime(FileInfoAttr.EDT,string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now), "修改日期", false, false);
                map.AddTBInt(FileInfoAttr.Idx, 0, "顺序号", false, false);
                map.AddTBInt(FileInfoAttr.ReadTimes, 0, "阅读次数", false, false);
                map.AddTBInt(FileInfoAttr.DownLoadTimes, 0, "下载次数", false, false);
                map.AddTBInt(FileInfoAttr.IsDownload, 0, "是否可以下载?", false, false);
                map.AddTBString(FileInfoAttr.UploadPath, null, "文件存放路径", true, false, 0, 2000, 300);
                map.AddTBString(FileInfoAttr.FileExt, null, "扩展名", true, false, 0, 10, 30);
                map.AddTBFloat(FileInfoAttr.FileSize, 0, "文件大小", true, false);
                map.AddDDLSysEnum(FileInfoAttr.FileStatus, 0, "文件状态", true, true, FileInfoAttr.FileStatus, "@0=私有@1=共享给全体人员@2=共享给指定人@3=删除");
                map.AddBoolean(FileInfoAttr.IsShare, false, "是否共享", true, false);
                map.AddTBInt(FileInfoAttr.IsEnable, 1, "是否启用", false, false);
                map.AddDDLSysEnum(FileInfoAttr.KMTreeCtrlWay, 0, "该节点访问控制方式", true, true,
                    "KMTreeCtrlWay", "@0=任何人@1=继承@2=按岗位@3=按部门@4=按人员@5=按岗位与部门的交集@6=按SQL");
                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        protected override bool beforeInsert()
        {
            if (string.IsNullOrEmpty(this.MyPK))
            {
                this.MyPK = System.Guid.NewGuid().ToString();
                this.FK_Emp = BP.Web.WebUser.No;
                this.FK_Dept = BP.Web.WebUser.FK_Dept;
                this.RDT = DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd");
                this.EDT = DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd");
            }

            return base.beforeInsert();
        }
        protected override bool beforeUpdateInsertAction()
        {
            string sql = "SELECT COUNT(*) AS num from KM_FileInfo where RefTreeNo='" + this.RefTreeNo + "'";
            Tree en = new Tree(this.RefTreeNo);
            en.NumOfInfos = BP.DA.DBAccess.RunSQLReturnValInt(sql);
            sql = "SELECT SUM(FileSize) FileSize from KM_FileInfo where RefTreeNo='" + this.RefTreeNo + "'";
            en.FileSize = BP.DA.DBAccess.RunSQLReturnValFloat(sql, 0);
            en.Update();

            return base.beforeUpdateInsertAction();
        }
        protected override void afterDelete()
        {
            string sql = "SELECT COUNT(*) AS num from KM_FileInfo where RefTreeNo='" + this.RefTreeNo + "'";
            Tree en = new Tree(this.RefTreeNo);
            en.NumOfInfos = BP.DA.DBAccess.RunSQLReturnValInt(sql);
            sql = "SELECT SUM(FileSize) FileSize from KM_FileInfo where RefTreeNo='" + this.RefTreeNo + "'";
            en.FileSize = BP.DA.DBAccess.RunSQLReturnValFloat(sql, 0);
            en.Update();

            base.afterDelete();
        }
        #endregion 重写方法
    }
    /// <summary>
    /// 知识
    /// </summary>
    public class FileInfos : SimpleNoNames
    {
        /// <summary>
        /// 知识s
        /// </summary>
        public FileInfos() { }


        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new FileInfo();
            }
        }
    }
}
