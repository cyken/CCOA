﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;

namespace BP.OA.Meeting
{
    public class RoomResourceAttr : EntityNoNameAttr
    {
    }
    public class RoomResource : EntityNoName
    {

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_RoomResource");
                map.EnDesc = "会议室资源";
                map.CodeStruct = "3";
                map.IsAllowRepeatName = false;

                map.AddTBStringPK(RoomResourceAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(RoomResourceAttr.Name, null, "名称", true, false, 1, 100, 30, false);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    public class RoomResources : EntitiesNoName
    {
        /// </summary>
        public RoomResources() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new RoomResource();
            }
        }
    }
}
