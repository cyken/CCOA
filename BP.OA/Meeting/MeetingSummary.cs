﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.DA;
using System.Data;

namespace BP.OA.Meeting
{
    public class MeetingSummaryAttr : EntityOIDAttr
    {
        /// <summary>
        /// 发起的会议OID
        /// </summary>
        public const string FK_OID = "FK_OID";
        /// <summary>
        /// 会议纪要内容
        /// </summary>
        public const string Content = "Content";
        /// <summary>
        /// 会议纪要发送人
        /// </summary>
        public const string SenderEmps = "SenderEmps";
        /// <summary>
        /// 会议纪要发送部门
        /// </summary>
        public const string SenderDepts = "SenderDepts";
        /// <summary>
        /// 附件
        /// </summary>
        public const string Attachment = "Attachment";
        /// <summary>
        /// 会议记录员
        /// </summary>
        public const string Recorder = "Recorder";
        //发送时间
        public const string SendDate = "SendDate";
        //附件名称
        public const string FileName = "FileName";
    }
    public class MeetingSummary : EntityOID
    {
        #region 属性
        public string FK_OID
        {
            get
            {
                return this.GetValStrByKey(MeetingSummaryAttr.FK_OID);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.FK_OID, value);
            }
        }
        public string Content
        {
            get
            {
                return this.GetValStrByKey(MeetingSummaryAttr.Content);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.Content, value);
            }
        }
        public string SenderEmps
        {
            get
            {
                return this.GetValStrByKey(MeetingSummaryAttr.SenderEmps);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.SenderEmps, value);
            }
        }
        public string Attachment
        {
            get
            {
                return this.GetValStrByKey(MeetingSummaryAttr.Attachment);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.Attachment, value);
            }
        }
        public string SenderDepts
        {
            get
            {
                return this.GetValStrByKey(MeetingSummaryAttr.SenderDepts);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.SenderDepts, value);
            }
        }
        public string Recorder
        {
            get
            {
                return this.GetValStrByKey(MeetingSummaryAttr.Recorder);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.Recorder, value);
            }
        }
        //附件名称
        public string FileName
        {
            get
            {
                return this.GetValStrByKey(MeetingSummaryAttr.FileName);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.FileName, value);
            }
        }
        //发送时间	
        public DateTime DateTo
        {
            get
            {
                return this.GetValDateTime(MeetingSummaryAttr.SendDate);
            }
            set
            {
                this.SetValByKey(MeetingSummaryAttr.SendDate, value);
            }
        }
        #endregion

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_MeetingSummary");
                map.EnDesc = "会议纪要";
                map.CodeStruct = "9";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBIntPKOID();
                map.AddTBString(MeetingSummaryAttr.FK_OID, string.Empty, "发起的会议OID", true, false, 0, 10, 30);
                map.AddTBString(MeetingSummaryAttr.Content, string.Empty, "纪要内容", true, false, 0, 4000, 500);
                map.AddTBString(MeetingSummaryAttr.SenderEmps, string.Empty, "纪要发送人", true, false, 0, 4000, 500);
                map.AddTBString(MeetingSummaryAttr.SenderDepts, string.Empty, "纪要发送给哪个部门", true, false, 0, 4000, 500);
                map.AddTBString(MeetingSummaryAttr.Attachment, string.Empty, "附件", true, false, 0, 4000, 500);
                map.AddTBString(MeetingSummaryAttr.Recorder, string.Empty, "会议记录员", true, false, 0, 10, 50);
                map.AddTBString(MeetingSummaryAttr.FileName, string.Empty, "附件名", true, false, 0, 200, 200);
                map.AddTBDateTime(MeetingSummaryAttr.SendDate, null, "发送时间", true, false);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 预定信息
    /// </summary>
    public class MeetingSummaries : EntitiesOID
    {
        /// <summary>
        /// 预定信息s
        /// </summary>
        public MeetingSummaries() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new MeetingSummary();
            }
        }
        /// <summary>
        /// 根据预定编号，查询会议纪要
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        public static DataTable GetMeetringSummary(string oid)
        {
            string sql = "select SenderEmps from OA_MeetingSummary where fk_oid=" + oid;
            DataTable isExitDt = DBAccess.RunSQLReturnTable(sql);
            string trueName = "'" + "无" + "'";
            bool isExitBool = false;
            if (isExitDt.Rows.Count > 0)
            {
                trueName = null;
                string getNoSql = DBAccess.RunSQLReturnTable(sql).Rows[0]["SenderEmps"].ToString();
                string[] getNoSqlArray = getNoSql.Split(',');
                string biaodian = ",";
                string getNo = null;
                for (int i = 0; i < getNoSqlArray.Length - 1; i++)
                {
                    if (i == getNoSqlArray.Length - 2)
                    {
                        biaodian = null;
                    }
                    getNo += "'" + getNoSqlArray[i] + "'" + biaodian;
                }
                string trueNameSql = string.Format("SELECT Name from port_emp WHERE No IN ({0})", getNo);


                DataTable dt = DBAccess.RunSQLReturnTable(trueNameSql);
                biaodian = ",";
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (j == dt.Rows.Count - 1)
                    {
                        biaodian = null;
                    }
                    trueName += "'" + dt.Rows[j]["Name"] + "'" + biaodian;
                }
                isExitBool = true;
            }
            if (isExitBool)
            {
                trueName = '"' + trueName +'"';
            }
            string strSQL = "select OID,FK_OID,Content," + trueName + " as SenderEmps,SenderDepts,Attachment,Recorder,"
                             + "FileName,SendDate from OA_MeetingSummary where fk_oid=" + oid;

            DataTable TrueDt = BP.DA.DBAccess.RunSQLReturnTable(strSQL);
            return TrueDt;
        }
    }
}
