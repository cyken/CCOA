﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 短消息类别属性
    /// </summary>
    public class UserConfigAttr : EntityOIDAttr
    {
        public const String ConfigGroup = "ConfigGroup";
        public const String ConfigItem = "ConfigItem";
        public const String AddTime = "AddTime";
        public const String Config = "Config";
        public const String State = "State";
    }
    /// <summary>
    /// 短消息类别
    /// </summary>
    public partial class UserConfig : EntityOID
    {
        #region 属性
        public string ConfigGroup
        {
            get { return this.GetValStrByKey(UserConfigAttr.ConfigGroup); }
            set { this.SetValByKey(UserConfigAttr.ConfigGroup, value); }
        }
        public string ConfigItem
        {
            get { return this.GetValStrByKey(UserConfigAttr.ConfigItem); }
            set { this.SetValByKey(UserConfigAttr.ConfigItem, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(UserConfigAttr.AddTime); }
            set { this.SetValByKey(UserConfigAttr.AddTime, value); }
        }
        public string Config
        {
            get { return this.GetValStrByKey(UserConfigAttr.Config); }
            set { this.SetValByKey(UserConfigAttr.Config, value); }
        }
        public int State
        {
            get { return this.GetValIntByKey(UserConfigAttr.State); }
            set { this.SetValByKey(UserConfigAttr.State, value); }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 短消息类别
        /// </summary>
        public UserConfig() { }
        /// <summary>
        /// 短消息类别
        /// </summary>
        /// <param name="no">编号</param>
        public UserConfig(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_UserConfig";
                map.EnDesc = "短消息文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBString(UserConfigAttr.ConfigGroup, null, "配置组", true, false, 2, 50, 20);
                map.AddTBString(UserConfigAttr.ConfigItem, null, "发布对象", true, false, 2, 50, 20);
                map.AddTBDateTime(UserConfigAttr.AddTime, null, "添加时间", true, false);
                map.AddTBStringDoc(UserConfigAttr.Config, null, "设置", true, false);
                map.AddTBInt(UserConfigAttr.State, 1, "状态", true, false);

                //map.AddTBString(UserConfigAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(UserConfigAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        public static void SetConfigItem0(string group, string item, string key, object value)
        {
            BP.OA.UserConfig uc = new BP.OA.UserConfig();
            bool bln = uc.RetrieveByAttrAnd(UserConfigAttr.ConfigGroup, group, UserConfigAttr.ConfigItem, item);
            if (bln)
            {
                string config = uc.Config;
                String[] arr_config = config.Split(new String[] { ",", ";", "|", ":" }, StringSplitOptions.None);
                int i = 0;
                for (; i < arr_config.Length; i += 2)
                {
                    if (key.ToLower() == arr_config[i].ToLower())
                    {
                        arr_config[i + 1] = Convert.ToString(value);
                    }
                }
                config = String.Join("|", arr_config);
                if (i >= arr_config.Length)
                {
                    config += String.Format("|{0}|{1}", key, Convert.ToString(value));
                }
                uc.Config = config;
                uc.Update();
            }
            else
            {
                uc.Config = String.Format("{0}|{1}", key, Convert.ToString(value));
                uc.AddTime = DateTime.Now;
                uc.ConfigGroup = group;
                uc.ConfigItem = item;
                uc.Insert();
            }
        }
        public static String GetConfigItem0(string group, string item, string key)
        {
            BP.OA.UserConfig uc = new BP.OA.UserConfig();
            bool bln = uc.RetrieveByAttrAnd(UserConfigAttr.ConfigGroup, group, UserConfigAttr.ConfigItem, item);
            if (bln)
            {
                string config = uc.Config;
                if (String.IsNullOrEmpty(config))
                    return null;
                String[] arr_config = config.Split(new String[] { ",", ";", "|", ":" }, StringSplitOptions.None);
                for (int i = 0; i < arr_config.Length; i += 2)
                {
                    if (key.ToLower() == arr_config[i].ToLower())
                    {
                        return arr_config[i + 1];
                    }
                }
                return null;
            }
            else
                return null;
        }
        public static void SetConfigItem(string key, object value)
        {
            BP.Sys.UserRegedit en = new BP.Sys.UserRegedit();
            en.MyPK = BP.Web.WebUser.No + key;
            en.FK_Emp = BP.Web.WebUser.No;
            en.CfgKey = key;
            en.Vals = value.ToString();
            en.Save();
        }
        public static String GetConfigItem(string key)
        {
            BP.Sys.UserRegedit en = new BP.Sys.UserRegedit();
            en.MyPK = BP.Web.WebUser.No + key;
            int iR = en.RetrieveFromDBSources();
            if (iR == 0)
                return null;
            else
                return en.Vals;
        }
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class UserConfigs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new UserConfig();
            }
        }
        /// <summary>
        /// 短消息类别集合
        /// </summary>
        public UserConfigs()
        {
        }
    }
}
