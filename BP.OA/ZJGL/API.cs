﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.ZJGL
{
    public class API
    {
        #region 年检 api.
        /// <summary>
        /// 年检流程是否启用?
        /// </summary>
        public static bool Certificates_Use_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("Certificates_Use_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 证件 流程标记
        /// </summary>
        public static string Certificates_Use_FlowMark
        {
            get
            {
                return "CertificatesUse";
            }
        }

        /// <summary>
        /// 印鉴使用API
        /// </summary>
        /// <param name="sealtype">印鉴类型 </param>
        /// <param name="usetime">使用时间</param>
        /// <param name="userid">使用人</param>
        /// <param name="usereason">事由</param>
        /// <param name="note">备注</param>
        /// <param name="workid">WorkID</param>
        /// <returns>使用结果，如果失败就抛出异常.</returns>
        public static string Certificates_Use(int usetime, string userid, string deptno,string usereason, string ctype,string cname,string BeginDT,string EndDT,
            Int64 workid)
        {

            CertificatesUse en = new CertificatesUse();
            try
            {
               
                en.UseTime = usetime;
                en.UserID = userid;
                en.UseReason = usereason;
                en.BeginDate = BeginDT;
                en.EndDate = EndDT;
                en.WorkID = workid;
                en.CType = ctype;
                en.FK_Certificates = cname;
                en.FK_Dept = deptno;
                en.Insert();

                Certificate es = new Certificate();
                es.No = cname;
                es.Retrieve();// 从数据库获取对应No的证件信息
                es.OperatorName = userid;
                es.RegistrationDate = EndDT;
                es.Update();// 更新信息

                return "证件使用记录成功...";
            }
            catch (Exception ex)
            {
                en.Delete();
                return "@印鉴使用失败:" + ex.Message;
            }
        }
        #endregion 采购api.
    }
}

