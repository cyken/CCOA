﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.Port;
using BP.Web;

namespace BP.OA.HR
{
    /// <summary>
    /// 福利 属性
    /// </summary>
    public class FuLiAttr : EntityOIDAttr
    {
        #region 基本属性
        /// <summary>
        /// 部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 单位
        /// </summary>
        public const string FK_DanWei = "FK_DanWei";
        /// <summary>
        /// 姓名
        /// </summary>
        public const string FK_Name = "FK_Name";
        /// <summary>
        /// 月份
        /// </summary>
        public const string Mouth = "Mouth";
        /// <summary>
        /// 新社保基数
        /// </summary>
        public const string XinSheBaoJiShu = "XinSheBaoJiShu";
        /// <summary>
        /// 新医保基数
        /// </summary>
        public const string XinYiBaoJiShu = "XinYiBaoJiShu";
        /// <summary>
        /// 养老个人缴费
        /// </summary>
        public const string YanLaoGeRenJiaoFei = "YanLaoGeRenJiaoFei";
        /// <summary>
        /// 养老单位缴费
        /// </summary>
        public const string YanLaoDanWeiJiaoFei = "YanLaoDanWeiJiaoFei";
        /// <summary>
        /// 养老合计
        /// </summary>
        public const string YanLaoHeJi = "YanLaoHeJi";
        /// <summary>
        /// 失业个人缴费
        /// </summary>
        public const string ShiYeGeRenJiaoFei = "ShiYeGeRenJiaoFei";
        /// <summary>
        /// 失业单位缴费
        /// </summary>
        public const string ShiYeDanWeiJiaoFei = "ShiYeDanWeiJiaoFei";
        /// <summary>
        /// 失业合计
        /// </summary>
        public const string ShiYeHeJi = "ShiYeHeJi";
        /// <summary>
        /// 医疗个人缴费
        /// </summary>
        public const string YiLiaoGeRenJiaoFei = "YiLiaoGeRenJiaoFei";
        /// <summary>
        /// 医疗单位缴费
        /// </summary>
        public const string YiLiaoDanWeiJiaoFei = "YiLiaoDanWeiJiaoFei";
        /// <summary>
        /// 医疗合计
        /// </summary>
        public const string YiLiaoHeJi = "YiLiaoHeJi";
        /// <summary>
        /// 工伤单位缴费
        /// </summary>
        public const string GongShangDanWeiJiaoFei = "GongShangDanWeiJiaoFei";
        /// <summary>
        /// 生育单位缴费
        /// </summary>
        public const string ShengYuDanWeiJiaoFei = "ShengYuDanWeiJiaoFei";
        /// <summary>
        /// 单位缴费合计
        /// </summary>
        public const string DanWeiJiaoFeiHeJi = "DanWeiJiaoFeiHeJi";
        /// <summary>
        /// 个人缴费合计
        /// </summary>
        public const string GeRenJiaoFeiHeJi = "GeRenJiaoFeiHeJi";
        /// <summary>
        /// 合计
        /// </summary>
        public const string HeJi = "HeJi";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
        /// <summary>
        /// 年份
        /// </summary>
        public const string NianFen = "NianFen";
        /// <summary>
        /// 月份
        /// </summary>
        public const string YueFen = "YueFen";
        #endregion
    }
    /// <summary>
    /// 福利
    /// </summary>
    public class FuLi : EntityOID
    {
        /// <summary>
        /// 年份
        /// </summary>
        public string NianFen
        {
            get { return this.GetValStringByKey(FuLiAttr.NianFen); }
            set { this.SetValByKey(FuLiAttr.NianFen, value); }
        }
        /// <summary>
        /// 月份
        /// </summary>
        public string YueFen
        {
            get { return this.GetValStringByKey(FuLiAttr.YueFen); }
            set { this.SetValByKey(FuLiAttr.YueFen, value); }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept
        {
            get { return this.GetValStringByKey(FuLiAttr.FK_Dept); }
            set { this.SetValByKey(FuLiAttr.FK_Dept, value); }
        }
        /// <summary>
        /// 单位
        /// </summary>
        public string FK_DanWei
        {
            get { return this.GetValStringByKey(FuLiAttr.FK_DanWei); }
            set { this.SetValByKey(FuLiAttr.FK_DanWei, value); }
        }
        /// <summary>
        /// 姓名
        /// </summary>
        public string FK_Name
        {
            get { return this.GetValStringByKey(FuLiAttr.FK_Name); }
            set { this.SetValByKey(FuLiAttr.FK_Name, value); }
        }

        /// <summary>
        /// 月份
        /// </summary>
        public string Mouth
        {
            get { return this.GetValStringByKey(FuLiAttr.Mouth); }
            set { this.SetValByKey(FuLiAttr.Mouth, value); }
        }
        /// <summary>
        /// 新社保基数
        /// </summary>
        public float XinSheBaoJiShu
        {
            get { return this.GetValFloatByKey(FuLiAttr.XinSheBaoJiShu); }
            set { this.SetValByKey(FuLiAttr.XinSheBaoJiShu, value); }
        }
        /// <summary>
        /// 新医保基数
        /// </summary>
        public float XinYiBaoJiShu
        {
            get { return this.GetValFloatByKey(FuLiAttr.XinYiBaoJiShu); }
            set { this.SetValByKey(FuLiAttr.XinYiBaoJiShu, value); }
        }
        /// <summary>
        /// 养老个人缴费
        /// </summary>
        public float YanLaoGeRenJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.YanLaoGeRenJiaoFei); }
            set { this.SetValByKey(FuLiAttr.YanLaoGeRenJiaoFei, value); }
        }
        /// <summary>
        /// 养老单位缴费
        /// </summary>
        public float YanLaoDanWeiJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.YanLaoDanWeiJiaoFei); }
            set { this.SetValByKey(FuLiAttr.YanLaoDanWeiJiaoFei, value); }
        }
        /// <summary>
        /// 养老合计
        /// </summary>
        public float YanLaoHeJi
        {
            get { return this.GetValFloatByKey(FuLiAttr.YanLaoHeJi); }
            set { this.SetValByKey(FuLiAttr.YanLaoHeJi, value); }
        }
        /// <summary>
        /// 失业个人缴费
        /// </summary>
        public float ShiYeGeRenJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.ShiYeGeRenJiaoFei); }
            set { this.SetValByKey(FuLiAttr.ShiYeGeRenJiaoFei, value); }
        }
        /// <summary>
        /// 失业单位缴费
        /// </summary>
        public float ShiYeDanWeiJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.ShiYeDanWeiJiaoFei); }
            set { this.SetValByKey(FuLiAttr.ShiYeDanWeiJiaoFei, value); }
        }
        /// <summary>
        /// 失业合计
        /// </summary>
        public float ShiYeHeJi
        {
            get { return this.GetValFloatByKey(FuLiAttr.ShiYeHeJi); }
            set { this.SetValByKey(FuLiAttr.ShiYeHeJi, value); }
        }
        /// <summary>
        /// 医疗个人缴费
        /// </summary>
        public float YiLiaoGeRenJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.YiLiaoGeRenJiaoFei); }
            set { this.SetValByKey(FuLiAttr.YiLiaoGeRenJiaoFei, value); }
        }
        /// <summary>
        /// 医疗单位缴费
        /// </summary>
        public float YiLiaoDanWeiJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.YiLiaoDanWeiJiaoFei); }
            set { this.SetValByKey(FuLiAttr.YiLiaoDanWeiJiaoFei, value); }
        }
        /// <summary>
        /// 医疗合计
        /// </summary>
        public float YiLiaoHeJi
        {
            get { return this.GetValFloatByKey(FuLiAttr.YiLiaoHeJi); }
            set { this.SetValByKey(FuLiAttr.YiLiaoHeJi, value); }
        }
        /// <summary>
        /// 工伤单位缴费
        /// </summary>
        public float GongShangDanWeiJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.GongShangDanWeiJiaoFei); }
            set { this.SetValByKey(FuLiAttr.GongShangDanWeiJiaoFei, value); }
        }
        /// <summary>
        /// 生育单位缴费
        /// </summary>
        public float ShengYuDanWeiJiaoFei
        {
            get { return this.GetValFloatByKey(FuLiAttr.ShengYuDanWeiJiaoFei); }
            set { this.SetValByKey(FuLiAttr.ShengYuDanWeiJiaoFei, value); }
        }
        /// <summary>
        /// 单位缴费合计
        /// </summary>
        public float DanWeiJiaoFeiHeJi
        {
            get { return this.GetValFloatByKey(FuLiAttr.DanWeiJiaoFeiHeJi); }
            set { this.SetValByKey(FuLiAttr.DanWeiJiaoFeiHeJi, value); }
        }
        /// <summary>
        /// 个人缴费合计
        /// </summary>
        public float GeRenJiaoFeiHeJi
        {
            get { return this.GetValFloatByKey(FuLiAttr.GeRenJiaoFeiHeJi); }
            set { this.SetValByKey(FuLiAttr.GeRenJiaoFeiHeJi, value); }
        }
        /// <summary>
        /// 合计
        /// </summary>
        public float HeJi
        {
            get { return this.GetValFloatByKey(FuLiAttr.HeJi); }
            set { this.SetValByKey(FuLiAttr.HeJi, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get { return this.GetValStringByKey(FuLiAttr.BeiZhu); }
            set { this.SetValByKey(FuLiAttr.BeiZhu, value); }
        }

        #region 构造函数
        public FuLi()
        { }
        public FuLi(int oid)
            : base(oid)
        {

        }
        #endregion
        #region 重写父类的方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                return uac.OpenForSysAdmin();
            }
        }
        protected override bool beforeUpdateInsertAction()
        {
            //Port.Emp e = new Emp(this.FK_Name);
            this.YanLaoHeJi = this.YanLaoDanWeiJiaoFei + this.YanLaoGeRenJiaoFei;
            this.ShiYeHeJi = this.ShiYeGeRenJiaoFei + this.YiLiaoDanWeiJiaoFei;
            this.YiLiaoHeJi = this.YiLiaoDanWeiJiaoFei + this.YiLiaoGeRenJiaoFei;
            this.DanWeiJiaoFeiHeJi = this.YanLaoDanWeiJiaoFei + this.ShiYeDanWeiJiaoFei + this.YiLiaoDanWeiJiaoFei + this.GongShangDanWeiJiaoFei + this.ShengYuDanWeiJiaoFei;
            this.GeRenJiaoFeiHeJi = this.YanLaoGeRenJiaoFei + this.ShiYeGeRenJiaoFei + this.YiLiaoGeRenJiaoFei;
            this.HeJi = this.DanWeiJiaoFeiHeJi + this.GeRenJiaoFeiHeJi;
            //this.FK_Name = e.Name;
            //this.FK_Dept = e.FK_Dept;
            return base.beforeUpdateInsertAction();
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_FuLi");
                map.EnDesc = "福利签订";
                //自动编号
                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;
                map.AddTBIntPKOID();
                //字段
                map.AddTBString(FuLiAttr.FK_DanWei, null, "单位", true, false, 1, 50, 100);
                map.AddDDLEntities(FuLiAttr.FK_Dept, null, "部门", new Depts(), true);
                map.AddDDLEntities(FuLiAttr.FK_Name, null, "姓名", new BP.OA.HR.HRRecords(), true);
                map.AddDDLSysEnum(FuLiAttr.NianFen, 0, "年份", true, true, FuLiAttr.NianFen, "@0=2010@1=2011@2=2012@3=2013@4=2014@5=2015");
                map.AddDDLSysEnum(FuLiAttr.YueFen, 0, "月份", true, true, FuLiAttr.YueFen, "@0=1@1=2@2=3@3=4@4=5@5=6@6=7@7=8@8=9@9=10@10=11@11=12");
                map.AddTBMoney(FuLiAttr.XinSheBaoJiShu, 0, "新社保基数", true, false);
                map.AddTBMoney(FuLiAttr.XinYiBaoJiShu, 0, "新医保基数", true, false);
                map.AddTBMoney(FuLiAttr.YanLaoGeRenJiaoFei, 0, "养老个人缴费", true, false);
                map.AddTBMoney(FuLiAttr.YanLaoDanWeiJiaoFei, 0, "养老单位缴费", true, false);
                map.AddTBMoney(FuLiAttr.ShiYeGeRenJiaoFei, 0, "失业个人缴费", true, false);
                map.AddTBMoney(FuLiAttr.ShiYeDanWeiJiaoFei, 0, "失业单位缴费", true, false);
                map.AddTBMoney(FuLiAttr.YiLiaoGeRenJiaoFei, 0, "医疗个人缴费", true, false);
                map.AddTBMoney(FuLiAttr.YiLiaoDanWeiJiaoFei, 0, "医疗单位缴费", true, false);
                map.AddTBMoney(FuLiAttr.GongShangDanWeiJiaoFei, 0, "工伤单位缴费", true, false);
                map.AddTBMoney(FuLiAttr.ShengYuDanWeiJiaoFei, 0, "生育单位缴费", true, false);
                map.AddTBMoney(FuLiAttr.YanLaoHeJi, 0, "养老合计", true, true);
                map.AddTBMoney(FuLiAttr.ShiYeHeJi, 0, "失业合计", true, true);
                map.AddTBMoney(FuLiAttr.YiLiaoHeJi, 0, "医疗合计", true, true);
                map.AddTBMoney(FuLiAttr.DanWeiJiaoFeiHeJi, 0, "单位缴费合计", true, true);
                map.AddTBMoney(FuLiAttr.GeRenJiaoFeiHeJi, 0, "个人缴费合计", true, true);
                map.AddTBString(FuLiAttr.HeJi, null, "合计", true, true, 1, 50, 100);
                map.AddTBStringDoc(FuLiAttr.BeiZhu, null, "备注", true, false, true);
                //map.AddMyFile("福利");
                //查询条件
                //map.AddSearchAttr(FuLiAttr.FK_Dept);
                map.AddSearchAttr(FuLiAttr.YueFen);
                map.AddSearchAttr(FuLiAttr.FK_Name);
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    /// 福利s
    /// </summary>
    public class FuLis : EntitiesOID
    {
        #region 构造函数

        public FuLis()
        {

        }
        #endregion

        #region 重写父类方法
        public override Entity GetNewEntity
        {
            get { return new FuLi(); }
        }

        #endregion
    }
}
