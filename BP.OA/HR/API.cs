﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.HR
{
    /// <summary>
    /// 合同API
    /// </summary>
    public static class API
    {
        #region 解除合同 api.
        /// <summary>
        /// 解除合同流程是否启用?
        /// </summary>
        public static bool HR_ContractRemove_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("HR_ContractRemove_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 解除合同的流程标记
        /// </summary>
        public static string HR_ContractRemove_FlowMark
        {
            get
            {
                return "HRContractRemove";
            }
        }
        /// <summary>
        /// 解除合同
        /// </summary>
        /// <param name="FK_HRContract"></param>
        /// <param name="FK_HRRecord"></param>
        /// <param name="DengJiRen"></param>
        /// <param name="DengJiDT"></param>
        /// <param name="GuDingQiXian"></param>
        /// <param name="StartDT"></param>
        /// <param name="EndDT"></param>
        /// <param name="RemoveDT"></param>
        /// <param name="Note"></param>
        /// <returns></returns>
        public static string HR_ContractRemove(string FK_HRContract, string FK_HRRecord, string DengJiRen,
            string DengJiDT, string GuDingQiXian, string StartDT, string EndDT, string RemoveDT, string Note)
        {
            BP.OA.HR.
            HRContractRemove cr = new HRContractRemove();
            cr.FK_HRContract = FK_HRContract;
            cr.FK_HRRecord = FK_HRRecord;
            cr.DengJiRen = DengJiRen;
            cr.DengJiDT = DengJiDT;
            cr.GuDingQiXian = GuDingQiXian;
            cr.StartDT = StartDT;
            cr.EndDT = EndDT;
            cr.RemoveDT = RemoveDT;
            cr.Note = Note;
            cr.Insert();
            return "调用api成功...";
        }
        #endregion 解除合同api.

        #region 培训计划 api.
        /// <summary>
        /// 培训计划流程是否启用?
        /// </summary>
        public static bool TrainingPlan_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("  TrainingPlan_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 培训计划的流程标记
        /// </summary>
        public static string TrainingPlan_FlowMark
        {
            get
            {
                return "TrainingPlan";
            }
        }
         //<summary>
         //培训计划
         //</summary>
         //<param name="FK_HRContract"></param>
         //<param name="FK_HRRecord"></param>
         //<param name="DengJiRen"></param>
         //<param name="DengJiDT"></param>
         //<param name="GuDingQiXian"></param>
         //<param name="StartDT"></param>
         //<param name="EndDT"></param>
         //<param name="RemoveDT"></param>
         //<param name="Note"></param>
         //<returns></returns>
        //public static string TrainingPlan(string Year, string Dept, string CreateUser,
        //    string RecordDT, string Types, string Name, string DataTimes, string DuiXiang, string ShiShug, string FeiYong, string BeiZhu)
        //{
        //    BP.OA.HR.
        //    TrainingPlans cr = new TrainingPlans();
        //    cr.Year = Year;
        //    cr.Dept = Dept;
        //    cr.CreateUser = CreateUser;
        //    cr.RecordDT = RecordDT;
        //    cr.Type = Types;
        //    cr.Name = Name;
        //    cr.DataTime = DataTimes;

        //    cr.DuiXiang = DuiXiang;
        //    cr.ShiShug = ShiShug;
        //    cr.FeiYong = FeiYong;
        //    cr.BeiZhu = BeiZhu;
        //    cr.Insert();
        //    return "调用api成功...";
        //}
        #endregion 培训计划api.

        #region 请假 API
        /// <summary>
        /// 请假申请流程是否启用?
        /// </summary>
        public static bool AskForLeave_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("AskForLeave_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 请假的流程标记
        /// </summary>
        public static string AskForLeave_FlowMark
        {
            get
            {
                return "AskForLeave";
            }
        }
        /// <summary>
        /// 请假API
        /// </summary>
        /// <param name="fk_dsMain">人员</param>
        /// <param name="lx">类型</param>
        /// <param name="bm">部门</param>
        /// <param name="gw">岗位</param>
        /// <param name="sjc">时间从</param>
        /// <param name="sjd">时间到</param>
        /// <param name="xs">请假小时</param>
        /// <param name="yy">原因</param>

        /// <returns></returns>
        public static string AskForLeave(string fk_dsMain, string lx, string sj, string bm,
            string sjc, string sjd, string xs, string yy)
        {
            HRAskForLeave en = new HRAskForLeave();
            en.Fk_HrRecord = fk_dsMain;
            en.AskCate = lx;
            en.Fk_NY = sj;
            en.Fk_Dept = bm;
            en.StartDT = sjc;
            en.EndDT = sjd;
            en.Hours = xs;
            en.AskWhy = yy;
            
            en.Insert();
            return "调用api成功...";
        }
        #endregion 请假api.





        #region  人员调动API
        /// <summary>
        /// 人员调动流程是否启用?
        /// </summary>
        public static bool Transfer_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("Transfer_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 人员调动的流程标记
        /// </summary>
        public static string Transfer_FlowMark
        {
            get
            {
                return "Transfer";
            }
        }
        /// <summary>
        /// 人员调动API
        /// </summary>
        /// <param name="fk_dsMain">人员</param>
        /// <param name="diaodDT">调动日期</param>
        /// <param name="ybm">原部门</param>
        /// <param name="yzw">原职务</param>
        /// <param name="xbm">现在部门</param>
        /// <param name="xzw">现在职务</param>
        /// <param name="diaoboWhy">调动原因</param>
        /// <param name="note">备注</param>
        /// <returns></returns>
        public static string Transfer(string fk_dsMain, string diaodDT, string ybm, string yzw,
            string xbm, string xzw, string diaoboWhy, string note)
        {

            HRTransfer en = new HRTransfer();
            en.Name = fk_dsMain;
            en.DiaoDongDT = diaodDT;
            en.FK_OldDept = ybm;
            en.OldZhiWu = yzw;
            en.FK_NewDept = xbm;
            en.NewZhiWu = xzw;
            en.DiaoDongWhy = diaoboWhy;
            en.Note = note;
            en.Insert();
            return "调用api成功...";
        }
        #endregion
    }
}
