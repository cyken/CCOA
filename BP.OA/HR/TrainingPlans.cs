﻿using BP.En;
using BP.Port;
using BP.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.HR
{
    /// <summary>
    /// 培训计划 属性
    /// </summary>
    public class TrainingPlansAttr : EntityOIDAttr
    {


        #region 基本属性

        /// <summary>
        /// 年份
        /// </summary>
        public const string Year = "Year";
        // <summary>
        /// 部门
        /// </summary>
        public const string Dept = "Dept";
        // <summary>
        /// 制定人
        /// </summary>Create
        public const string CreateUser = "CreateUser";
        /// <summary>
        /// 填写时间
        /// </summary>
        public const string RecordDT = "RecordDT";
        /// <summary>
        /// 培训类型
        /// </summary>
        public const string Type = "Type";
        /// <summary>
        /// 培训课程名称
        /// </summary>
        public const string Name = "Name";
        /// <summary>
        /// 培训时间
        /// </summary>
        public const string DataTime = "DataTime";
        /// <summary>
        /// 预计费用
        /// </summary>
        public const string FeiYong = "FeiYong";
        /// <summary>
        /// 预计时数
        /// </summary>
        public const string ShiShug = "ShiShug";
        /// <summary>
        /// 培训对象
        /// </summary>
        public const string DuiXiang = "DuiXiang";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";

        #endregion
    }
    /// <summary>
    /// 培训计划
    /// </summary>

    public class TrainingPlans : EntityOID
    {
        /// <summary>
        /// 年份
        /// </summary>

        public string Year
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.Year); }
            set { this.SetValByKey(TrainingPlansAttr.Year, value); }
        }
        /// <summary>
        /// 部门
        /// </summary>

        public string Dept
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.Dept); }
            set { this.SetValByKey(TrainingPlansAttr.Dept, value); }
        }
        /// <summary>
        /// 制定人
        /// </summary>

        public string CreateUser
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.CreateUser); }
            set { this.SetValByKey(TrainingPlansAttr.CreateUser, value); }
        }
        /// <summary>
        /// 制定时间
        /// </summary>

        public string RecordDT
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.RecordDT); }
            set { this.SetValByKey(TrainingPlansAttr.RecordDT, value); }
        }
        /// <summary>
        /// 培训时间
        /// </summary>

        public string DataTime
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.DataTime); }
            set { this.SetValByKey(TrainingPlansAttr.DataTime, value); }
        }
        /// <summary>
        /// 培训类型
        /// </summary>
        public string Type
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.Type); }
            set { this.SetValByKey(TrainingPlansAttr.Type, value); }
        }
        /// <summary>
        /// 培训课程名称
        /// </summary>
        public string Name
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.Name); }
            set { this.SetValByKey(TrainingPlansAttr.Name, value); }
        }
        /// <summary>
        /// 预计费用
        /// </summary>
        public string FeiYong
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.FeiYong); }
            set { this.SetValByKey(TrainingPlansAttr.FeiYong, value); }
        }

        /// <summary>
        /// 预计时数
        /// </summary>
        public string ShiShug
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.ShiShug); }
            set { this.SetValByKey(TrainingPlansAttr.ShiShug, value); }
        }
        /// <summary>
        ///  培训对象
        /// </summary>
        public string DuiXiang
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.DuiXiang); }
            set { this.SetValByKey(TrainingPlansAttr.DuiXiang, value); }
        }


        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get { return this.GetValStringByKey(TrainingPlansAttr.BeiZhu); }
            set { this.SetValByKey(TrainingPlansAttr.BeiZhu, value); }
        }
        #region 构造函数
        public TrainingPlans()
        {

        }
        #endregion
        #region 重写父类的方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                if (WebUser.No == "admin")
                {
                    uac.IsUpdate = true;
                    uac.IsDelete = true;
                    uac.IsInsert = true;
                    uac.IsView = true;
                    uac.IsAdjunct = true;
                }
                else
                {
                    uac.IsUpdate = false;
                    uac.IsDelete = false;
                    uac.IsInsert = false;
                    uac.IsView = true;
                    uac.IsAdjunct = true;
                }
                return uac;
            }
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_TrainingPlans");
                map.EnDesc = "培训计划";
                //自动编号
                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;
                map.AddTBIntPKOID();
                //字段
                map.AddTBString(TrainingPlansAttr.Year, null, "年份", true, false, 0, 100, 30, false);
                map.AddDDLEntities(TrainingPlansAttr.Dept, null, "部门", new Depts(), true);
                map.AddTBString(TrainingPlansAttr.CreateUser, null, "制定人", true, false, 0, 100, 30, false);
                map.AddTBDate(TrainingPlansAttr.RecordDT, null, "制定时间", true, true);
                map.AddDDLSysEnum(TrainingPlansAttr.Type, 0, "类型", true, true, HRContractAttr.GuDingQiXian, "@0=内训@1=外训");
                map.AddTBString(TrainingPlansAttr.Name, null, "培训课程名称", true, false, 0, 100, 30, false);
                map.AddTBDate(TrainingPlansAttr.DataTime, null, "培训时间", true, false);
                map.AddBoolean(TrainingPlansAttr.DuiXiang, false, "培训对象", true, false);
                map.AddTBString(LeaderAttr.Name, null, "预计时数", true, false, 0, 100, 30, false);
                map.AddTBMoney(TrainingPlansAttr.FeiYong, 0, "预计培训费用", true, false);
                map.AddBoolean(TrainingPlansAttr.BeiZhu, false, "备注", true, false);
                //map.AddMyFile("培训计划");
                //查询条件
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    /// 培训计划s
    /// </summary>
    public class TrainingPlanss : EntitiesOID
    {
        #region 构造函数
        public TrainingPlanss()
        {

        }
        #endregion

        #region 重写父类方法
        public override Entity GetNewEntity
        {
            get { return new TrainingPlans(); }
        }
        #endregion
    }
}
