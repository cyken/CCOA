﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using BP.DA;
using BP.En;
using BP.GPM;

namespace BP.OA
{
    /// <summary>
    /// 新闻
    /// </summary>
    public class BarOfNewsList :BarBase
    {
        #region 系统属性.
        /// <summary>
        /// 流程编号/流程标记.
        /// </summary>
        override public string No
        {
            get
            {
                return this.ToString();
            }
        }
        /// <summary>
        /// 名称
        /// </summary>
        override public string Name
        {
            get
            {
                return "新闻";
            }
        }
        /// <summary>
        /// 权限控制-是否可以查看
        /// </summary>
        override public bool IsCanView
        {
            get
            {
                return true; //任何人都可以看到.
            }
        }
        #endregion 系统属性.

        #region 外观行为.
        /// <summary>
        /// 标题
        /// </summary>
        override public string Title
        {
            get
            {
                return "新闻";
            }
        }
        /// <summary>
        /// 更多连接
        /// </summary>
        override public string More
        {
            get
            {
                return "<a href='../../App/News/List.htm' target=_self >更多</a>";
            }
        }
        /// <summary>
        /// 内容信息
        /// </summary>
        override public string Documents
        {
            get
            {
                string sql = "SELECT OID,Title,RDT FROM OA_News ORDER BY RDT DESC ";
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                if (dt.Rows.Count == 0)
                    return "新闻为 0 ";

                string html = "<table>";
                Int32 count = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string pk = dr["OID"].ToString();
                    string title = dr["Title"].ToString();
                    string rdt = dr["RDT"].ToString();
                    count++;
                    if (count > 6)
                        html += "<tr><td><a href='../../App/News/List.htm?OID=" + pk + "&1=2' target=_blank >数量(" + count + ")更多....</td></tr>";
                    else
                        html += "<tr><td><a href='../../App/News/Dtl.htm?OID=" + pk + "&1=2' target=_blank>" + title + "</a></td></tr>";
                }
                html += "</table>";
                return html;
            }
        }
        /// <summary>
        /// 宽度
        /// </summary>
        override public string Width
        {
            get
            {
                return "300";
            }
        }
        /// <summary>
        /// 高度
        /// </summary>
        override public string Height
        {
            get
            {
                return "200";
            }
        }

        public override bool IsLine
        {
            get
            {
                return false;  
            }
        }
        #endregion 外观行为.
    }
}
