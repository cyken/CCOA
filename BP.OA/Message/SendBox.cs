﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Message
{
    /// <summary>
    /// 发件箱属性
    /// </summary>
    public class SendBoxAttr : EntityMyPKAttr
    {
        /// <summary>
        /// 发送人
        /// </summary>
        public const String Sender = "Sender";
        /// <summary>
        /// 接受人
        /// </summary>
        public const String Receiver = "Receiver";
        /// <summary>
        /// 抄送人
        /// </summary>
        public const String CCers = "CCers";
        /// <summary>
        /// 发送日期
        /// </summary>
        public const String SendTime = "SendTime";
        /// <summary>
        /// 内容
        /// </summary>
        public const String Docs = "Docs";
        /// <summary>
        /// 标题
        /// </summary>
        public const String Title = "Title";
        /// <summary>
        /// 是否发送.
        /// </summary>
        public const String IsSend = "IsSend";
    }
    /// <summary>
    /// 发件箱
    /// </summary>
    public partial class SendBox : EntityMyPK
    {
        #region 属性
        /// <summary>
        /// 是否发送
        /// </summary>
        public string IsSend
        {
            get { return this.GetValStrByKey(SendBoxAttr.IsSend); }
            set { this.SetValByKey(SendBoxAttr.IsSend, value); }
        }
        /// <summary>
        /// 发送人
        /// </summary>
        public string Sender
        {
            get { return this.GetValStrByKey(SendBoxAttr.Sender); }
            set { this.SetValByKey(SendBoxAttr.Sender, value); }
        }
        /// <summary>
        /// 发送时间
        /// </summary>
        public string SendTimer
        {
            get { return this.GetValStrByKey(SendBoxAttr.SendTime); }
            set { this.SetValByKey(SendBoxAttr.SendTime, value); }
        }
        /// <summary>
        /// 接受人
        /// </summary>
        public string CCers
        {
            get { return this.GetValStrByKey(SendBoxAttr.CCers); }
            set { this.SetValByKey(SendBoxAttr.CCers, value); }
        }
        /// <summary>
        /// 接收人
        /// </summary>
        public string Receiver
        {
            get { return this.GetValStrByKey(SendBoxAttr.Receiver); }
            set { this.SetValByKey(SendBoxAttr.Receiver, value); }
        }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get { return this.GetValStrByKey(SendBoxAttr.Title); }
            set { this.SetValByKey(SendBoxAttr.Title, value); }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Docs
        {
            get { return this.GetValStrByKey(SendBoxAttr.Docs); }
            set { this.SetValByKey(SendBoxAttr.Docs, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 发件箱
        /// </summary>
        public SendBox() { }
        /// <summary>
        /// 发件箱
        /// </summary>
        /// <param name="no">编号</param>
        public SendBox(string mypk) : base(mypk) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_MessageSendBox";
                map.EnDesc = "发件箱";                   // 实体的描述.

                map.AddMyPK();

                map.AddTBInt(SendBoxAttr.IsSend,0,"是否发送",true,true);
                map.AddTBString(SendBoxAttr.Receiver, null, "接收者", true, false, 0, 1000, 20);
                map.AddTBString(SendBoxAttr.CCers, null, "抄送给", true, false, 0, 1000, 20);

                map.AddTBString(SendBoxAttr.Sender, null, "发送者", true, false, 2, 50, 20);
                map.AddTBDateTime(SendBoxAttr.SendTime, null, "发送日期", true, false);

                map.AddTBString(SendBoxAttr.Title, null, "标题", true, false, 2, 50, 20);
                map.AddTBStringDoc(SendBoxAttr.Docs, null, "内容", true, false);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        protected override bool beforeInsert()
        {
            this.MyPK = BP.DA.DBAccess.GenerGUID();
            this.SendTimer = DataType.CurrentDataTime;
            return base.beforeInsert();
        }

    }
    /// <summary>
    ///发件箱s
    /// </summary>
    public class SendBoxs : EntitiesMyPK
    {
        /// <summary>
        /// 发件箱s
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new SendBox();
            }
        }
        /// <summary>
        /// 发件箱集合
        /// </summary>
        public SendBoxs()
        {
        }
        /// <summary>
        /// 获取发件箱中总条数
        /// </summary>
        /// <returns></returns>
        public string SendCountData()
        {
            SendBoxs sendboxs = new SendBoxs();
            QueryObject qo = new QueryObject(sendboxs);
            qo.AddWhere(SendBoxAttr.Sender, WebUser.No);
            qo.addAnd();
            qo.AddWhere(SendBoxAttr.IsSend, "=", 1);
            //总条数
            Int32 totalCount = qo.GetCount();

            return totalCount.ToString();

        }
        /// <summary>
        /// 获取草稿箱中总条数
        /// </summary>
        /// <returns></returns>
        public string DraftCountData()
        {
            SendBoxs sendboxs = new SendBoxs();
            QueryObject qo = new QueryObject(sendboxs);
            qo.AddWhere(SendBoxAttr.Sender, WebUser.No);
            qo.addAnd();
            qo.AddWhere(SendBoxAttr.IsSend, "=", 0);
            //总条数
            Int32 totalCount = qo.GetCount();

            return totalCount.ToString();

        }
    }
}
