using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 领取流程 - 开始节点.
    /// </summary>
    public class DailySupplieTakeND601NEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 领取流程事件
        /// </summary>
        public DailySupplieTakeND601NEE()
        {
        }
        #endregion 属性.

        #region 重写属性.
        /// <summary>
        /// 流程标记
        /// </summary>
        public override string FlowMark
        {
            get { return BP.DS.API.DailySupplie_Take_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点表单事件.
        /// <summary>
        /// 表单载入前
        /// </summary>
        public override string FrmLoadAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单载入后
        /// </summary>
        public override string FrmLoadBefore()
        {
            return null;
        }
        /// <summary>
        /// 表单保存后
        /// </summary>
        public override string SaveAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单保存前
        /// </summary>
        public override string SaveBefore()
        {

            return null;

        }
        #endregion 重写节点表单事件

        #region 重写节点运动事件.
        /// <summary>
        /// 发送前:用于检查业务逻辑是否可以执行发送，不能执行发送就抛出异常.
        /// </summary>
        public override string SendWhen()
        {
            if (this.HisNode.NodeID == 601)
            {
                //限制多次领取同一物品
                string[] GetName = new string[100];
                int GetNameInd = 0;

                //获取设计器节点表单字段,进行判断
                int KuCunShuLiang = 0;
                int ShenQingShuLiang = 0;
                string ForUse = "";

                string sql = "select * from ND601Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    //数组赋值
                    GetName[GetNameInd] = dr["OA_DSMain"].ToString();
                    GetNameInd += 1;

                    KuCunShuLiang = int.Parse(dr["KuCunShuLiang"].ToString());
                    ShenQingShuLiang = int.Parse(dr["ShenQingShuLiang"].ToString());
                    ForUse = dr["BeiZhu"].ToString();

                    if (ShenQingShuLiang > KuCunShuLiang)
                    {
                        throw new Exception("@库存不足!");
                    }
                    if (ShenQingShuLiang <= 0 || KuCunShuLiang <= 0)
                    {
                        throw new Exception("@申请数量为空或错误的数据状态");
                    }
                    if (string.IsNullOrEmpty(ForUse))
                    {
                        throw new Exception("@用途不可为空!");
                    }
                }
                //判断
                for (int i = 0; i < GetNameInd; i++)
                {
                    for (int j = i + 1; j < GetName.Length; j++)
                    {
                        if (GetName[i] == GetName[j])
                        {
                            throw new Exception("@不允许重复领取物品!");
                        }
                    }
                }

                return "合计已经在发送前事件完成.";
            }

            if (this.HisNode.NodeID == 603)
            {
                //获取设计器节点表单字段,进行判断
                int ShenQingShuLiang = 0;
                int PiZhunShuLiang = 0;

                string sql = "select * from ND601Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    ShenQingShuLiang = int.Parse(dr["ShenQingShuLiang"].ToString());
                    PiZhunShuLiang = int.Parse(dr["PiZhunShuLiang"].ToString());

                    if (PiZhunShuLiang > ShenQingShuLiang || PiZhunShuLiang < 0)
                    {
                        throw new Exception("@批准数量不可大于申请数量或数据不合法!");
                    }
                }
                return "合计已经在发送前事件完成.";
            }

            if (this.HisNode.NodeID == 602)
            {
                //获取设计器节点表单字段,进行判断
                int PiZhunShuLiang = 0;
                int ChuKuShuLiang = 0;

                string sql = "select * from ND601Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    ChuKuShuLiang = int.Parse(dr["ChuKuShuLiang"].ToString());
                    PiZhunShuLiang = int.Parse(dr["PiZhunShuLiang"].ToString());

                    if (ChuKuShuLiang > PiZhunShuLiang || ChuKuShuLiang <= 0)
                    {
                        throw new Exception("@出库数量不可大于批准数量或数据不合法!");
                    }
                }
                return "合计已经在发送前事件完成.";
            }

            return null;
        }
        /// <summary>
        /// 发送成功后
        /// </summary>
        public override string SendSuccess()
        {
            if (this.HisNode.NodeID == 601)
            {
                string sql = "select * from ND601Dtl1 where  RefPK=" + this.WorkID;


                DataTable dt = DBAccess.RunSQLReturnTable(sql);

                foreach (DataRow dr in dt.Rows)
                {
                    BP.DS.API.DailySupplieTake_OneInsert(this.WorkID,
                        dr["OA_DSMain"].ToString(),
                        int.Parse(dr["ShenQingShuLiang"].ToString()),
                        0,
                        0,
                      BP.Web.WebUser.Name,
                      "",
                      "0",
                      dr["BeiZhu"].ToString(),
                      this.GetValStr("LiShuBuMen"));
                }
                return "SendSuccess调用成功....";
            }
            if (this.HisNode.NodeID == 602)
            {
                string sql = "select * from ND601Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    //获取OID
                    string fk_DsMain = dr["OA_DSMain"].ToString();
                    string GetOidsSql = "select OID FROM  OA_DSTake WHERE WorkID='" + this.WorkID + "' AND  FK_DSMain=" + fk_DsMain;
                    DataTable GetOidDt = DBAccess.RunSQLReturnTable(GetOidsSql);
                    int oid = int.Parse(GetOidDt.Rows[0][0].ToString());

                    BP.DS.API.DailySupplieTake_TwoUpdate(
                        oid,
                        int.Parse(dr["ChuKuShuLiang"].ToString()),
                        this.GetValStr("ChuKuRen"));
                }
                return "SendSuccess调用成功....";
            }
            if (this.HisNode.NodeID == 603)
            {
                string sql = "select * from ND601Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    //获取OID
                    string fk_DsMain = dr["OA_DSMain"].ToString();
                    string DsTakeZT = "0";
                    string GetOidsSql = "select OID FROM  OA_DSTake WHERE WorkID='" + this.WorkID + "' AND  FK_DSMain=" + fk_DsMain;
                    DataTable GetOidDt = DBAccess.RunSQLReturnTable(GetOidsSql);
                    int oid = int.Parse(GetOidDt.Rows[0][0].ToString());

                    if (this.GetValStr("SHZT") == "0")
                    {
                        DsTakeZT = "1";
                    }
                    else
                    {
                        DsTakeZT = "2";
                    }

                    BP.DS.API.DailySupplieTake_ThreeUpdate(
                        oid,
                        int.Parse(dr["PiZhunShuLiang"].ToString()),
                         DsTakeZT);
                }
                return "SendSuccess调用成功....";
            }
            return null;
        }
        /// <summary>
        /// 发送失败后
        /// </summary>
        public override string SendError()
        {
            return null;
        }
        /// <summary>
        /// 退回前
        /// </summary>
        public override string ReturnBefore()
        {
            return null;
        }
        /// <summary>
        /// 退回后
        /// </summary>
        public override string ReturnAfter()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
