﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Text;
using System.Web;
using BP.DA;
using BP.Sys;
using BP.Web;
using BP.Port;
using BP.En;
using BP.WF;
using BP.WF.Template;
using BP.WF.HttpHandler;

namespace BP.OA.HttpHandler
{
    /// <summary>
    /// 页面功能实体
    /// </summary>
    public class App_Admin : DirectoryPageBase
    {
        /// <summary>
        /// 页面功能实体
        /// </summary>
        /// <param name="mycontext"></param>
        public App_Admin(HttpContext mycontext)
        {
            this.context = mycontext;
        }
           /// <summary>
        /// 构造函数
        /// </summary>
        public App_Admin()
        {
        }

        #region 执行父类的重写方法.
        /// <summary>
        /// 默认执行的方法
        /// </summary>
        /// <returns></returns>
        protected override string DoDefaultMethod()
        {
            switch (this.DoType)
            {
                case "DtlFieldUp": //字段上移
                    return "执行成功.";
                default:
                    break;
            }

            //找不不到标记就抛出异常.
            throw new Exception("@标记[" + this.DoType + "]，没有找到. @RowURL:" + context.Request.RawUrl);
        }
        #endregion 执行父类的重写方法.


        /// <summary>
        /// 执行安装.
        /// </summary>
        /// <returns></returns>
        public string DBInstall_Submit()
        {
            try
            {
                //安装ccflow .
                BP.WF.Glo.DoInstallDataBase("CH", 2);

                //升级ccflow.
                BP.WF.Glo.UpdataCCFlowVer();

                //执行GPM安装.
                BP.GPM.Glo.DoInstallDataBase();

                //执行ccoa安装.
                BP.OA.Glo.DoInstallDataBase();

                //创建一个空白的流程.
                BP.WF.Flow fl = new Flow();
                fl.DoNewFlow("100", "新建流程测试", DataStoreModel.ByCCFlow, null, null);

                //如果启用加密
                if (BP.Sys.SystemConfig.IsEnablePasswordEncryption)
                {
                    //用户密码加密
                    BP.Port.Emps emps = new BP.Port.Emps();
                    emps.RetrieveAll();
                    foreach (BP.Port.Emp emp in emps)
                    {
                        if (emp.Pass.Length < 16)
                        {
                            emp.Pass = BP.Tools.Cryptography.EncryptString(emp.Pass);
                            emp.Update();
                        }
                    }
                }

                return "url@../../Portal/Login.htm";
            }
            catch(Exception ex)
            {
                return "err@" + ex.Message;
            }
        }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        public string DBInstall_Init()
        {
            if (DBAccess.TestIsConnection() == false)
                return "err@数据库连接配置错误.";

            if (BP.DA.DBAccess.IsExitsObject("WF_Flow") == true)
                return "err@info数据库已经安装上了，您不必在执行安装. 点击: <a href='../../' >系统索引页</a>";

            Hashtable ht = new Hashtable();
            ht.Add("OSModel", (int)BP.WF.Glo.OSModel); //组织结构类型.
            ht.Add("DBType", SystemConfig.AppCenterDBType.ToString()); //数据库类型.
            ht.Add("Ver", BP.WF.Glo.Ver); //版本号.
            return BP.Tools.Json.ToJson(ht);

        }

    }
}
