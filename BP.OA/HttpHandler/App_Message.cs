﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Text;
using System.Web;
using BP.DA;
using BP.Sys;
using BP.Web;
using BP.Port;
using BP.En;
using BP.WF;
using BP.WF.Template;
using BP.WF.HttpHandler;
using BP.OA.Message;

namespace BP.OA.HttpHandler
{
    /// <summary>
    /// 页面功能实体
    /// </summary>
    public class App_Message : DirectoryPageBase
    {
        /// <summary>
        /// 页面功能实体
        /// </summary>
        /// <param name="mycontext"></param>
        public App_Message(HttpContext mycontext)
        {
            this.context = mycontext;
        }
           /// <summary>
        /// 构造函数
        /// </summary>
        public App_Message()
        {
        }

        #region 执行父类的重写方法.
        /// <summary>
        /// 默认执行的方法
        /// </summary>
        /// <returns></returns>
        protected override string DoDefaultMethod()
        {
            switch (this.DoType)
            {
                case "DtlFieldUp": //字段上移
                    return "执行成功.";
                default:
                    break;
            }

            //找不不到标记就抛出异常.
            throw new Exception("@标记[" + this.DoType + "]，没有找到. @RowURL:" + context.Request.RawUrl);
        }
        #endregion 执行父类的重写方法.

        /// <summary>
        /// 收件箱
        /// </summary>
        /// <returns></returns>
        public string InitInBox()
        {
            //关键字（标题）查询
            string searchKey = this.GetRequestVal("SearchKey");
            int pageIdx = this.GetRequestValInt("PageIdx");
            InBoxs boxs = new InBoxs();
            QueryObject qo = new QueryObject(boxs);
            qo.AddWhere(InBoxAttr.UserNo, WebUser.No);
            qo.addAnd();
            qo.AddWhere(InBoxAttr.InBoxState, "<=", 1);
            qo.addAnd();
            if (SystemConfig.AppCenterDBVarStr == "@" || SystemConfig.AppCenterDBVarStr == "?")
                qo.AddWhere(InBoxAttr.Title, " LIKE ", SystemConfig.AppCenterDBType == DBType.MySQL ? (" CONCAT('%'," + SystemConfig.AppCenterDBVarStr + "SearchKey,'%')") : (" '%'+" + SystemConfig.AppCenterDBVarStr + "SearchKey+'%'"));
            else
                qo.AddWhere(InBoxAttr.Title, " LIKE ", " '%'||" + SystemConfig.AppCenterDBVarStr + "SearchKey||'%'");
            qo.addOrderByDesc(InBoxAttr.SendTime);
            qo.MyParas.Add("SearchKey", searchKey);

            qo.DoQuery(InBoxAttr.MyPK, 15, pageIdx);
            return boxs.ToJson();
        }

        /// <summary>
        /// 发件箱
        /// </summary>
        /// <returns></returns>
        public string InitSendBox()
        {
            //关键字（标题）查询
            string searchKey = this.GetRequestVal("SearchKey");
            int pageIdx = this.GetRequestValInt("PageIdx");
            SendBoxs boxs = new SendBoxs();
            QueryObject qo = new QueryObject(boxs);
            qo.AddWhere(SendBoxAttr.Sender, WebUser.No);
            qo.addAnd();
            qo.AddWhere(SendBoxAttr.IsSend, "=", 1);
            qo.addAnd();
            if (SystemConfig.AppCenterDBVarStr == "@" || SystemConfig.AppCenterDBVarStr == "?")
                qo.AddWhere(SendBoxAttr.Title, " LIKE ", SystemConfig.AppCenterDBType == DBType.MySQL ? (" CONCAT('%'," + SystemConfig.AppCenterDBVarStr + "SearchKey,'%')") : (" '%'+" + SystemConfig.AppCenterDBVarStr + "SearchKey+'%'"));
            else
                qo.AddWhere(SendBoxAttr.Title, " LIKE ", " '%'||" + SystemConfig.AppCenterDBVarStr + "SearchKey||'%'");
            qo.addOrderByDesc(SendBoxAttr.SendTime);
            qo.MyParas.Add("SearchKey", searchKey);

            qo.DoQuery(SendBoxAttr.MyPK, 15, pageIdx);
            return boxs.ToJson();
        }

        /// <summary>
        /// 草稿箱
        /// </summary>
        /// <returns></returns>
        public string InitDraftBox()
        {
            //关键字（标题）查询
            string searchKey = this.GetRequestVal("SearchKey");
            int pageIdx = this.GetRequestValInt("PageIdx");
            SendBoxs boxs = new SendBoxs();
            QueryObject qo = new QueryObject(boxs);
            qo.AddWhere(SendBoxAttr.Sender, WebUser.No);
            qo.addAnd();
            qo.AddWhere(SendBoxAttr.IsSend, "=", 0);
            qo.addAnd();
            if (SystemConfig.AppCenterDBVarStr == "@" || SystemConfig.AppCenterDBVarStr == "?")
                qo.AddWhere(SendBoxAttr.Title, " LIKE ", SystemConfig.AppCenterDBType == DBType.MySQL ? (" CONCAT('%'," + SystemConfig.AppCenterDBVarStr + "SearchKey,'%')") : (" '%'+" + SystemConfig.AppCenterDBVarStr + "SearchKey+'%'"));
            else
                qo.AddWhere(SendBoxAttr.Title, " LIKE ", " '%'||" + SystemConfig.AppCenterDBVarStr + "SearchKey||'%'");
            qo.addOrderByDesc(SendBoxAttr.SendTime);
            qo.MyParas.Add("SearchKey", searchKey);

            qo.DoQuery(SendBoxAttr.MyPK, 15, pageIdx);
            return boxs.ToJson();
        }

        /// <summary>
        /// 垃圾箱
        /// </summary>
        /// <returns></returns>
        public string InitRecyleBox()
        {
            //关键字（标题）查询
            string searchKey = this.GetRequestVal("SearchKey");
            int pageIdx = this.GetRequestValInt("PageIdx");
            InBoxs boxs = new InBoxs();
            QueryObject qo = new QueryObject(boxs);
            qo.AddWhere(InBoxAttr.UserNo, WebUser.No);
            qo.addAnd();
            qo.AddWhere(InBoxAttr.InBoxState, "=", 2);
            qo.addAnd();
            if (SystemConfig.AppCenterDBVarStr == "@" || SystemConfig.AppCenterDBVarStr == "?")
                qo.AddWhere(InBoxAttr.Title, " LIKE ", SystemConfig.AppCenterDBType == DBType.MySQL ? (" CONCAT('%'," + SystemConfig.AppCenterDBVarStr + "SearchKey,'%')") : (" '%'+" + SystemConfig.AppCenterDBVarStr + "SearchKey+'%'"));
            else
                qo.AddWhere(InBoxAttr.Title, " LIKE ", " '%'||" + SystemConfig.AppCenterDBVarStr + "SearchKey||'%'");
            qo.addOrderByDesc(InBoxAttr.SendTime);
            qo.MyParas.Add("SearchKey", searchKey);

            qo.DoQuery(InBoxAttr.MyPK, 15, pageIdx);
            return boxs.ToJson();
        }

        /// <summary>
        /// 收件人
        /// </summary>
        /// <returns></returns>
        public string SelectEmps()
        {
            //收件人
            string emps = this.GetRequestVal("Emps");
            Emps portEmps = new Emps();
            emps = "'" + emps+"'";
            int i = emps.IndexOf(",");
            emps= emps.Replace(",", "','");
            portEmps.RetrieveIn("No", emps);
            return portEmps.ToJson();
        }
       

    }
}
