﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Text;
using System.Web;
using BP.DA;
using BP.Sys;
using BP.Web;
using BP.Port;
using BP.En;
using BP.WF;
using BP.WF.Template;
using BP.WF.HttpHandler;

namespace BP.OA.HttpHandler
{
    public class App_Notice : DirectoryPageBase
    {
        /// <summary>
        /// 页面功能实体
        /// </summary>
        /// <param name="mycontext"></param>
        public App_Notice(HttpContext mycontext)
        {
            this.context = mycontext;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        public App_Notice()
        {
        }

        /// <summary>
        /// 添加公告
        /// </summary>
        /// <returns></returns>
        public string Notice_Add()
        {
            //判断是否有同名+同类型 公告
            string title = this.GetRequestVal("Title");
            string fk_NoticeCategory = this.GetRequestVal("NoticeCategory");
            string attachFile = this.GetRequestVal("AttachFile");

            Notice notice = new Notice();
            notice.RetrieveByAttrAnd(NoticeAttr.Title, title, NoticeAttr.FK_NoticeCategory, fk_NoticeCategory);
            if (notice.OID.ToString() != "0")
                return "false";

            #region 为Notice属性赋值

            notice.Title = title;
            notice.FK_NoticeCategory = fk_NoticeCategory;
            notice.Doc = this.GetRequestVal("Doc");
            notice.RDT = DateTime.Parse(DateTime.Now.ToString());
            notice.FK_Dept = BP.Web.WebUser.FK_Dept;
            notice.Rec = BP.Web.WebUser.No;
            notice.Importance = int.Parse(this.GetRequestVal("Importance"));
            //接收对象
            bool toAll = int.Parse(this.GetRequestVal("Users")) == 0 ? true : false;
            DataTable dtCollecion = new DataTable();
            string strusers = "", strdepts = "", strstations = "";
            if (toAll)
            {
                dtCollecion = Get_AllGPM();
                strusers = dtCollecion.Rows[0][1].ToString();
                strdepts = dtCollecion.Rows[1][1].ToString();
                strstations = dtCollecion.Rows[2][1].ToString();
            }
            else
            {
                strusers = this.GetRequestVal("TB_Emps");
                strdepts = this.GetRequestVal("TB_Depts");
                strstations = this.GetRequestVal("TB_Stations");
            }

            notice.SendToPart = toAll;
            notice.SendToDepts = strdepts;
            notice.SendToStations = strstations;
            notice.SendToUsers = strusers;
            notice.NoticeSta = int.Parse(this.GetRequestVal("NoticeSta"));
            //notice.Doc = Doc;
            //保存附件
            if (context.Request.Files.Count > 0)
            {
                HttpPostedFile file = context.Request.Files[0];
                //设置路径
                string dir = String.Format("\\DataUser\\UploadFile\\Notice\\{0:yyyyMMdd}", DateTime.Now);
                if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(dir)))
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(dir));

                //获取名称
                string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.'));
                //获取后缀
                string ext = System.IO.Path.GetExtension(file.FileName);
                dir = context.Server.MapPath(dir);
                string pathTo = dir + "\\" + fileName + ext; ;
                file.SaveAs(pathTo);
                notice.AttachFile = pathTo;
            }
            else
            {
                notice.AttachFile = "";
            }
            notice.Insert();
            #endregion 为Notice属性赋值



            return "true";
        }
        public string DownFile()
        {
            Notice ne = new Notice(int.Parse(this.GetRequestVal("OID")));
            return "url@" + ne.AttachFile;
        }
        /// <summary>
        /// 获取新文件名函数
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetAttachFileName(string fileName)
        {
            string f = System.IO.Path.GetFileNameWithoutExtension(fileName);
            string ext = System.IO.Path.GetExtension(fileName);
            string newFileName = String.Format("{0}_{1:yyyyMMddHHmmss}{2}", f, DateTime.Now, ext);
            return newFileName;
        }

        #region 获取所有的组织结构数据
        /// <summary>
        /// 获取所有的组织结构数据
        /// </summary>
        /// <returns></returns>
        public DataTable Get_AllGPM()
        {
            string strusers = "", strdepts = "", strstations = "";
            DataTable dtCollection = new DataTable();
            dtCollection.Columns.AddRange(new DataColumn[] { new DataColumn("Type"), new DataColumn("Doc") });

            //人员
            Emps emps = new Emps();
            emps.RetrieveAll();
            foreach (Emp emp in emps)
                strusers += "'" + emp.No + "',";

            DataRow dr = dtCollection.Rows.Add();
            dr["Type"] = "Emps";
            dr["Doc"] = SubString(strusers);

            //部门
            Depts depts = new Depts();
            depts.RetrieveAll();
            foreach (Dept dept in depts)
                strdepts += "'" + dept.No + "',";

            dr = dtCollection.Rows.Add();
            dr["Type"] = "Depts";
            dr["Doc"] = SubString(strdepts);

            //岗位
            Stations stations = new Stations();
            stations.RetrieveAll();
            foreach (Station station in stations)
                strstations += "'" + station.No + "',";

            dr = dtCollection.Rows.Add();
            dr["Type"] = "Stations";
            dr["Doc"] = SubString(strstations);

            return dtCollection;
        }
        #endregion 获取所有的组织结构数据

        /// <summary>
        /// 截取字符串
        /// </summary>
        /// <returns></returns>
        public string SubString(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return "";
            return str.Substring(0, str.Length - 1);
        }
    }
}
