﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Text;
using System.Web;
using BP.DA;
using BP.Sys;
using BP.Web;
using BP.Port;
using BP.En;
using BP.WF;
using BP.WF.Template;
using BP.WF.HttpHandler;

namespace BP.OA.HttpHandler
{
    /// <summary>
    /// 页面功能实体
    /// </summary>
    public class Main : DirectoryPageBase
    {
        /// <summary>
        /// 页面功能实体
        /// </summary>
        /// <param name="mycontext"></param>
        public Main(HttpContext mycontext)
        {
            this.context = mycontext;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        public Main()
        {

        }
        public string Default_Init()
        {
            //使用GPM后删除上面一句.
            string userName = BP.Web.WebUser.No;
            DataTable dtMenu = null; // BP.OA.GPM.GetUserMenuOfDatatable(userName);

            //转化成可用的DataTable，此数据从GPM来.
            if (dtMenu != null)
            {
                dtMenu.Columns["FK_Menu"].ColumnName = "menuid";
                dtMenu.Columns["Name"].ColumnName = "menuname";
                dtMenu.Columns["ParentNo"].ColumnName = "parentid";
                dtMenu.Columns["Url"].ColumnName = "url";
                dtMenu.Columns["WebPath"].ColumnName = "icon";
                dtMenu.Columns["MenuType"].ColumnName = "degree";
            }

            foreach (DataRow dr in dtMenu.Rows)
            {
                string icon = String.Format("{0}", dr["icon"]);
                //修改菜单左侧显示路径 hzm
                if (!String.IsNullOrEmpty(icon)
                    && icon.StartsWith("//"))
                    icon = icon.Substring(1);

                icon = String.Format("{0}{1}", "", icon);
                dr["icon"] = string.IsNullOrEmpty(icon) ? BP.WF.Glo.CCFlowAppPath + "WF/Img/FileType/ie.gif" : icon;
            }

            DataSet ds = GetUserMenu(dtMenu, AppMenuNo);
            return BP.Tools.Json.ToJson(ds);
        }

        /// <summary>
        /// 系统在菜单中的编号
        /// </summary>
        public string AppMenuNo
        {
            get
            {
                string appMenuNo = BP.Sys.SystemConfig.AppSettings["AppMenuNo"];
                if (string.IsNullOrEmpty(appMenuNo))
                    return "2002";
                return appMenuNo;
            }
        }

        private DataSet GetUserMenu(DataTable dtMenu, string AppMenuNo)
        {
            DataSet ds = new DataSet();
            DataTable dtMenus = new DataTable("menus");
            DataTable dtChild = new DataTable("child");

            DataRow[] drs1 = dtMenu.Select("parentid='" + AppMenuNo + "'");

            foreach (DataRow dr1 in drs1)
            {
                dtMenus.ImportRow(dr1);

                DataRow[] drs2 = dtMenu.Select("parentid='" + Convert.ToString(dr1["menuid"]) + "'");
                if (drs2.Length > 0 && drs2 != null)
                {
                    foreach (DataRow dr2 in drs2)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(dr2["icon"])) || Convert.ToString(dr2["icon"]).Contains("WF/Img/FileType/ie.gif"))
                            dr2["icon"] = "/WF/Img/Tree/Dir.gif";

                        dtChild.ImportRow(dr2);
                    }
                }
            }

            ds.Tables.Add(dtChild);
            ds.Tables.Add(dtMenus);
            return ds;
        }


    }


}
