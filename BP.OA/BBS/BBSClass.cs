﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;
using System.Data;
using BP.Port;

namespace BP.OA.BBS
{
    //版块类别
    public class BBSClassAttr : EntityTreeAttr
    {
        /// <summary>
        /// 图片路径
        /// </summary>
        public const String ImgUrl = "ImgUrl";
        /// <summary>
        /// 主题数量
        /// </summary>
        public const String ThemeCount = "ThemeCount";
        /// <summary>
        /// 回复次数
        /// </summary>
        public const String ReplyCount = "ReplyCount";
        /// <summary>
        /// 新增人员
        /// </summary>
        public const String FK_Emp = "FK_Emp";
        /// <summary>
        /// 记录日期
        /// </summary>
        public const String RDT = "RDT";
        /// <summary>
        /// 备注
        /// </summary>
        public const String ReMark = "ReMark";
    }
    /// <summary>
    /// 版块类别
    /// </summary>
    public partial class BBSClass : EntityTree
    {
        #region 属性
        /// <summary>
        /// 图片路径
        /// </summary>
        public string ImgUrl
        {
            get
            {
                return this.GetValStrByKey(BBSClassAttr.ImgUrl);
            }
            set
            {
                this.SetValByKey(BBSClassAttr.ImgUrl, value);
            }
        }
        /// <summary>
        /// 主题数量
        /// </summary>
        public decimal ThemeCount
        {
            get
            {
                return this.GetValDecimalByKey(BBSClassAttr.ThemeCount);
            }
            set
            {
                this.SetValByKey(BBSClassAttr.ThemeCount, value);
            }
        }
        /// <summary>
        /// 回复次数
        /// </summary>
        public decimal ReplyCount
        {
            get
            {
                return this.GetValDecimalByKey(BBSClassAttr.ReplyCount);
            }
            set
            {
                this.SetValByKey(BBSClassAttr.ReplyCount, value);
            }
        }
        /// <summary>
        /// 新增人员
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(BBSClassAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(BBSClassAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 记录日期
        /// </summary>
        public DateTime RDT
        {
            get
            {
                return this.GetValDateTime(BBSClassAttr.RDT);
            }
            set
            {
                this.SetValByKey(BBSClassAttr.RDT, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string ReMark
        {
            get
            {
                return this.GetValStrByKey(BBSClassAttr.ReMark);
            }
            set
            {
                this.SetValByKey(BBSClassAttr.ReMark, value);
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 版块类别
        /// </summary>
        public BBSClass() { }

        /// <summary>
        /// 版块类别
        /// </summary>
        public BBSClass(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_BBSClass";
                map.EnDesc = "论坛版块";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.IsAllowRepeatName = false;
                map.CodeStruct = "4";
                map.AddTBStringPK(BBSClassAttr.No, null, "编号", true, true, 4, 4, 80);
                map.AddTBString(BBSClassAttr.Name, null, "名称", true, false, 1, 100, 100);
                map.AddTBString(BBSClassAttr.ParentNo, null, "父节点编号", false, false, 0, 100, 30);
               // map.AddTBString(BBSClassAttr.TreeNo, null, "树编号", false, false, 0, 100, 30);
                map.AddTBString(BBSClassAttr.FK_Emp, null, "发布人", true, false, 0, 100, 100);
                map.AddTBDateTime(BBSClassAttr.RDT, "创建日期", false, false);
                map.AddTBString(BBSClassAttr.ImgUrl, null, "图片路径", true, false, 0, 1000, 100);
                map.AddTBInt(BBSClassAttr.ThemeCount, 0, "主题数量", true, true);
                map.AddTBInt(BBSClassAttr.ReplyCount, 0, "回复次数", true, true);
                map.AddTBStringDoc(BBSClassAttr.ReMark, null, "备注", true, false,true);
                map.AddTBInt(BBSClassAttr.Idx, 0, "Idx", false, false);
               // map.AddTBInt(BBSClassAttr.IsDir, 0, "IsDir", false, false);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///版块类别集合
    /// </summary>
    public class BBSClasss : EntitiesTree
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new BBSClass();
            }
        }
        /// <summary>
        /// 版块类别集合
        /// </summary>
        public BBSClasss()
        {
        }
    }
}
