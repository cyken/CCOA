﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.HuiYi
{
    public class HuiYiAPI
    {
        #region 年检 api.
        /// <summary>
        /// 会议室审批流程是否启用?
        /// </summary>
        public static bool HuiYi_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("HuiYi_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 会议室审批流程标记
        /// </summary>
        public static string HuiYi_FlowMark
        {
            get
            {
                return "HuiYiShiShenPi";
            }
        }

       
        public static string HuiYiShiShenPi(string hyzt, string hylx, string
            sqbm, string fqr, string jlr, string djrq, decimal chrs,
            string qssj, string zzsj,
            string FK_HuiYiShi, string bzyq, string bzzt, string hyjl,
            Int64 workid)
        {
            HuiYiShi hys = new HuiYiShi();
            hys.RetrieveByAttr("Name",FK_HuiYiShi);
            
            HuiYiShenQing en = new HuiYiShenQing();
          
            try
            {
                en.HYZT = hyzt;
                en.HYLX = hylx;
                en.SQBM = sqbm;
                en.FQR = fqr;
                en.JLR = jlr;
                en.DJRQ = djrq;
                en.CHRS = chrs;
                en.QSSJ = qssj;
                en.ZZSJ = zzsj;
                //nn.Name = FK_HuiYiShi;
                en.FK_HuiYiShi = hys.No;
                en.BZYQ = bzyq;
                en.BZZT = bzzt;
                en.HYJL = hyjl;
                en.WorkID = workid;
                en.Insert();
                //nn.Insert();
                return "会议室审批成功...";
            }
            catch (Exception ex)
            {
                en.Delete();
                return "@会议室审批失败:" + ex.Message;
            }
        }
        #endregion 采购api.

       



    }
}
