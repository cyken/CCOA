﻿## 特别说明
1. 为了支持java版本，目前ccoa正在升级。
2. 旧版本的程序在\CCOA\CCApp\*.* 解决方案文件是     CCOA\CCOA.sln
3. 新版本的文件在\CCOA\CCApp2018\*.* 解决方案文件是  CCOA\CCOA2018.sln
4. 旧版本演示环境: http://ccoa.ccflow.org/Plant/Login.htm   用户名:admin 密码:123 
5. 数据库脚本下载地址http://140.143.236.168:7080/svn/CCBPMDOCS/InstallDataBaseScript 用户名/密码:ccbpm 

### CCOA 基本信息. ###
01. 驰骋OA简称CCOA, 是济南驰骋公司提供一款100%开源的自动化办公系统,无任何限制.
02. 采用GPL开源协议,如果您要友好的使用CCOA请遵守GPL开源协议，如果您不能遵守该协议并且想使用CCOA需要获得驰骋公司的授权,咨询电话:13864102942, QQ:1146275232
05. 关于GPL开源协议   URL: http://www.gnu.org/licenses/gpl.html 
06. CCOA目前有支持.net版本 . 我们计划开发java版本的CCOA，敬请期待.
07. 开发运行环境:  VS2010, .net4.0 c#.net. 客户端: FireFox 3.0以上. IE6+,或者使用IE内核的浏览器.
08. 支持sqlserver系列版本， oracle 系列版本  未来支持MySQL数据库.
08. 组成部分: 流程设计器、表单设计器、流程解析执行器、表单解析执行器、流程服务。
09. 基本功能: 图形化流程设计/智能表单web定义免程序开发/级联下拉框/流程轨迹/单据自定义打印/邮件短信工作到达通知/自动任务分配/支持sdk模式开发/简洁集成/消息侦听/丰富事件接口/报表定义/工作量分析/绩效考核/手机访问/支持sqlserve,oracle,mysql,informix数据库.
10. **Git 地址:** svn://git.oschina.net/opencc/CCOA
11. **下载地址** 请参考: http://git.oschina.net/opencc/CCOA/wikis/home
12. 演示环境: http://ccoa.ccflow.org/Plant/Login.htm   用户名:admin 密码:123




### 为什么选择CCOA? ###
01. CCOA集成了强大的开源工作流程引擎 ccflow.
02. CCOA集成了CCGPM的权限管理系统，可以方便定义权限与组织结构.
03. CCFlow集成方便, 概念、名词通俗易懂.


###  如何学习好CCOA? ###
01.  多看视频与文档.
   1. ccflow提供的视频是4.0的视频，有一些功能对应不上您可以看文档，文档是最新的。
   2. ccflow提供了两个重要的文档<<驰骋工作流引擎-流程设计器操作手册-CCFlow6.doc>> <<驰骋工作流引擎-表单设计器操作手册-CCFlow6.doc>> 这是您掌握ccflow的基础.   
02. 加入群里与ccflow爱好者交流.请打开http://ccflow.org 网站,找到qq群加入里面, 把不明白的问题，提交到群论坛里，会有人回答你的问题.
03. 有问题反馈到bbs,  http://bbs.ccflow.org/  注意不要省掉注册邀请码.    
04. 通过OSC发布bug，反馈问题，协助我们开发。
05. 如果您们项目工期紧张并且有充足的费用, 请联系我们, 您会得到现场的技术支持与系统培训。

### CCOA 基础功能 ###

1011	我的计划	252	010101
1012	公告通知	2002	0102
1013	公告管理	1012	010201
1014	添加公告	1012	10202
1015	我的公告	1012	10203
1016	邮件管理	2002	0103
1017	写邮件	1016	010301
1018	收件箱	1016	10302
1019	发件箱	1016	10303
1020	新闻管理	2002	0104
1021	添加新闻	1020	010401
1022	新闻管理	1020	10402
1024	新闻栏目	1020	10404
1025	工作流程	2002	0105
1026	发起	1025	010501
1027	待办	1025	10502
1028	抄送	1025	10503
1029	挂起	1025	10504
1030	在途	1025	10505
1031	查询	1025	10506
1032	关键字查询	1025	10507
1033	取消审批	1025	10508
1036	设置	1025	10511
1037	我的日程	252	010102
1038	创建流程	1025	10512
1039	草稿箱	1016	10304
1040	垃圾箱	1016	10305
1041	我的新闻	1020	11916
106	文档中心	2002	0106
107	知识树	106	010601
145	车辆管理	2002	0107
146	驾驶员信息	145	010701
147	我的文档	106	10602
160	知识管理	106	10603
185	指标管理	145	01070101
186	最新文档	106	10604
188	我的工作台	187	010801
189	所有资讯	187	10802
190	收件箱(0/1)	187	10803
191	我的日程	187	10804
192	我的文档	187	10805
193	信息块维护	187	10809
194	安全退出	187	10807
195	参数设置	272	11203
196	固定资产	2002	0113
197	资产登记	196	011301
198	办公用品	2002	0114
199	库存台账	198	011401
200	购买台账	198	11402
2000	业务系统	1000	0101
2001	办公系统	1000	0102
2002	驰骋OA	2000	0103
2003	Tester	2001	0104
2004	工作流程	2000	0105
2005	即时通讯	2000	0106
2006	公共信息网	2000	0107
2007	权限管理	2000	0108
2008	单点登陆	2000	0109
201	人事档案	2002	0115
2010	发送短消息	252	10103
2011	公告类型	1012	10204
202	员工档案	201	011501
203	合同签订信息	201	11502
204	领取台账	198	1140201
214	合同解除信息	201	11504
217	公积金台账	201	11507
219	社保与福利台账	201	11509
224	调动记录	223	011601
228	调动记录	227	01170101
230	发起人员调动流程	227	1170102
242	新建节点242	213	1150301
243	所属大类	196	11302
244	所属小类	196	11303
246	资产领用	196	11304
248	资产报修	196	11305
249	资产报废	196	11306
250	发起投票	252	010901
251	我的投票	252	10902
252	我的工具	2002	0110
253	科学计算器	252	011001
254	万年历	252	11002
255	记事便签	252	11003
256	同事共享	106	10605
257	资产归还	196	11307
258	我的通讯录	252	11004
259	合同变更台帐	201	11510
261	车辆信息	145	10702
262	知识共享	106	10606
263	用品类别	198	1140202
264	知识库	187	10808
265	会议管理	2002	0111
267	会议类型	265	11102
268	会议室资源维护	265	11103
269	会议室列表	265	11104
270	会议室预订	265	11105
271	我的会议	265	11106
272	我的论坛	2002	0112
273	论坛板块	272	011201
274	论坛首页	272	11202
297	快捷入口	2002	0119
299	待办	297	11911
300	在途	297	11912
301	发起	297	11913
302	我的邮件	297	11914
303	我的公告	297	11915
304	我的新闻	297	11916
305	我的会议	297	11917
 

### CCOA 程序文件清单:  ###
1. D:\CCOA\Components   -- 组件目录.
2. D:\CCOA\Components\BP.En30   -- 底层基类.
3. D:\CCOA\Components\BP.Web.Controls30   --BS 控件层.
4. D:\CCOA\Components\BP.WF  --工作流程引擎层
5. D:\CCOA\RefDLL  -- 第三方组件中需要调用dll.
6. D:\CCOA\Documents -  文档
7. D:\CCOA\CCApp  --OA系统前台主目录.  
8. D:\CCOA\DemoAndTesting  - 单元测试&Demo


### CCOA 前台目录结构.前台程序.(不建议用户修改，如果修改请提交给我们，否则您就没有办法升级.) ###
1. D:\CCOA\CCApp\WF\ --前台程序.
2. D:\CCOA\CCApp\WF\Comm  --通用功能层.
3. D:\CCOA\CCApp\WF\Data  -- 应用程序数据目录. 包含一些xml,等等。
4. D:\CCOA\CCApp\WF\Data\Install 与安装有关系的文件
5. D:\CCOA\CCApp\WF\Data\JSLib  系统javascript 函数库。
6. D:\CCOA\CCApp\WF\Data\Language 语言包(完善中)
7. D:\CCOA\CCApp\WF\Data\Node  cs流程设计器节点类型（cs流程设计器不在发展）
8. D:\CCOA\CCApp\WF\Data\XML  xml配置文件不仅仅ccflow使用,bp框架也使用它。
9. D:\CCOA\CCApp\WF\UC  --用户控件.
10. D:\CCOA\CCApp\WF\DocFlow -- 公文流程(目前还不是很完善)
11. D:\CCOA\CCApp\WF\Admin - 对ccflow的管理比如设计方向条件.报表定义...
12. D:\CCOA\CCApp\WF\Admin\XAP CCFlowDesigner.xap流程设计器，CCForm.xap表单设计器。   
13. D:\CCOA\CCApp\WF\MapDef - 表单定义.
14. D:\CCOA\CCApp\WF\SDKComponents  --流程组件目录.
15. D:\CCOA\CCApp\WF\WorkOpt -- 工作处理器的附件功能.
16. D:\CCOA\CCApp\WF\Admin\CCBPMDesigner -- H5的流程设计器.
17. D:\CCOA\CCApp\WF\Admin\CCFormDesigner -- H5的表单设计器.
18. D:\CCOA\CCApp\WF\WebOffice -- 公文流程处理程序.

19. D:\CCOA\CCApp\GPM -- 权限管理系统.
20. D:\CCOA\CCApp\App -- 模块程序. 

### 1.2 前台的用户数据文件，用户可以更改. ###
1. D:\CCOA\CCApp\DataUser --用户文件.
2. D:\CCOA\CCApp\DataUser\Seal -- 电子盖章图片.
3. D:\CCOA\CCApp\DataUser\UploadFile - 上传附件
4. D:\CCOA\CCApp\DataUser\Style -- 个性化风格文件.
5. D:\CCOA\CCApp\DataUser\CyclostyleFile -- 单据模版文件.
6. D:\CCOA\CCApp\DataUser\EmailTemplete -邮件模版文件.
7. D:\CCOA\CCApp\DataUser\ICON --ICON
8. D:\CCOA\CCApp\DataUser\TaoHong --公文套红.
9. D:\CCOA\CCApp\DataUser\Bill  单据打印生成数据.
10. D:\CCOA\CCApp\DataUser\CyclostyleFile 单据模板数据
11. D:\CCOA\CCApp\DataUser\DtlTemplete  导入明细表模板文件.
12. D:\CCOA\CCApp\DataUser\EmailTemplete  自定义邮件发送格式文件.
13. D:\CCOA\CCApp\DataUser\JSLib 用户自定义函数库
14. D:\CCOA\CCApp\DataUser\JSLibData 用户自定义函数生成文件。
15. D:\CCOA\CCApp\DataUser\Log 系统日志文件
16. D:\CCOA\CCApp\DataUser\ReturnLog 退回日志文件.
17. D:\CCOA\CCApp\DataUser\Siganture 签名文件.
18. D:\CCOA\CCApp\DataUser\Style 用户自定义风格文件。
19. D:\CCOA\CCApp\DataUser\UploadFile 表单附件上传文件，单附件，与多附件。
20. D:\CCOA\CCApp\DataUser\XML 用户系统配置文件。
21.  D:\CCOA\CCApp\GPM\**  -- 权限控制系统，如果不启用权限系统就可以省略。
 
 
**更多商业问题请参考:**   [http://ccflow.org/About.aspx](http://ccflow.org/About.aspx "商业问题") 




