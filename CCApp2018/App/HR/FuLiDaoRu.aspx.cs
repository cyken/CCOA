﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.OA.HR;
using System.IO;
using BP.Port;
using BP.Web.Controls;
using BP.OA;
using BP.DA;
namespace CCOA.App.HR
{
    public partial class FuLiDaoRu : System.Web.UI.Page
    {
        TB tb = new TB();
        protected void Page_Load(object sender, EventArgs e)
        {
            Lab la = new Lab();
            la.Text = "月份：";
            this.Pub1.Add(la);
            tb.ID = "TB_DaoRu";
            tb.ShowType = TBType.Date;
            tb.Attributes["onfocus"] = "WdatePicker();";
            this.Pub1.Add(tb);
        }
        protected void BT_DaoRu_Click(object sender, EventArgs e)
        {
            if (this.fu.HasFile)
            {
                //保存上传文件
                string filename = this.fu.PostedFile.FileName;
                string extension = (new FileInfo(filename)).Extension;
                string newfilename = System.DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                string path = Server.MapPath("/DataUser/userloadfile/");
                this.fu.PostedFile.SaveAs(path + newfilename);
                //导入execel
                try
                {
                     DataTable dt = DBLoad.ReadExcelFileToDataTable(path + newfilename);
                string s = dt.Columns[4].ColumnName.ToString() + " like '" + "%" + DateTime.Parse(tb.Text.ToString()).Month + "%" + "'";
                dt.Select(s);
               
                FuLi gj = new FuLi();
                foreach (DataRow row in dt.Rows)
                {
                    BP.Port.Depts des = new Depts();
                    des.RetrieveAll();
                    gj.FK_Dept = ((Dept)des.Filter("Name", row[1].ToString())).No.ToString();
                    gj.FK_DanWei = row[2].ToString();
                    Emps hrrs = new Emps();
                    hrrs.RetrieveAll();
                    gj.FK_Name = ((Emp)hrrs.Filter("Name", row[3].ToString())).No.ToString();
                    gj.Mouth = row[4].ToString();
                    gj.XinSheBaoJiShu = float.Parse(row[5].ToString());
                    gj.XinYiBaoJiShu = float.Parse(row[6].ToString());
                    gj.YanLaoGeRenJiaoFei = float.Parse(row[7].ToString());
                    gj.YanLaoDanWeiJiaoFei = float.Parse(row[8].ToString());
                    gj.ShiYeGeRenJiaoFei = float.Parse(row[9].ToString());
                    gj.ShiYeDanWeiJiaoFei = float.Parse(row[10].ToString());
                    gj.YiLiaoGeRenJiaoFei = float.Parse(row[11].ToString());
                    gj.YiLiaoDanWeiJiaoFei = float.Parse(row[12].ToString());
                    gj.GongShangDanWeiJiaoFei = float.Parse(row[13].ToString());
                    gj.ShengYuDanWeiJiaoFei = float.Parse(row[14].ToString());
                    //gj.YanLaoHeJi = float.Parse(row[15].ToString());
                    //gj.ShiYeHeJi = float.Parse(row[16].ToString());
                    //gj.YiLiaoHeJi = float.Parse(row[17].ToString());
                    //gj.DanWeiJiaoFeiHeJi = float.Parse(row[18].ToString());
                    //gj.GeRenJiaoFeiHeJi = float.Parse(row[19].ToString());
                    //gj.HeJi = float.Parse(row[20].ToString());
                    gj.BeiZhu = row[15].ToString();
                    gj.Save();
                }
                Response.Redirect("/WF/Comm/Search.aspx?EnsName=BP.OA.HR.FuLis");
                }
                catch (Exception)
                {
                    BP.Sys.PubClass.Alert("文件的格式必须是xls,并且符合模板要求！");
                    return;
                }
            }
        }
    }
}