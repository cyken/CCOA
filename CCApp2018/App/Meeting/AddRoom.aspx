﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddRoom.aspx.cs" Inherits="CCOA.App.Meeting.AddRoom" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <style type="text/css">
        .table th
        {
            text-align: right;
            width: 150px;
            margin-right: 10px;
        }
        .table div
        {
            float: left;
        }
        input[type=text], select
        {
            width: 250px;
            border-style: solid;
            border-width: thin;
            border-color: Silver;
            height: 20px;
        }
        .Validform_checktip
        {
            color: Red;
            margin-left: 5px;
        }
        .txtAdd
        {
            background-image: url("../../Images/Add01.png");
            background-position: right center;
            background-repeat: no-repeat;
        }
    </style>
    <script type="text/javascript">
        function openDialog(name, id, e) {
            var diag = new Dialog();
            diag.Width = 620;
            diag.Height = 450;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = e;
            diag.OKEvent = function () {
                var ddl = diag.innerFrame.contentWindow.document.getElementById('lbRight');
                var resultName = "";
                var resultNo = "";
                for (i = 0; i < ddl.options.length; i++) {
                    resultName += ddl.options[i].text + ",";
                    resultNo += ddl.options[i].value + ",";
                }
                $("input[id='" + name + "']").val(resultName);
                $("input[id='" + id + "']").val(resultNo);
                diag.close();
            };
            diag.show();
        }
    </script>
    <script type="text/javascript">
        function openDialog2(sender, e) {
            var diag = new Dialog();
            diag.Width = 620;
            diag.Height = 450;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = e + "?no=" + $("#RoomResource").val();
            diag.OKEvent = function () {
                var result1 = "", result2 = "";
                var ddl = diag.innerFrame.contentWindow.document.getElementsByName('cbNo');
                $(ddl).each(function () {
                    if (this.checked == true) {
                        result1 += this.value + ",";
                        result2 += this.title + ",";
                    }
                    $("#RoomResource").val(result1);
                    $("#ResourceName").val(result2);
                });
                diag.close();
            };
            diag.show();
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#form1").Validform({
                tiptype: 2,
                showAllError: true
            });
        });
        function DoRefresh() {

//            alert("sgbk");
//            //            window.parent.1.DoRefresh();
//            window.location.DoRefresh();
            window.parent.DoRefresh();
//            window.top.DoRefresh();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table class="table">
        <tr>
            <th class="td">
                会议室名称：
            </th>
            <td>
                <div>
                    <input type="text" name="name" maxlength="50" datatype="s4-50" errormsg="会议名称至少4个字符！"
                        nullmsg="会议名称不能为空！" value="<%=room==null?string.Empty:room.Name%>" />
                </div>
                <div>
                    <span class="Validform_checktip"><span style="color: Red">*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                容纳人数：
            </th>
            <td>
                <div>
                    <input type="text" name="capacity" maxlength="4" datatype="n2-4" errormsg="只能输入2-4位数字！"
                        value="<%=room==null?string.Empty:room.Capacity.ToString()%>" />
                </div>
                <div>
                    <span class="Validform_checktip"><span style="color: Red">*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                会议室管理员：
            </th>
            <td>
                <div>
                    <input type="text" id="EmpName" onclick="openDialog('EmpName','EmpNo','../../Ctrl/SelectUsers/SelectUser_Jq.aspx')"
                        value="<%=room==null?string.Empty:GetEmpName(room.FK_Emp)%>" class="txtAdd" readonly="readonly" />
                    <input type="hidden" id="EmpNo" value="<%=room==null?string.Empty:room.FK_Emp%>"
                        name="FK_Emp" />
                </div>
            </td>
        </tr>
        <tr>
            <th>
                会议室资源描述：
            </th>
            <td>
                <div>
                    <input type="text" onclick="openDialog2(this,'ResSelect.aspx')" id="ResourceName" value="<%=room==null?string.Empty:GetResName(room.RoomResource)%>" />
                    <input type="hidden" id="RoomResource" name="RoomResource" value="<%=room==null?string.Empty:room.RoomResource%>" />
                </div>
            </td>
        </tr>
        <tr>
            <th>
                会议室状态
            </th>
            <td>
                <div>
                    <select name="RoomSta">
                        <option value="0" <%=room==null?string.Empty:GetSelectedState(room.RoomSta,"正常")%>>正常</option>
                        <option value="1" <%=room==null?string.Empty:GetSelectedState(room.RoomSta,"维修中")%>>
                            维修中</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                会议室说明：
            </th>
            <td>
                <div>
                    <input type="text" name="note" value="<%=room==null?string.Empty:room.Note%>" />
                </div>
            </td>
        </tr>
        <tr>
            <th>
                允许使用人：
            </th>
            <td>
                <div>
                    <input type="text" onclick="zDialog_open2('../../Ctrl/SelectUsers/SelectUser_Jq.aspx','选择人员',650,430,'#AuthorityNo','AuthorityNo','AuthorityName')"
                        id="AuthorityName" value="<%=room==null?string.Empty:GetEmpName(room.Authority)%>"
                        class="txtAdd" readonly="readonly" />
                    <input type="hidden" value="<%=room==null?string.Empty:room.Authority%>" id="AuthorityNo"
                        name="Authority" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="Button1" runat="server" Text="添加" OnClick="Button1_Click" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
