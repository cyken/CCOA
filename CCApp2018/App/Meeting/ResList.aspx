﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResList.aspx.cs" Inherits="CCOA.App.Meeting.ResList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function openWin(e) {
            var diag = new Dialog();
            diag.Width = 400;
            diag.Height = 100;
            diag.Title = "添加会议室资源";
            diag.URL = e;
            diag.show();
        }
        function open(e1, e2) {
            alert("dd");
            var str = "AddRes.aspx?no=" + e1 + "&name=" + e2;
            openwin(str);
        }
        function doDelete(no) {
            if (window.confirm("你确定要删除吗？")) {
                $("#del").val(no);
                $("#form1").submit();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <label>
            会议室资源列表：
        </label>
        <a href="#" onclick="openWin('AddRes.aspx')">添加会议室资源</a> <a href="#" onclick="javascript:location.reload()">
            刷新</a>
        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table class="easyui-datagrid">
                    <thead>
                        <tr>
                            <th data-options="field:'no'">
                                资源编号
                            </th>
                            <th data-options="field:'name'">
                                资源名称
                            </th>
                            <th data-options="field:'operate'">
                                操作
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem ,"No")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem ,"Name")%>
                    </td>
                    <td>
                        <a href="javascript:void(0)" onclick='<%#Check(Eval("No"),Eval("Name"))%>'>编辑</a>
                        <a href="javascript:void(0)" onclick='<%#Delete(Eval("No"))%>'>删除</a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody></table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <input name="del" id="del" value="" type="hidden" />
    </form>
</body>
</html>
