﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Meeting
{
    public partial class ResList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                string no = Request.Form["del"];
                if (!string.IsNullOrEmpty(no))
                {
                    BP.OA.Meeting.RoomResource res = new BP.OA.Meeting.RoomResource();
                    res.No = no;
                    res.Delete();
                }
            }
            BP.OA.Meeting.RoomResources reses = new BP.OA.Meeting.RoomResources();
            reses.RetrieveAll();
            Repeater1.DataSource = reses;
            Repeater1.DataBind();
        }
        public string Check(object obj1, object obj2)
        {
            string str = "openWin(\"AddRes.aspx?no={0}&name={1}\")";
            return string.Format(str, Convert.ToString(obj1), Convert.ToString(obj2));
        }
        public string Delete(object obj1)
        {
            string str = "doDelete(\"{0}\")";
            return string.Format(str, Convert.ToString(obj1));
        }
    }
}