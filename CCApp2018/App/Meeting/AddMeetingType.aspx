﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddMeetingType.aspx.cs"
    Inherits="CCOA.App.Meeting.AddMeetingType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <style type="text/css">
        input[type=text]
        {
            width: 200px;
        }
        .table th
        {
            text-align: right;
            margin-right: 10px;
        }
        .table td
        {
            text-align: left;
        }
        .table div
        {
            float: left;
        }
    </style>
    <style type="text/css">
        .Validform_checktip
        {
            color: Red;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#form1").Validform({
                tiptype: 2,
                showAllError: true
            });
        });
    </script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <table class="table">
        <tr>
            <th style="width: 100px">
                类别名称：
            </th>
            <td>
                <div>
                    <input type="text" maxlength="20" datatype="*" nullmsg="*名称不能为空！" name="Name"
                        value="<%=type.Name%>" /></div>
                <div>
                    <span class="Validform_checktip"><span style="color: Red">*</span></span></div>
            </td>
        </tr>
        <tr>
            <th>
                说明：
            </th>
            <td>
                <input type="text" name="Note" value="<%=type.Note%>" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
