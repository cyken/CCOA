﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CCOA.App.Meeting
{
    public partial class Details : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //查询本会议室指定一天的全部预订
            string room = Request.QueryString["room"];
            string date = Request.QueryString["date"];
            if (!string.IsNullOrEmpty(room) && !string.IsNullOrEmpty(date))
            {
                string strSql = "select OID ,DateFrom ,DateTo,Title,a. Fk_Emp,a.Fk_Dept,a.MTResource,a.State,";
                strSql += "b.Name as OrderName,c.Name as RoomName,d.Name as deptName ";
                strSql += "from OA_RoomOrding a,Port_Emp b,OA_Room c ,Port_Dept d ";
                strSql += "where a.FK_Emp=b.No and a.FK_Room=c.No and FK_Room='{0}' and a.Fk_Dept=d.No";
                strSql += " and DATEDIFF(day,CONVERT(varchar(100), DateFrom, 23) ,'{1}')>=0 and ";
                strSql += "DATEDIFF(day,CONVERT(varchar(100), DateTo, 23) ,'{1}')<=0";
                if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
                {
                    strSql = "select OID ,DateFrom ,DateTo,Title,a. Fk_Emp,a.Fk_Dept,a.MTResource,a.State,";
                    strSql += "b.Name as OrderName,c.Name as RoomName,d.Name as deptName ";
                    strSql += "from OA_RoomOrding a,Port_Emp b,OA_Room c ,Port_Dept d ";
                    strSql += "where a.FK_Emp=b.No and a.FK_Room=c.No and FK_Room='{0}' and a.Fk_Dept=d.No";
                    strSql += " and DATEDIFF(DateFrom,'{1}')>=0 and ";
                    strSql += "DATEDIFF(DateTo,'{1}')<=0";
                }
                DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(string.Format(strSql, room, date));
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }

        #region 会议室状态
        public string GetState(object dateFrom, object dateTo, object state)
        {
            return MyHelper.GetMeetingState(dateFrom, dateTo, state);
        }
        #endregion

        public string GetResourceName(object obj)
        {
            return MyHelper.GetResName(obj);
        }
    }
}