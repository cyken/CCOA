﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Meeting
{
    public partial class AddMeetingType : System.Web.UI.Page
    {
        public BP.OA.Meeting.MeetingType type = new BP.OA.Meeting.MeetingType();
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["no"];
            if (!string.IsNullOrEmpty(id))
            {
                type.No = id;
                type.Retrieve();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            string strOp = string.Empty;
            if (string.IsNullOrEmpty(type.No))
            {
                i = BP.OA.UI.PageCommon.Insert(this, new BP.OA.Meeting.MeetingType());
                strOp = "插入";
            }
            else
            {
                i = BP.OA.UI.PageCommon.Update(this, type);
                strOp = "更新";
            }
            if (i > 0)
            {
                Common.Alert(strOp + "成功！", this);
            }
            else
            {
                Common.Alert(strOp + "失败！", this);
            }
        }
    }
}