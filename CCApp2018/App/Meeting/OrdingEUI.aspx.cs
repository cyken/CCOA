﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
namespace CCOA.App.Meeting
{
    public partial class OrdingEUI1 : System.Web.UI.Page
    {

        //#endregion 属性.
        private static DataTable _DTNoCount = null;
        public static DataTable DTNoCount
        {
            get
            {
                string strSql = "select a.No,Fk_Room,DateFrom,DateTo from OA_Room a,OA_RoomOrding b where a.No=b.FK_Room";
                _DTNoCount = BP.DA.DBAccess.RunSQLReturnTable(strSql);
                return _DTNoCount;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }
        ///// <summary>
        ///// 会议室使用状态转换。
        ///// </summary>
        ///// <param name="o"></param>
        ///// <returns></returns>
        public string StateConverter(object o)
        {
            return (Convert.ToString((MeetingRoomState)o));
        }

        ///// <summary>
        ///// 检测会议室预定数量
        ///// </summary>
        ///// <returns></returns>
        [WebMethod]
        public static string CheckOrderNum(string no, string CurrDate)
        {
            int i = 0;
            DataRow[] drs = DTNoCount.Select("no=" + Convert.ToString(no));
            foreach (DataRow dr in drs)
            {
                DateTime dtCurr = Convert.ToDateTime(CurrDate).Date;
                DateTime dtFrom = Convert.ToDateTime(dr["DateFrom"]).Date;
                DateTime dtTo = Convert.ToDateTime(dr["DateTo"]).Date;
                if (dtFrom <= dtCurr && dtTo >= dtCurr)
                {
                    i++;
                }
            }
            return i.ToString();
        }
        //public string GetOrderList(object no)
        //{
        //    string result = string.Empty;
        //    DataRow[] drs = DTNoCount.Select("no=" + Convert.ToString(no));
        //    foreach (DataRow dr in drs)
        //    {
        //        DateTime dtCurr = Convert.ToDateTime(CurrDate);
        //        DateTime dtFrom = Convert.ToDateTime(dr["DateFrom"]);
        //        DateTime dtTo = Convert.ToDateTime(dr["DateTo"]);
        //        if (dtFrom.Date <= dtCurr.Date && dtTo.Date >= dtCurr.Date)
        //        {
        //            result += dtFrom + "--" + dtTo + "<br/>";
        //        }
        //    }
        //    return result;
        //}
    }
}