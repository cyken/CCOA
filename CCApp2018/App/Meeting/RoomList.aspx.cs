﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.Port;
using BP.OA.Meeting;


namespace CCOA.App.Meeting
{
    public partial class RoomList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BP.OA.Meeting.Rooms rooms = new BP.OA.Meeting.Rooms();
            rooms.RetrieveAll();
            Repeater1.DataSource = rooms;
            Repeater1.DataBind();
        }
        public string GetEmpName(object empNo)
        {
            return CCOA.MyHelper.GetEmpName(empNo);
        }
        public string GetResName(object resNo)
        {
            return CCOA.MyHelper.GetResName(resNo);
        }
        public string GetRoomSta(object obj)
        {
            return CCOA.MyHelper.GetRoomSta(obj);
        }
    }
}