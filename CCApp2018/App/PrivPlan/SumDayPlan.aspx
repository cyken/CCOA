﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="SumDayPlan.aspx.cs" Inherits="CCOA.App.PrivPlan.SumDayPlan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        div#nav a
        {
            border: solid 1px #777;
            padding: 1px 1px 1px 1px;
            margin: 3px 3px 3px 3px;
            background-color: #eee;
            font-weight: lighter;
            color: #555;
        }
    </style>
    <script type="text/javascript">
        var theLastIndex = <%=this.TheLastIndex %>;  //至少几个任务，从1开始。
        var theTopIndex = <%=this.TheTopIndex %>;  //至少几个任务，从1开始。
        /*
        $(function ($) {
            for (var i = 2; i <= theLastIndex; i++) {
                setOK();
            }
        });
        $(function ($) {
           refreshButtons();
        });        
        */
        function setOK(index) {
            var divButton = $("#div_reasonButton_" + String(index));
            var divReason = $("#div_reason_" + String(index));
            var legendReason = $("#legend_reason_" + String(index));
            var hiddenReason = $("#hidden_reason_" + String(index));
            divButton.hide();
            divReason.show();
            legendReason.text("成功方式与方法");
            hiddenReason.val(1);
        }
        function setFailed(index) {
            var divButton = $("#div_reasonButton_" + String(index));
            var divReason = $("#div_reason_" + String(index));
            var legendReason = $("#legend_reason_" + String(index));
            var hiddenReason = $("#hidden_reason_" + String(index));
            divButton.hide();
            divReason.show();
            legendReason.text("失败原因与障碍");
            hiddenReason.val(0);
        }
        function setImprove(index)
        {
            var divButton = $("#div_improveButton_" + String(index));
            var divReason = $("#div_improve_" + String(index));
            divButton.hide();
            divReason.show();
        }
        function preSubmit() {
            var taskCount = $("#txt_itemCount").val();
            var hasMain = false;
            var sumHours = 0;
            for(var i=1;i<=taskCount;i++)
            {
                var succeed = $("#hidden_reason_" + String(i)).val();
                var reason = $("#txt_reason_" + String(i)).val();
                var improve = $("#txt_improve_" + String(i)).val();
                if (reason.length < 10||improve.length<10) {alert("目标达成与方式改进都不能少于10个字！");  return false; }
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div id="nav" style="padding: 5px 5px 5px 5px; ">
        <asp:LinkButton runat="server" ID="lbReturn" Text="返回" OnClick="lbReturn_Click"></asp:LinkButton>
        <asp:Label runat="server" ID="lblDate" Font-Size="18px" Font-Bold="true" ForeColor="#B8860B"></asp:Label>
    </div>
    <div id="div_mainPlan">
        <fieldset style="padding: 10px 10px 10px 10px">
            <legend>今日计划简介</legend>
            <asp:Literal runat="server" ID="liTodayDesc"></asp:Literal>
            <asp:HiddenField runat="server" id="txt_itemCount" />
        </fieldset>
        <asp:Repeater runat="server" ID="rptTasks">
            <ItemTemplate>
                <div id="div_task_<%# Eval("Sort") %>">
                    <div style="padding: 5px 5px 1px 5px; margin: 5px 5px 1px 5px">
                        <span id="span_no_<%# Eval("Sort") %>">
                            <%# GetTitleNo(Eval("Sort")) %></span>&nbsp;&nbsp;
                        <%# Convert.ToInt32(Eval("Type"))==0?"可选任务":"主要任务"%>
                        &nbsp;&nbsp; 耗时:<%# Eval("Hours") %>小时 &nbsp;&nbsp;结果：<%# Convert.ToInt32(Eval("Succeed")) == 1 ? "成功" : "失败"%>&nbsp;&nbsp;</div>
                    <div style="padding: 1px 5px 5px 5px; margin: 1px 5px 5px 5px; border: dashed 1px #ccc;
                        height: 70px;width:600px; position:relative;">
                        <div>
                        <%# Eval("Task") %>
                        </div>
                        <div id="div_reasonButton_<%# Eval("Sort") %>" style="width: 120px; position:absolute; margin-left: 50px; bottom:0px;<%# Convert.ToInt32(Eval("Checked"))==1?"display:none;":"" %>">
                            <input type="button" value="成功"
                                onclick="setOK(<%# Eval("Sort") %>)" />
                            <input type="button" value="失败"
                                onclick="setFailed(<%# Eval("Sort") %>)" />
                        </div>
                    </div>
                    <div id="div_reason_<%# Eval("Sort") %>" style="margin-left: 50px; width: 600px;
                        position: relative;<%# Convert.ToInt32(Eval("Checked"))==0?"display:none;":"" %>">
                        <fieldset style="padding: 10px 10px 10px 10px;height:90px;">
                            <legend id="legend_reason_<%# Eval("Sort") %>"><%# GetResultTitle(Eval("Succeed"),Eval("Checked")) %></legend>
                            <input type="hidden" id="hidden_reason_<%# Eval("Sort") %>" name='hidden_reason_<%# Eval("Sort") %>' value='<%# Eval("Succeed") %>' />
                            <textarea id="txt_reason_<%# Eval("Sort") %>" name='txt_reason_<%# Eval("Sort") %>'
                                style="width: 550px; height: 50px;"><%# Eval("Reason") %></textarea>
                            <div id="div_improveButton_<%# Eval("Sort") %>" style="width: 120px; position: absolute;
                                margin-left: 50px; bottom: 0px;<%# Convert.ToInt32(Eval("Checked"))==1?"display:none;":"" %>">
                                <input type="button" value="还要改进" onclick="setImprove(<%# Eval("Sort") %>)" />
                            </div>
                        </fieldset>
                    </div>
                    <div id="div_improve_<%# Eval("Sort") %>" style="margin-left: 100px; width: 600px;
                        position: relative;<%# Convert.ToInt32(Eval("Checked"))==0?"display:none;":"" %>">
                        <fieldset style="padding: 10px 10px 10px 10px;">
                            <legend>方式改进</legend>
                            <textarea id="txt_improve_<%# Eval("Sort") %>" name='txt_improve_<%# Eval("Sort") %>'
                                style="width: 550px; height: 90px;"><%# Eval("Improve") %></textarea>
                        </fieldset>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div style="text-align: center;">
        <!--input type="button" value="提交" name="btnSubmit" onclick="preSubmit()" /-->
        <asp:Button runat="server" Text="提交" OnClick="btnSubmit_Onclick" OnClientClick="return preSubmit()" />
     </div>
</asp:Content>
