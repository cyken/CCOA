﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA.PrivPlan;
using System.Globalization;
namespace CCOA.App.PrivPlan
{
    public partial class MyPlan : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        private string[] ChineseNo = new String[] { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" };
        /// <summary>
        /// 创建计划截至时间
        /// </summary>
        public string CreatePlanTimeLine = BP.OA.Main.GetConfigItem("CreatePlanTimeLine") == null ? "09:00" : BP.OA.Main.GetConfigItem("CreatePlanTimeLine");
        /// <summary>
        /// 计划总结开始时间
        /// </summary>
        public string SumPlanTimeLine = BP.OA.Main.GetConfigItem("SumPlanTimeLine") == null ? "16:00" : BP.OA.Main.GetConfigItem("SumPlanTimeLine");
        //权限控制
        //private string FuncNo = null;
        //当登录者是否使用过计划？
        private bool IsStart
        {
            get { return BP.OA.PrivPlan.PrivPlan.IsStart; }
        }
        /// <summary>
        /// 今天是否有计划
        /// </summary>
        private bool TodayIsPlan
        {
            get { return BP.OA.PrivPlan.PrivPlan.TodayIsPlan(BP.Web.WebUser.No,this.rDate); }
        }
        /// <summary>
        /// 当前登录人
        /// </summary>
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        /// <summary>
        /// 传入日期
        /// </summary>
        private DateTime rDate
        {
            get
            {
                string sDate = Convert.ToString(Request.QueryString["Date"]);
                if (String.IsNullOrEmpty(sDate))
                    return DateTime.Now;
                else
                    return Convert.ToDateTime(sDate);
            }
        }
        public string GetTitleNo(object evalNo)
        {
            int i = Convert.ToInt32(evalNo);
            return String.Format("第{0}项", this.ChineseNo[i - 1]);
        }
        
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
                if (!this.IsStart) { this.Response.Redirect("~/App/PrivPlan/Start.aspx", true); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.hlWeek.NavigateUrl = String.Format("WeekView.aspx?Date={0:yyyy-MM-dd}", this.rDate);
                this.hlMonth.NavigateUrl = String.Format("CheckMonthView.aspx?Date={0:yyyy-MM-dd}", this.rDate);
                this.lbToday.ForeColor = String.Format("{0:yyyy-MM-dd}", this.rDate) == String.Format("{0:yyyy-MM-dd}", DateTime.Now) ? System.Drawing.Color.Red : System.Drawing.Color.FromArgb(5, 5, 5);
                this.hlEdit.NavigateUrl = String.Format("EditDayPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate);
                this.hlEdit1.NavigateUrl = String.Format("EditDayPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate);
                this.hlSum.NavigateUrl = String.Format("SumDayPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate);
                this.lblDate.Text = String.Format("&nbsp;&nbsp;{0:dddd}&nbsp;&nbsp;{0:yyyy年MM月dd日}", this.rDate);
                this.RefreshStatusBar();
                this.LoadData();
            }
        }
        private void RefreshStatusBar()
        {
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            bool blnR = pp.RetrieveByAttrAnd(PrivPlanAttr.FK_UserNo, BP.Web.WebUser.No, PrivPlanAttr.PlanDate, String.Format("{0:yyyy-MM-dd}", this.rDate));
            //根据选择日期判断是否已写计划
            if (blnR)
            {
                this.pNoTodayPlan.Style["display"] = "none";
                this.divCommon.Style["display"] = "block";
                //当天已写计划
                if (String.Format("{0:yyyy-MM-dd}", this.rDate) == String.Format("{0:yyyy-MM-dd}", DateTime.Now))
                {
                    //当前时间小于允许创建计划时间，可以编制计划，不可以总结
                    if (String.Format("{0:HH:mm}", DateTime.Now).CompareTo(this.CreatePlanTimeLine) < 0)
                    {
                        this.hlEdit.Enabled = true;
                        this.hlSum.Enabled = false;
                        this.lbDayState.Text = "你还可以进行编制计划，直到"+this.CreatePlanTimeLine;
                    }
                    else if (String.Format("{0:HH:mm}", DateTime.Now).CompareTo(this.SumPlanTimeLine) > 0)
                    {
                        //当前时间大于允许总结时间，可以进行总结，不能进行编制计划
                        this.hlEdit.Enabled = false;
                        this.hlSum.Enabled = true;
                        if(!pp.Checked)
                            this.lbDayState.Text = "你可以开始总结今日计划了！";
                        else
                            this.lbDayState.Text = "你还可以对总结进行再编辑，直到明天早上";
                    }
                    else
                    {
                        this.liScore.Text = "执行阶段";
                        this.hlEdit.Enabled = false;
                        this.hlSum.Enabled = false;
                        this.lbDayState.Text = this.SumPlanTimeLine+"你就可以进行计划总结了！";
                    }
                }
                else if (String.Format("{0:yyyy-MM-dd}", this.rDate).CompareTo(String.Format("{0:yyyy-MM-dd}", DateTime.Now)) > 0)//明日计划
                {
                    this.liScore.Text = "请进行编制";
                    this.hlEdit.Enabled = true;
                    this.hlSum.Enabled = false;
                    this.lbDayState.Text = String.Format("你还可以进行编制计划，直到{0:yyyy年MM月dd日} {1}",this.rDate,this.CreatePlanTimeLine);
                }
                else
                {
                    this.hlEdit.Enabled = false;
                    this.hlSum.Enabled = false;
                    if (pp.Checked)
                        this.lbDayState.Text = "有计划，有总结，此日你一定很棒！";
                    else
                        this.lbDayState.Text = "今日有计划，没有总结，此日你一定是虎头蛇尾！";
                }
            }
            else
            {
                this.pNoTodayPlan.Style["display"] = "block";
                this.divCommon.Style["display"] = "none";
                if (String.Format("{0:yyyy-MM-dd}", this.rDate) == String.Format("{0:yyyy-MM-dd}", DateTime.Now))
                {
                    if (String.Format("{0:HH:mm}", DateTime.Now).CompareTo(this.CreatePlanTimeLine) < 0)
                    {
                        this.hlEdit1.Enabled = true;
                        this.hlEdit.Enabled = true;
                        this.hlSum.Enabled = false;
                    }
                    else if (String.Format("{0:HH:mm}", DateTime.Now).CompareTo(this.SumPlanTimeLine) > 0)
                    {
                        this.hlEdit1.Enabled = false;
                        this.lblMsg.Text = "今日无计划，所以不用总结！今日你状态一定很差！";
                        this.hlEdit.Enabled = false;
                        this.hlSum.Enabled = false;
                    }
                    else
                    {
                        this.hlEdit1.Enabled = false;
                        this.lblMsg.Text = "今日无计划！今日你状态一定很差！";
                        this.hlEdit.Enabled = false;
                        this.hlSum.Enabled = false;
                    }
                }
                else if (String.Format("{0:yyyy-MM-dd}", this.rDate).CompareTo(String.Format("{0:yyyy-MM-dd}", DateTime.Now)) > 0)
                {
                    this.hlEdit1.Enabled = true;
                    this.hlEdit.Enabled = true;                    
                    this.hlSum.Enabled = false;
                }
                else
                {
                    this.hlEdit1.Enabled = false;
                    this.lblMsg.Text = "无计划，时间早已过期！今日你状态一定很差！";
                    this.hlEdit.Enabled = false;
                    this.hlSum.Enabled = false;
                } 
            }
        }
        private void LoadData()
        {
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            bool blnR = pp.RetrieveByAttrAnd(PrivPlanAttr.FK_UserNo, BP.Web.WebUser.No, PrivPlanAttr.PlanDate, String.Format("{0:yyyy-MM-dd}", this.rDate));
            if (blnR)
            {
                this.liTodayDesc.Text = pp.MyDoc;
            }
            else
            {
                //this.Response.Redirect(String.Format("EditDayPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate), true);
                return;
            }
                      
            string sql = String.Format("select Sort,Task,Hours,Score,Reason,Succeed,Improve,PrivPlanItemType as Type,'{1}' as Checked  from OA_PrivPlanItem where FK_PrivPlan='{0}' order by Sort",pp.OID,pp.Checked);
            System.Data.DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            this.rptTaskItems.DataSource = dt;
            this.rptTaskItems.DataBind();
        }
        public string GetResultTitle(object evalSucceed,object evalChecked)
        { 
            bool ck=Convert.ToBoolean(evalChecked);
            int su = Convert.ToInt32(evalSucceed);
            if (!ck)
                return "目标达成";
            else if (su == 0)
                return "失败原因与障碍";
            else
                return "成功方式与方法";
        }
        #endregion

        #region //3.页面事件(Page Event)

        protected void lbNextDay_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}",this.rDate.AddDays(1)),true);
        }

        protected void lbToday_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}", DateTime.Now), true);
        }

        protected void lbPreviousDay_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate.AddDays(-1)), true);
        }
        #endregion
    }
}