﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkRegister.aspx.cs" Inherits="CCOA.App.Attendance.WorkRegister" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <thead>
                <tr>
                    <td>
                        登记次序
                    </td>
                    <td>
                        登记类型
                    </td>
                    <td>
                        规定时间
                    </td>
                    <td>
                        登记时间
                    </td>
                    <td>
                        操作
                    </td>
                </tr>
                <asp:Repeater ID="repeater1" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem,"Sequence")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem,"RegisterType")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem,"CanRegisterTime")%>
                            </td>
                            <td>
                                <%#CheckState(Eval("OID"))%>
                            </td>
                            <td>
                                <%#CheckState(Eval("CanRegisterTime"), Eval("AheadRegisterTime"), Eval("BehindRegisterTime"), Eval("RegisterType"), Eval("OID"))%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </thead>
        </table>
    </div>
    </form>
    <a href="workregister.aspx?id=2">dfffffffff</a>
</body>
</html>
