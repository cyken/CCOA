﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="TesNewCalendarTask.aspx.cs" Inherits="CCOA.App.Calendar.TesNewCalendarTask" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" action="EditCalendarTask.ashx">
    <div>
        OID:<asp:TextBox ID="ui_OID" runat="server" Width="200"></asp:TextBox><br/>
        UserNo:<asp:TextBox ID="ui_FK_UserNo" runat="server" Width="200"></asp:TextBox><br/>
        Title:<asp:TextBox ID="ui_Title" runat="server" Width="200"></asp:TextBox><br/>
        StartDate:<asp:TextBox ID="ui_StartDate" runat="server" Width="200"></asp:TextBox><br/>
        EndDate:<asp:TextBox ID="ui_EndDate" runat="server" Width="200"></asp:TextBox><br/>
        Location:<asp:TextBox ID="ui_Location" runat="server" Width="200"></asp:TextBox><br/>
        Notes:<asp:TextBox ID="ui_Notes" runat="server" Width="200"></asp:TextBox><br/>
        Url:<asp:TextBox ID="ui_Url" runat="server" Width="200"></asp:TextBox><br/>
        IsAllDay:<asp:TextBox ID="ui_IsAllDay" runat="server" Width="200"></asp:TextBox><br/>
        Reminder:<asp:TextBox ID="ui_Reminder" runat="server" Width="200"></asp:TextBox><br/>
        IsNew:<asp:TextBox ID="ui_IsNew" runat="server" Width="200"></asp:TextBox><br/>
        CalendarCatagory:<asp:TextBox ID="ui_CalendarCatagory" runat="server" Width="200"></asp:TextBox><br/>
    </div>
    <input type="submit" value="submit" />
    </form>
</body>
</html>
