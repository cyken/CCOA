﻿<%@ Page Language="C#" MasterPageFile="~/App/DailyRecord/Site.Master" AutoEventWireup="true"
    CodeBehind="NewSheet.aspx.cs" ValidateRequest="false" ClientIDMode="Static" Inherits="CCOA.App.DailyRecord.NewSheet" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
    border-collapse: collapse;
            border-spacing: 0;
            margin-left: 0px;
            display: table;
            border-color: gray;
            width: 95%;
            float: left;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 100px;
            text-align: right;
             background-image:none;
             background-color:White;
              font-size:14px;
              color:#336699;
              font-weight:bold;
              margin:inherit;
        }
        td.text
        {
            text-align: left;
            padding-left: 10px;
        }
    </style>
    <link rel="stylesheet" href="/ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="/ctrl/kindeditor/plugins/code/prettify.css" />
    <%--<script type="text/javascript" charset="utf-8" src="/ctrl/kindeditor/kindeditor.js"></script>--%>
    <script type="text/javascript" src="../../Ctrl/kindeditor/kindeditor-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script type="text/javascript">
        KindEditor.ready(function (K) {
            var editor1 = K.create('#TB_Doc', {
                cssPath: '/ctrl/kindeditor/plugins/code/prettify.css',
                uploadJson: '/ctrl/kindeditor/asp.net/upload_json.ashx',
                fileManagerJson: '/ctrl/kindeditor/asp.net/file_manager_json.ashx',
                allowFileManager: true,
                afterBlur: function () {
                    this.sync();
                }
            });
            editor1.sync();
            prettyPrint();
        });



        //判断
        function check_login() {

            if (document.getElementById("TB_Doc").value.length < 50) {
                alert("日志内容不能少于50字！");

                return false;
            }
            else {
                return true;
            }
        }
        window.onload = function () {
            var myDate = new Date();

            document.getElementById("TB_Title").value = myDate.toLocaleString();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div>
    </div>
    <div style="margin: 30px 10px; width: 820px; height: 350px;">
        <table class="doc-table">
        <tr>
        <td  class=" title">注：</td>
        <td><a style=" color:Red">一般日志直接选择共享类型操作，无需选择日志类型。周月总结选择共享类型，然后选择日志类型操作！</a></td>
        </tr>
            <tr>
                <td class="title">
                    &nbsp;共享类型:
                </td>
                <td>
                    <asp:RadioButton ID="RB_0" Text="私有" GroupName="s" runat="server" />
                    <asp:RadioButton ID="RB_1" Text="共享" runat="server" GroupName="s" Checked="true" />
                    <asp:RadioButton ID="RB_2" Text="公开" runat="server" GroupName="s" />
                           &nbsp&nbsp  &nbsp&nbsp  &nbsp&nbsp  &nbsp&nbsp&nbsp&nbsp&nbsp;日志类型: 
                      <asp:RadioButton ID="RB_3" Text="周总结" runat="server" GroupName="z" />
                          <asp:RadioButton ID="RB_4" Text="月总结" runat="server" GroupName="z" />
                </td>
                
            </tr>
            <tr>
                <td class="title">
                    &nbsp;标题:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="TB_Title" runat="server" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;内容:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="TB_Doc" runat="server"  TextMode="MultiLine" Style="width: 97%;
                        height: 275px;"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: center; width: 800px">
        <asp:Button runat="server" Text="保存" ID="btnSub" OnClick="btnSub_Click" OnClientClick=" return check_login()" /></div>
</asp:Content>
