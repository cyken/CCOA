﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true"   enableEventValidation="false" MaintainScrollPositionOnPostback="true"
    CodeBehind="MySheet.aspx.cs" Inherits="CCOA.App.DailyRecord.MySheet" %>

<%@ Register Src="../Pub.ascx" TagName="Pub" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Del(oid) {
            if (confirm('您确定要删除吗？') == false)
                return;
            var url = 'Do.aspx?DoType=Del&OID=' + oid;
            val = window.showModalDialog(url, '', 'dialogHeight: 1px; dialogWidth: 1px; center: yes; help: no');
            window.location.href = window.location.href;

        }
      
    </script>
    <style type="text/css">
        .style1
        {
            width: 100px;
        }
    </style>
  <style type="text/css">
      .aaa{ color:#000; text-decoration:none;}
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%; border-spacing: 5px; border-collapse: separate; table-layout: fixed;
        word-wrap: break-word">
        <asp:Repeater ID="rep_List" runat="server">
            <ItemTemplate>
                <tr>
                    <td rowspan="2" valign="top" style="width: 100px">
                        <img src='/DataUser/UserIcon/<%#Eval("Rec")%>.png' width='100px' onerror="this.src='/DataUser/UserIcon/Default.png'"
                            border="0" />
                    </td>
                    <td>
                        <div style="float: left; font-weight: bold; font-size: 15px; word-wrap: break-word;
                            word-break: break-all">
                            <h3>
                                    <img src="./Img/<%#Eval("DRShe")%>.png" class="Icon" />
                                <%#Eval("Title")%>
                            </h3>
                        </div>
                        <div style="float: right; font-weight: bold; font-size: 15px; color: Green">
                            <%#Eval("RDT") %>&nbsp&nbsp<%#GetWeekNameOfDay( Convert.ToDateTime( Eval("RDT")))%></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <%#Eval("Doc")%>
                        </div>
                  <br />
                        <div style="float: right">
                         <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("OID") %>' />
                            <a href="NewSheet.aspx?OID=<%#Eval("OID")%>">
                                <img src='../../WF/Img/Btn/Edit.gif' border="0" />编辑</a> | <asp:LinkButton ID="LinkBut" runat="server"   OnClick="butDele_Click"  OnClientClick="return confirm('确定要删除吗?')"  ><img src='../../WF/Img/Btn/Delete.gif' border="”0“" />删除</asp:LinkButton>
                        </div>
                        
                        <div>   <h3 style=" font-weight:normal; height:10px; color:Blue">评论:</h3></div>
                        <div>
                         <asp:Repeater ID="sublist" runat="server" DataSource='<%# getdt(Eval("OID").ToString()) %>'>
                                 <ItemTemplate>
                                    <h5 style="font-size: 15px; font-weight: normal; height: 5px">
                                        &nbsp&nbsp&nbsp<a style=" color:Green"><%#Eval("Critic") %>:</a><%# BP.DA.DataType.ParseText2Html(Convert.ToString( Eval("Comm")))%>&nbsp&nbsp<a style=" font-size:10px"><%#Eval("CommDate") %></a></h5><br /><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <hr>
    <uc1:Pub ID="Pub1" runat="server" />
</asp:Content>
