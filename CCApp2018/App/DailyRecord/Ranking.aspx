﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/DailyRecord/Site.Master" AutoEventWireup="true"
    CodeBehind="Ranking.aspx.cs" Inherits="CCOA.App.DailyRecord.Ranking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"><br />
    <div style="text-align: center">
       <a  style=" color:Blue; font-size:14px">日志周排行榜</a>
    </div><br />
    <asp:Repeater ID="Rep_List0" runat="server">
        <HeaderTemplate>
            <table width="100%">
                <tr >
                    <th  align="center">
                        名次
                    </th>
                    <th  align="center">
                        姓名
                    </th>
                    <th align="center">
                        规定篇数
                    </th>
                    <th width="20%">
                        实际篇数
                    </th>
                    <th width="20%">
                        完成率
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td style="text-align: center">
                    <%#Container.ItemIndex+1%>
                </td>
                <%--       <td > <%#Eval("Rec")%> </td>--%>
                <td style="text-align: center">
                    <%#GetCHName(Convert.ToString( Eval("Rec"))) %>
                </td>
                <td style="text-align: center">
                    <%#7 + "&nbsp篇"%>
                </td>
                <td style="text-align: center">
                    <%#Eval("total") +"&nbsp篇"%>
                </td>
                <td style="text-align: center">
                    <%#(int)(((Convert.ToDouble(Eval("total")))/7)*100)+"%"%>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table></FooterTemplate>
    </asp:Repeater>
    <br />
    <div style="text-align: center">
        <a style=" color:Blue; font-size:14px">日志总排行榜</a></div><br />
    <asp:Repeater ID="Rep_List1" runat="server"  DataSource="<%#GetRanking()%>" >
        <HeaderTemplate>
            <table width="100%" >
                <tr >
                    <th >
                        名次
                    </th>
                    <th>
                        姓名
                    </th>
                    <th>
                        私有日志
                    </th>
                    <th>
                        共享日志
                    </th>
                    <th>
                        公开日志
                    </th>
                    <th>
                        周总结
                    </th>
                    <th>
                        月总结
                    </th>
                    <th>
                        日志总数
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr >
                <td  style=" text-align:center">
                    <%#Container.ItemIndex+1%>
                </td>
                <td style=" text-align:center">
                    <%#GetCHName(Convert.ToString(Eval("rec"))) %>
                </td>
                <td style=" text-align:center">
                    <%#GetPrivate(Convert.ToString(Eval("rec"))) + "&nbsp篇"%>
                </td>
                <td style=" text-align:center"><%# GetShare(Convert.ToString(Eval("rec"))) + "&nbsp"%></td>
                <td style=" text-align:center"><%#GetPublic(Convert.ToString(Eval("rec"))) + "&nbsp篇"%></td>
                <td style=" text-align:center"><%#GetWeek(Convert.ToString(Eval("rec"))) + "&nbsp篇"%></td>
                <td style=" text-align:center"><%#GetMonth(Convert.ToString(Eval("rec"))) + "&nbsp篇"%></td>
                <td style=" text-align:center"><%#Eval("total") + "&nbsp篇"%></td>
              
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table></FooterTemplate>
    </asp:Repeater>
</asp:Content>
