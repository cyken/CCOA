﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using BP;
using BP.En;
using BP.DA;
using BP.Sys;
using BP.Web;
using BP.OA;
using BP.Port;

namespace CCOA.App.CheckWork.DataService
{
    public partial class CKDataService : System.Web.UI.Page
    {

        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(Request[param], System.Text.Encoding.UTF8);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (BP.Web.WebUser.No == null)
            //    return;

            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(getUTF8ToString("method")))
                method = getUTF8ToString("method");

            switch (method)
            {
                case "getCKCollect"://获取考勤模块
                    s_responsetext = GetCKCollect();
                    break;
                case "getRWType"://设置签到类型
                    s_responsetext = GetRWType();
                    break;
                case "getRWState"://设置签到状态
                    s_responsetext = GetRWState();
                    break;
                case "opRWState"://设置签到状态
                    s_responsetext = OpRWState();
                    break;
                case "registerWork"://签到
                    s_responsetext = RegisterWork();
                    break;
                case "getRegister"://获取签到类型
                    s_responsetext = GetRegister();
                    break;
                case "opRWType"://设置签到类型
                    s_responsetext = opRWType();
                    break;
                case "registerWorkDJ"://签到登记
                    s_responsetext = RegisterWorkDJ();
                    break;
                case "getQJ"://获取请假
                    s_responsetext = GetQJ();
                    break;
                case "registerQJ"://请假
                    s_responsetext = RegisterQJ();
                    break;
                case "getWC"://获取外出
                    s_responsetext = GetWC();
                    break;
                case "registerWC"://外出
                    s_responsetext = RegisterWC();
                    break;
                case "getCC"://获取出差
                    s_responsetext = GetCC();
                    break;
                case "registerCC"://出差
                    s_responsetext = RegisterCC();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            this.Response.Charset = "UTF-8";
            this.Response.ContentEncoding = System.Text.Encoding.UTF8;
            this.Response.ContentType = "text/html";
            this.Response.Expires = 0;
            this.Response.Write(s_responsetext);
            this.Response.End();
        }
        /// <summary>
        /// 获取 出差
        /// </summary>
        /// <returns></returns>
        public string GetCC()
        {
            string nyr = DateTime.Now.ToString("yyyy-MM-dd");
            string sql = "select * from CW_RegisterWork where FK_RWType='CC' and NYR='" + nyr + "'";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            return CKDbOperator.GetJsonFromTable(dt);
        }
        /// <summary>
        /// 出差
        /// </summary>
        /// <returns></returns>
        public string RegisterCC()
        {
            string starttime = getUTF8ToString("starttime");
            string endtime = getUTF8ToString("endtime");
            string demo = getUTF8ToString("demo");
            string addr = getUTF8ToString("addr");

            BP.OA.CheckWork.RegisterWork rw = new BP.OA.CheckWork.RegisterWork();
            rw.FK_Emp = BP.Web.WebUser.No;
            rw.FK_RWType = "CC";
            rw.FK_RWState = "CC";
            rw.StartTime = starttime;
            rw.EndTime = endtime;
            rw.DCount = "1";
            rw.Demo = demo;
            rw.Addr = addr;
            rw.NYR = DateTime.Now.ToString("yyyy-MM-dd");
            rw.RecordTime = DateTime.Now.ToString();
            return rw.Insert().ToString();
        }
        /// <summary>
        /// 获取外出
        /// </summary>
        /// <returns></returns>
        public string GetWC()
        {
            string nyr = DateTime.Now.ToString("yyyy-MM-dd");
            string sql = "select * from CW_RegisterWork where FK_RWType='WC' and NYR='" + nyr + "'";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            return CKDbOperator.GetJsonFromTable(dt);
        }
        /// <summary>
        /// 外出
        /// </summary>
        /// <returns></returns>
        public string RegisterWC()
        {
            string starttime = getUTF8ToString("starttime");
            string endtime = getUTF8ToString("endtime");
            string demo = getUTF8ToString("demo");
            string addr = getUTF8ToString("addr");

            BP.OA.CheckWork.RegisterWork rw = new BP.OA.CheckWork.RegisterWork();
            rw.FK_Emp = BP.Web.WebUser.No;
            rw.FK_RWType = "WC";
            rw.FK_RWState = "WC";
            rw.StartTime = starttime;
            rw.EndTime = endtime;
            rw.DCount = "1";
            rw.Demo = demo;
            rw.Addr = addr;
            rw.NYR = DateTime.Now.ToString("yyyy-MM-dd");
            rw.RecordTime = DateTime.Now.ToString();
            return rw.Insert().ToString();
        }
        /// <summary>
        /// 获取 请假列表
        /// </summary>
        /// <returns></returns>
        public string GetQJ()
        {
            string nyr = DateTime.Now.ToString("yyyy-MM-dd");
            string sql = "select * from CW_RegisterWork where FK_RWType='QJ' and NYR='" + nyr + "'";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            return CKDbOperator.GetJsonFromTable(dt);
        }
        /// <summary>
        /// 请假
        /// </summary>
        public string RegisterQJ()
        {
            string starttime = getUTF8ToString("starttime");
            string endtime = getUTF8ToString("endtime");
            string demo = getUTF8ToString("demo");

            BP.OA.CheckWork.RegisterWork rw = new BP.OA.CheckWork.RegisterWork();
            rw.FK_Emp = BP.Web.WebUser.No;
            rw.FK_RWType = "QJ";
            rw.FK_RWState = "QJ";
            rw.Demo = demo;
            //开始时间 结束时间 记录时间
            rw.StartTime = starttime;
            rw.EndTime = endtime;
            rw.DCount = "1";
            rw.NYR = DateTime.Now.ToString("yyyy-MM-dd");
            rw.RecordTime = DateTime.Now.ToString();

            return rw.Insert().ToString();
        }

        /// <summary>
        /// 获取签到
        /// </summary>
        public string GetRegister()
        {
            string nyr = DateTime.Now.ToString("yyyy-MM-dd");
            string sql = " select CW_RWType.No, RegularTime, RegisterTime,  "
                      + "  case FK_RWState when '1' then '签到' when '2' then '迟到' when  '3' then '早退'  end  as FK_RWState "
                      + "   ,Demo, NYR from  CW_RWType left join CW_RegisterWork "
                      + " on CW_RWType.No = CW_RegisterWork.FK_RWType  and  CW_RegisterWork.NYR ='" + nyr + "'";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            return CKDbOperator.GetJsonFromTable(dt);
        }

        /// <summary>
        /// 签到
        /// </summary>
        public string RegisterWork()
        {
            string demo = getUTF8ToString("demo");
            string type = getUTF8ToString("type");
            string regulartime = BP.DA.DBAccess.RunSQLReturnString("select RegularTime from CW_RWType  where No = '" + type + "'");
            string registertime = DateTime.Now.ToString("hh:mm");

            BP.OA.CheckWork.RegisterWork rw = new BP.OA.CheckWork.RegisterWork();
            rw.FK_Emp = BP.Web.WebUser.No;
            rw.FK_RWType = type;
            rw.RegisterTime = registertime;
            rw.NYR = DateTime.Now.ToString("yyyy-MM-dd");
            string state = "1";
            switch (type)
            {
                case "S1"://上班(上午)
                    if (DateTime.Compare(DateTime.Parse(regulartime), DateTime.Parse(registertime)) < 0)
                    {
                        state = "2";//迟到
                    }
                    break;
                case "X1"://下班(上午)
                    if (DateTime.Compare(DateTime.Parse(regulartime), DateTime.Parse(registertime)) > 0)
                    {
                        state = "3";//早退
                    }
                    break;
                case "S2"://上班(下午)
                    if (DateTime.Compare(DateTime.Parse(regulartime), DateTime.Parse(registertime)) < 0)
                    {
                        state = "2";//迟到
                    }
                    break;
                case "X2"://下班(上午)
                    if (DateTime.Compare(DateTime.Parse(regulartime), DateTime.Parse(registertime)) > 0)
                    {
                        state = "3";//早退
                    }
                    break;
            }
            rw.FK_RWState = state;
            rw.Demo = demo;
            return rw.Insert().ToString();
        }
        /// <summary>
        /// 获取考勤数据列表
        /// </summary>
        /// <returns></returns>
        public string GetCKCollect()
        {
            string sql = "select FK_RWType,RegisterTime,FK_RWState,Demo  from CW_RegisterWork  order by NYR ";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            // return CCOA.Main.JsonConvert.GetJsonString(dt, false);
            return CKDbOperator.GetJsonFromTable(dt);
        }
        /// <summary>
        /// 签到类型
        /// </summary>
        public string GetRWType()
        {
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable("select * from cw_rwtype");
            return CKDbOperator.GetJsonFromTable(dt);
        }
        /// <summary>
        /// 签到状态
        /// </summary>
        public string GetRWState()
        {
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable("select * from CW_RWState");
            return CKDbOperator.GetJsonFromTable(dt);
        }
        /// <summary>
        /// 签到状态  修改
        /// </summary>
        public string OpRWState()
        {
            //类型
            string optype = getUTF8ToString("op");
            string no = getUTF8ToString("no");
            string name = getUTF8ToString("name");
            int iResult = 0;

            switch (optype)
            {
                case "add":
                    iResult = AddState(no, name);
                    break;
                case "modify":
                    iResult = UpdateState(no, name);
                    break;
                case "delete":
                    iResult = DelteState(no, name);
                    break;
            }
            return iResult.ToString();
        }
        /// <summary>
        /// 状态新增
        /// </summary>
        public int AddState(string no, string name)
        {
            BP.OA.CheckWork.RWState rwstate = new BP.OA.CheckWork.RWState();
            rwstate.No = no;
            rwstate.Name = name;
            return rwstate.Insert();
        }
        /// <summary>
        /// 状态修改
        /// </summary>
        public int UpdateState(string no, string name)
        {
            BP.OA.CheckWork.RWState rwstate = new BP.OA.CheckWork.RWState();
            rwstate.No = no;
            rwstate.Name = name;
            return rwstate.Update();
        }
        /// <summary>
        /// 状态删除
        /// </summary>
        public int DelteState(string no, string name)
        {
            BP.OA.CheckWork.RWState rwstate = new BP.OA.CheckWork.RWState();
            rwstate.No = no;
            rwstate.Name = name;
            return rwstate.Delete();
        }

        /// <summary>
        /// 签到类型 修改
        /// </summary>
        /// <returns></returns>
        public string opRWType()
        {
            //类型
            string optype = getUTF8ToString("op");
            string no = getUTF8ToString("no");
            string name = getUTF8ToString("name");
            string regulartime = getUTF8ToString("regulartime");
            int iResult = 0;

            switch (optype)
            {
                case "add":
                    iResult = AddType(no, name, regulartime);
                    break;
                case "modify":
                    iResult = UpdateType(no, name, regulartime);
                    break;
                case "delete":
                    iResult = DelteType(no, name, regulartime);
                    break;
            }
            return iResult.ToString();

        }

        /// <summary>
        /// 签到类型 新增
        /// </summary>
        /// <param name="no"></param>
        /// <param name="name"></param>
        /// <param name="regulartime"></param>
        /// <returns></returns>
        public int AddType(string no, string name, string regulartime)
        {
            BP.OA.CheckWork.RWType rwtype = new BP.OA.CheckWork.RWType();
            rwtype.No = no;
            rwtype.Name = name;
            rwtype.RegularTime = regulartime;
            return rwtype.Insert();

        }
        /// <summary>
        /// 签到类型 修改
        /// </summary>
        /// <param name="no"></param>
        /// <param name="name"></param>
        /// <param name="regulartime"></param>
        /// <returns></returns>
        public int UpdateType(string no, string name, string regulartime)
        {
            BP.OA.CheckWork.RWType rwtype = new BP.OA.CheckWork.RWType();
            rwtype.No = no;
            rwtype.Name = name;
            rwtype.RegularTime = regulartime;
            if (rwtype.IsExits)
            {
                return rwtype.Update();
            }
            return rwtype.Insert();
        }
        /// <summary>
        /// 签到类型 删除
        /// </summary>
        /// <param name="no"></param>
        /// <param name="name"></param>
        /// <param name="regulartime"></param>
        /// <returns></returns>
        public int DelteType(string no, string name, string regulartime)
        {
            BP.OA.CheckWork.RWType rwtype = new BP.OA.CheckWork.RWType();
            rwtype.No = no;
            rwtype.Name = name;
            rwtype.RegularTime = regulartime;
            if (rwtype.IsExits)
            {
                return rwtype.Delete();
            }
            return rwtype.Insert();
        }

        /// <summary>
        /// 签到 登记
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public string RegisterWorkDJ()
        {
            string no = getUTF8ToString("no");
            string demo = getUTF8ToString("demo");

            BP.OA.CheckWork.RegisterWork rw = new BP.OA.CheckWork.RegisterWork();

            rw.FK_Emp = BP.Web.WebUser.No;
            rw.FK_RWType = no;
            rw.FK_RWState = "1";
            rw.RegisterTime = DateTime.Now.ToString("hh:mm");
            rw.NYR = DateTime.Now.ToString("yyyy-MM-dd");
            if (1 == 1)
            {
                rw.FK_RWState = "2";
            }
            rw.Demo = demo;
            return rw.Insert().ToString();
        }
    }
}