﻿

$(function () {
 
    LoadGrid();
});

//加载抄送列表
function LoadGrid() {
    $("#pageloading").show();
    Application.data.getCKCollect(callBack, this);
} 
//回调函数
function callBack(jsonData, scope) {
    if (jsonData) {
        var pushData = eval('(' + jsonData + ')');
        var grid = $("#maingrid").ligerGrid({
            columns: [
                   { display: '类型', name: 'FK_RWType', width: 380, align: 'left' },
                   { display: '签到时间', name: 'RegisterTime' },
                   { display: '签到状态', name: 'FK_RWState' },
                   { display: '备注', name: 'Demo', width: 200 }
                  
                   ],
            pageSize: 20,
            data: pushData,
            rownumbers: true,
            height: "99%",
            width: "99%",
            columnWidth: 100,
            onReload: LoadGrid
        });
    }
    else {
        $.ligerDialog.warn('加载数据出错，请关闭后重试！');
    }
    $("#pageloading").hide();
}
