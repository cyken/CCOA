﻿var title = "新增状态";
//初始化
$(function () {
    var toolBarManager = $("#toptoolbar").ligerToolBar({ items: [
                { text: '新增', click: itemclick, icon: 'add' },
                { line: true },
                 { text: '修改', click: itemclick, icon: 'modify' },
                { line: true },
                { text: '删除', click: itemclick, icon: 'delete' },
                { line: true }
            ]
    });
    LoadGrid();
});

//加载列表
function LoadGrid() {
    $("#pageloading").show();
    Application.data.getRWState(callBack, this);
}
//回调函数
function callBack(jsonData, scope) {
    if (jsonData) {
        var pushData = eval('(' + jsonData + ')');
        var grid = $("#maingrid").ligerGrid({
            columns: [
                   { display: '编号', name: 'No', width: 380, align: 'left' },
                   { display: '类型', name: 'Name' }

                   ],
            pageSize: 20,
            data: pushData,
            rownumbers: true,
            height: "99%",
            width: "99%",
            columnWidth: 100,
            onReload: LoadGrid
        });
    }
    else {
        $.ligerDialog.warn('加载数据出错，请关闭后重试！');
    }
    $("#pageloading").hide();
}

//工具栏事件
function itemclick(item) {
    title = item.text + "状态";
    if (item.text == "新增") {
        $("#txtoptype").val('add');
        OpState("add");
    }
    else if (item.text == "修改") {
        $("#txtoptype").val('modify');
        OpState("modify");
    }
    else if (item.text == "删除") {
        $("#txtoptype").val('delete');
        OpState("delete");
    }
}
//状态基本操作
function OpState(obj) {
    var op = $("#txtoptype").val();
    $("#TB_No").attr("disabled", "");

    if (obj == "delete") {
        var grid = $("#maingrid").ligerGetGridManager();
        var row = grid.getSelectedRow();
        if (row) {
            $.ligerDialog.confirm('您确定要删除所选项吗？', function (yes) {
                if (yes) {
                    Application.data.opRWState(row.No, encodeURI(row.Name), op, function (js) {
                        if (js == "1") {
                            SetEmpty();
                            LoadGrid();
                            return;
                        }
                        else {
                            $.ligerDialog.warn('请重新操作！');
                        }
                    }, this);
                }
            })
        } else {
            $.ligerDialog.warn('请选择删除数据！');
            return;
        }
    }
    else {
        if (obj == "modify") {
            var grid = $("#maingrid").ligerGetGridManager();
            var row = grid.getSelectedRow();
            if (row) {
                $("#TB_No").val(row.No);
                $("#TB_No").attr("disabled", "disabled");
                $("#TB_Name").val(row.Name);
            } else {
                $.ligerDialog.warn('请选择修改数据！');
                return;
            }
        }
        $.ligerDialog.open({
            target: $("#stateInfo"),
            title: title,
            width: 380,
            height: 220,
            isResize: true,
            modal: true,
            buttons: [
        { text: '保存', onclick: function (i, d) {

            var no = $("#TB_No").val();
            var name = $("#TB_Name").val();
            if (name == "") {
                $.ligerDialog.warn('状态名称不能为空！');
                $("#TB_Name").focus();
                return;
            }
            name = encodeURI(name);
            Application.data.opRWState(no, name, op, function (js) {
                if (js == "1") {
                    SetEmpty();
                    d.hide();
                    LoadGrid();
                }
                else {
                    $.ligerDialog.warn('请重新操作！');
                }
            }, this);
        }
        }, { text: '关闭', onclick: function (i, d) {
            SetEmpty();
            d.hide();
        }
        }]
        });

    }

}
//清空 控件 
function SetEmpty() {
    $("#TB_No").val('');
    $("#TB_Name").val('');
    $("#txtoptype").val('');
}
