﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/BBS/BBSPage.Master" AutoEventWireup="true"
    CodeBehind="userImg.aspx.cs" Inherits="CCOA.App.BBS.userImg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="sorttag clearfix" style="margin-top: 8px;">
        <li class="ntop"></li>
        <li class="ncurr"><a href="userCenter.aspx">个人信息</a></li>
        <li><a href="userforum.aspx?act=f" class="nlast">主题</a></li>
        <li class="nbott"></li>
    </ul>
    <div class="threadtype userCenter">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="50" height="100">
                    &nbsp;
                </td>
                <td style="width: 100px; display: block; text-align: center;">
                    <div class="author_photo_in">
                        <span class="author_photo_bg"></span>
                        <img src="" width="81" height="81" onerror="this.src = 'images/default.gif'" runat="server"
                            id="userimg" />
                    </div>
                    现有头像
                </td>
                <td>
                    上传说明：可以上传.jpg .gif .jpeg的头像，大小15k，80*80像素最佳<br />
                    新上传：<asp:FileUpload ID="upload_Img" runat="server" />
                    <asp:Button ID="btn_upload" runat="server" Text="上传" OnClick="btn_upload_Click" /><br>
                    <asp:Label ID="lab_errMsg" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
