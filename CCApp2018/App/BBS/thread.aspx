﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/BBS/BBSPage.Master" AutoEventWireup="true"
    CodeBehind="thread.aspx.cs" Inherits="CCOA.App.BBS.thread" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var _FK_Class = "";
        var _PostType = "";

        function CallBack(js) {
            if (js != "true") {
                alert(js);
                return;
            }
            if (_PostType == "replay") {
                window.location.reload();
            } else if (_PostType == "motif") {
                window.location.href = "List.aspx?FK_Class=" + _FK_Class;
            }
        }

        function DelTarget(posttype, FK_Class, FK_Motif, FK_Replay) {
            _PostType = posttype;
            _FK_Class = FK_Class;
            if (confirm("确定要删除吗?")) {
                var param = {
                    posttype: posttype,
                    FK_Class: FK_Class,
                    FK_Motif: FK_Motif,
                    FK_Replay: FK_Replay
                };
                $.post("delpost.aspx", param, CallBack);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- page -->
    <div class="page page_whitebg" id="PageTop" runat="server">
        <input name="" type="button" class="but_new" value="" onmouseover="this.className = 'but_new but_new_h'"
            onmouseout="this.className = 'but_new'" />
        <input name="" type="button" class="but_reply" value="" onmouseover="this.className = 'but_reply but_reply_h'"
            onmouseout="this.className = 'but_reply'" />
        <span>总贴数：<font class="forange">1</font>篇</span> <span class="pgs"><a href="#">1</a><a
            class='pcurr'>2</a></span>
    </div>
    <!-- page end -->
    <!-- article  list -->
    <div class="article_box" id="top_lou" name="top_lou">
        <!-- 精华/置顶 图章-->
        <!-- 精华/置顶 图章-->
        <!-- authorinfo -->
        <asp:Localize ID="htm_fAuthor" runat="server" Text="&lt;div class=&quot;authorinfo&quot;&gt;&lt;/div&gt;"></asp:Localize>
        <!-- article text -->
        <div class="article">
            <h2>
                <asp:Localize ID="htm_fTitle" runat="server"></asp:Localize></h2>
            <div class="article_cnt">
                <asp:Localize ID="htm_fContent" runat="server" />
            </div>
        </div>
        <div style="float: left; width: 800px; position: relative; margin-left: 200px; margin-top: -20px;
            clear: both;" id="top_ma" name="top_ma">
            <div style="height: 25px; margin: 0px 25px 0px 25px; border-top: 1px dashed #e6e7e1;">
                <asp:Localize ID="Author_Manage" runat="server"></asp:Localize>
            </div>
        </div>
    </div>
    <!-- article  list -->
    <div class="article_box">
        <asp:DataList ID="datalistR" RepeatLayout="Flow" runat="server" Width="100%" ShowFooter="False"
            ShowHeader="False" RepeatDirection="Horizontal">
            <ItemTemplate>
                <div style="float: none; overflow: hidden; clear: both; padding-top: 2px; border-top: 5px solid #ddd;">
                    <!-- authorinfo -->
                    <%#GetUserInfo(Eval("FK_Emp").ToString()) %>
                    <!-- article text -->
                    <div class="article" id='lou<%#Eval("No") %>'>
                        <h3>
                            <span><font>发布时间：<%#Eval("AddTime")%></font> <em>[<%#Eval("Ranking")%>楼]</em> <a
                                href="#top_lou">TOP</a> </span>
                        </h3>
                        <div class="article_cnt" style="height: 100%; overflow: hidden;">
                            <%#FormatString(Eval("Contents").ToString())%>
                        </div>
                    </div>
                    <div style="float: left; width: 800px; position: relative; margin-left: 200px; margin-top: -18px;
                        clear: both;">
                        <div style="height: 25px; margin: 0px 25px 0px 25px; border-top: 1px dashed #e6e7e1;">
                            <%#GetAuthorPower(Eval("No").ToString(), "replay")%>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:DataList>
    </div>
    <!-- list end -->
    <!-- page -->
    <div class="page page_whitebg" id="PageDown" runat="server">
    </div>
    <!-- page end -->
</asp:Content>
