﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/BBS/BBSPage.Master" AutoEventWireup="true"
    CodeBehind="userforum.aspx.cs" Inherits="CCOA.App.BBS.userforum" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="sorttag clearfix" style="margin-top: 8px;">
        <li class="ntop"></li>
        <li><a href="userCenter.aspx?FK_Emp=<%=FK_Emp %>"
            class="nlast">个人信息</a></li>
        <li class="ncurr"><a href="">主题</a></li>
        <li class="nbott"></li>
    </ul>
    <div class="threadtype">
        <table align="center" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr class="header">
                    <th width="55%" class="partition">
                        标题
                    </th>
                    <th width="15%" class="partition">
                        版块
                    </th>
                    <th width="15%" class="partition">
                        查看/回复
                    </th>
                    <th width="15%" class="partition">
                        最后回复
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- 主题分类end -->
    <div class="threadlist">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <asp:DataList ID="datalistF" RepeatLayout="Flow" runat="server" Width="100%" ShowFooter="False"
                    ShowHeader="False" RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <tr class="hover">
                            <td width="55%">
                                <a href="thread.aspx?FK_Class=<%#Eval("FK_Class") %>&FK_Motif=<%#Eval("No") %>">
                                    <%#Eval("Name")%></a>
                            </td>
                            <td width="15%">
                                <%#GetClassName(Eval("FK_Class"))%>
                            </td>
                            <td width="15%">
                                <%#Eval("Hits")%>/<%#Eval("Replays")%>
                            </td>
                            <td width="15%">
                                <a href="userCenter.aspx?FK_Emp=<%#Eval("LastReEmp") %>">
                                    <%#Eval("LastReEmpText")%></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:DataList>
                <tr>
                    <td colspan="5">
                        <div class="fixsel" id="downHtml" runat="server" align="center">
                            <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                                runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                PageSize="10" PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                                CustomInfoTextAlign="Left" LayoutType="Table" OnPageChanged="AspNetPager1_PageChanged">
                            </webdiyer:AspNetPager>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
