﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.Web;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class BBSPage : System.Web.UI.MasterPage
    {
        string classid = StringFormat.GetQuerystring("FK_Class");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (htm_Nav.Text == "")
                {
                    htm_Nav.Text = "<a href=\"default.aspx\" class=\"ghome\">论坛首页</a>";
                    if (StringFormat.IsNumber(classid))
                    {
                        BBSClass selfClass = new BBSClass();
                        selfClass.Retrieve(BBSClassAttr.No, classid);

                        BBSClasss classs = new BBSClasss();
                        classs.RetrieveByAttr(BBSClassAttr.ParentNo, classid);

                        foreach (BBSClass item in classs)
                        {
                            htm_Nav.Text += " > <a href=\"List.aspx?FK_Class=" + item.No + "\" class=\"ghome\">" + item.Name + "</a>";
                        }

                        htm_Nav.Text += " > <a class=\"ghome\">" + selfClass.Name + "</a>";
                    }
                }
                //加载用户身份
                user_info.InnerHtml = "欢迎回来：<a href=\"userCenter.aspx?FK_Emp=" + WebUser.No + "\"><strong>" + WebUser.Name + "</strong></a>";
                BBSUserInfo bbsUser = new BBSUserInfo();
                bool isHave = bbsUser.RetrieveByAttr(BBSUserInfoAttr.FK_Emp, WebUser.No);
                if (!isHave)
                {
                    bbsUser = new BBSUserInfo();
                    bbsUser.FK_Emp = WebUser.No;
                    bbsUser.RegDate = DateTime.Now;
                    bbsUser.Insert();
                }
                //访问时间
                bbsUser.LastDate = DateTime.Now;
                bbsUser.Update();

                //增加配置信息
                BBSConfigs bbsConfigs = new BBSConfigs();
                bbsConfigs.RetrieveAll();
                if (bbsConfigs.Count == 0)
                {
                    BBSConfig config = new BBSConfig();
                    config.Insert();
                     config.RunSQL("UPDATE OA_BBSConfig SET OID=0");
                }
            }
        }
    }
}