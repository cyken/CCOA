﻿//处理接收人、抄送人的添加
//input 自动补全的=集合
var data = [];
$(function () {
    //input 框内容输入是弹出layer
    $(".js_input").bind('input propertychange', function () {
        var handler = new HttpHandler("BP.WF.HttpHandler.WF_WorkOpt");

        handler.AddPara("TB_Emps", $(this).val());
        data = handler.DoMethodReturnString("AccepterOfGener_SelectEmps");
        if (data.indexOf('err@') == 0) {
            alert(data);
            return;
        }
        data = JSON.parse(data);
        if (data.length == 0)
            return;
        var html = "";
        for (var i = 0; i < data.length; i++) {
            html = html + "<div class='item' id='" + data[i].No + "'>" + data[i].Name + "</div>";
        }
        var that = this;
        layer.tips(html, that, {
            tips: [3, 'rgb(111, 170, 244)'],
            time: 0
        });
        var inputFlag = this.id == "TB_Receiver" ? true : false;
        $(".item").bind("click", function () {
            if (inputFlag)
                $("#recevierDiv").sendMsgTag("loadData", [{ "No": this.id, "Name": $(this).html()}]);
            else
                $("#CCerDiv").sendMsgTag("loadData", [{ "No": this.id, "Name": $(this).html()}]);
            layer.closeAll();
        });
        $(this).val("");
    });

    //输入框按回车时的处理
    $(".js_input").bind('input keypress', function (event) {
        if (event.keyCode == "13") {
            //选择弹出tip的第一个值，如果存在选择第一个，否则查无此人设值为空
            if (data.length == 0)
                $(this).val("");
            //回显到div中
            else {
                var inputFlag = this.id == "TB_Receiver" ? true : false;
                if (inputFlag)
                    $("#recevierDiv").sendMsgTag("loadData", [data[0]]);
                else
                    $("#CCerDiv").sendMsgTag("loadData", [data[0]]);
                $(this).val("");
                layer.closeAll();
            }

        }

    });

    //bind resize事件
    $("#recevierDiv").bind('resize', function () {
        var padTop = $($(".div-group-addon")[0]).css("padding-top").replace("px", "");
        $($(".div-group-addon").parent()).css("margin-top", (parseInt(padTop) + 6) + "px");
        $($(".div-group-addon")[0]).css("padding-top", (parseInt(padTop) + 5) + "px");
        $($(".div-group-addon")[0]).css("padding-bottom", (parseInt(padTop) + 5) + "px");
    });
    $("#CCerDiv").bind('resize', function () {
        var padTop = $($(".div-group-addon")[0]).css("padding-top").replace("px", "");
        $($(".div-group-addon").parent()).css("margin-top", (parseInt(padTop) + 6) + "px");
        $($(".div-group-addon")[0]).css("padding-top", (parseInt(padTop) + 5) + "px");
        $($(".div-group-addon")[0]).css("padding-bottom", (parseInt(padTop) + 5) + "px");
    });

});

//弹出人员选择框
function SelectEmp(type, targetID) {
    var dept = getQueryStringByNameFromUrl("?" + $.cookie('CCS'), "FK_Dept");
    if (dept == null || dept == '' || dept == undefined) {
        dept = $.cookie('FK_Dept');
    }

    if (dept == null || dept == '' || dept == undefined) {
        var u = new WebUser();
        dept = u.FK_Dept;
    }

    if (dept == undefined) {
        dept = "0";
    }
    var url = "SelectEmps.htm?FK_Dept=" + dept + "&s=" + Math.random();
    var title = "选择发送人";
    if (type == "1")
        title = "选择抄送人";
    var iframeId = "SelectEmpIFrame";
    //打开人员选择页面
    OpenBootStrapModal(url, iframeId, title, 500, 400, "icon-edit", true, function () {
        var iframe = document.getElementById(iframeId);
        if (iframe) {
            var selectedEmps = iframe.contentWindow.returnVal;
            // type 0.接受者 1.抄送者
            if (type == 0) {
                $("#" + targetID).sendMsgTag("loadData", selectedEmps);

            } else {
                $("#" + targetID).sendMsgTag("loadData", selectedEmps);
            }
        }
    }, null, function () {

    }, "DIV_" + iframeId);

}

//关闭pop弹出框的处理
