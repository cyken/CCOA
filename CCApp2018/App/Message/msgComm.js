﻿//时间格式的处理
function dateFtt(fmt, date) { //author: meizz   
    var o = {
        "M+": date.getMonth() + 1,                 //月份   
        "d+": date.getDate(),                    //日   
        "h+": date.getHours(),                   //小时   
        "m+": date.getMinutes(),                 //分   
        "s+": date.getSeconds(),                 //秒   
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度   
        "S": date.getMilliseconds()             //毫秒   
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//获取星期的JS
function getWeekDay(dataString) {
    //判断是星期几
    var date = new Date(dataString);
    var weekDay = date.getDay();
    var weekDayString = "";
    if (weekDay == 1) {
        weekDayString = "星期一";
    } else if (weekDay == 2) {
        weekDayString = "星期二";
    } else if (weekDay == 3) {
        weekDayString = "星期三";
    } else if (weekDay == 4) {
        weekDayString = "星期四";
    } else if (weekDay == 5) {
        weekDayString = "星期五";
    } else if (weekDay == 6) {
        weekDayString = "星期六";
    } else if (weekDay == 7) {
        weekDayString = "星期日";
    }
    return weekDayString;
}


//msgBtn共同内容
function initBar(){
    var toolBar = $("#toolBar");
    var html = "";
    html += '<a style="display: inline-block; text-align: center" href="javascript:void(0);" onclick="changePage(0)">';
    html += '<span class="nav"><img style="" border="0" src="img/xieyoujian.png" alt="写邮件" onclick="skip(0)" /><br />写邮件</span>';
    html += '</a> ';
    html += '<a style="display: inline-block; text-align: center" class="1-link" href="javascript:void(0);" onclick="changePage(1)">';
    html += '<span class="nav"><img style="" border="0"  src="img/shoujianxiang.png" alt="收件箱" onclick="skip(1)" /><br /> 收件箱</span>';
    html += '</a> ';
    html += '<a style="display: inline-block; text-align: center" class="1-link" href="javascript:void(0);" onclick="changePage(2)">';
    html += '<span class="nav"><img style="" border="0"  src="img/fajianxiang.png" alt="发件箱" onclick="skip(2)" /><br />发件箱</span>';
    html += '</a>'; 
    html += '<a style="display: inline-block; text-align: center" class="1-link" href="javascript:void(0);" onclick="changePage(3)">';
    html += '<span class="nav"><img style="" border="0"  src="img/caogaoxiang.png" alt="草稿箱" onclick="skip(3)" /><br />草稿箱</span>';
    html += '</a>';
    html += '<a style="display: inline-block; text-align: center" class="1-link" href="javascript:void(0);" onclick="changePage(4)">';
    html += '<span class="nav"><img style="" border="0"  src="img/lajixiang.png" alt="垃圾箱" onclick="skip(4)" /><br />垃圾箱</span>';
    html += '</a>';
    toolBar.prepend(html);
}

function changePage(pageType) {
    if (pageType == 0)
        window.location.href = 'SendMsg.htm';
    if (pageType == 1)
        window.location.href = 'InBox.htm';
    if (pageType == 2)
        window.location.href = 'SendBox.htm';
    if (pageType == 3)
        window.location.href = 'DraftBox.htm';
    if (pageType == 4)
        window.location.href = 'RecycleBox.htm';
}

function skip(pageType) {
    if (pageType == 0)
        window.location.href = 'SendMsg.htm';
    if (pageType == 1)
        window.location.href = 'InBox.htm';
    if (pageType == 2)
        window.location.href = 'SendBox.htm';
    if (pageType == 3)
        window.location.href = 'DraftBox.htm';
    if (pageType == 4)
        window.location.href = 'RecycleBox.htm';
}

   
 