﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateVote.aspx.cs" Inherits="CCOA.App.Vote.CreateVote" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" charset="UTF-8" type="text/javascript"></script>
    <style type="text/css">
        .table td:first-child
        {
            text-align: right;
            width: 200px;
        }
        .table td:first-child span
        {
            margin-right: 50px;
        }
        input[type="text"], textarea
        {
            width: 500px;
        }
        textarea
        {
            height: 120px;
            margin-left: 0px;
        }
        .Validform_checktip
        {
            color: Red;
            margin-left: 10px;
        }
        .table td div
        {
            float: left;
        }
    </style>
    <script type="text/javascript">
        function openDia(e) {
            var diag = new Dialog();
            diag.Width = 630;
            diag.Height = 400;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = e;
            diag.OKEvent = function () {
                var ddl = diag.innerFrame.contentWindow.document.getElementById('lbRight');
                var resultName = "";
                var resultNo = "";
                for (i = 0; i < ddl.options.length; i++) {
                    resultName += ddl.options[i].text + ",";
                    resultNo += ddl.options[i].value + ",";
                }
                $id('VoteEmpName').value = resultName;
                $id("VoteEmpNo").value = resultNo;
                diag.close();
            };
            diag.show();
        }

        $(function () {
            $(".registerform").Validform({
                tiptype: 2,
                showAllError: true
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" action="CreateVote.aspx" method="post" target="iframe1"
    class="registerform">
    <table class="table">
        <tr>
            <td>
                <span><span style="color: Red; margin-right:5px;">*</span>投票主题</span>
            </td>
            <td>
                <div>
                    <input type="text" name="Title" id="txtTitle" value='<%=en==null?string.Empty:en.Title%>'
                        maxlength="50" datatype="s6-50" errormsg="昵称至少6个字符,最多18个字符！" nullmsg="投票主题不能为空！" />
                </div>
                <div>
                    <span class="Validform_checktip"></span>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <span><span style="color: Red; margin-right:5px;">*</span>投票项目</span>
            </td>
            <td>
                <div>
                    <div>
                        <textarea name="Item" id="txtItem" datatype="*" nullmsg="投票项不能为空！" <%=CheckMyType()%>><%=en == null ? string.Empty : en.Item%></textarea>
                    </div>
                    <div>
                        <span class="Validform_checktip"></span>
                    </div>
                </div>
                <div style="clear: both; margin: 10px 0px 10px 0px; color: Red;">
                    请输入投票项目不同的投票项目用回车换行表示
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <span>投票参与人</span>
            </td>
            <td style="text-align: left;">
                <textarea id="VoteEmpName" name="VoteEmpName" readonly="readonly"><%=en == null ? string.Empty : en.VoteEmpName%></textarea>
                
                <input type="button" value="添加投票人" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#VoteEmpNo','VoteEmpNo','VoteEmpName');" />
               
                <br />
                请选择投票人员，不选择默认为全体人员
            </td>
        </tr>
        <tr>
            <td>
                <span>投票起止日期</span>
            </td>
            <td style="text-align: left;">
                <p>
                    <span>从</span>
                    <input type="text" style="width: 150px" name="DateFrom" id="startDate" readonly="readonly"
                        value='<%=en==null?string.Empty:en.DateFrom%>' class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d',maxDate:'%y-%M-{%d+7}'})" />
                    <span>到 </span>
                    <input type="text" style="width: 150px" name="DateTo" id="endDate" readonly="readonly"
                        value='<%=en==null?string.Empty:en.DateTo%>' class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d',maxDate:'%y-%M-{%d+7}'})" />
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <span>投票属性</span>
            </td>
            <td style="text-align: left;">
                <input type="radio" value="0" name="voteType" <%=en==null?string.Empty:CheckType1(en.VoteType)%>
                    id="radiosingle" />单投
                <input type="radio" value="1" name="voteType" id="radiomany" <%=en==null?string.Empty:CheckType2(en.VoteType)%> />多投
                <input type="checkbox" name="IsAnonymous" value="1" <%=en==null?string.Empty:Check(en.IsAnonymous)%>
                    id="cbIsAnon" />允许匿名
                <input type="checkbox" name="IsEnable" value="1" <%=en==null?string.Empty:Check(en.IsEnable)%>
                    id="cbIsEnable" />是否启用
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="text-align: left;">
                <input type="submit" value="保存" />
                <input type="reset" value="重置" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="CreateDate" />
    <input type="hidden" name="FK_Emp" value="<%=UserNo%>" />
    <input type="hidden" value="<%=en==null?string.Empty:en.VoteEmpNo%>" id="VoteEmpNo"
        name="VoteEmpNo" />
    <input type="hidden" name="OID" value="" runat="server" id="hOid" />
    </form>
    <iframe id="iframe1" name="iframe1" style="display: none" />
</body>
</html>
