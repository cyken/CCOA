﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="ViewPlanCatalog.aspx.cs" ValidateRequest="false" ClientIDMode="Static"
    Inherits="CCOA.App.PublicPlan.ViewPlanCatalog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 5px;
            display: table;
            border-color: gray;
            width: 99%;
        }
        .doc-tableth
        {
            background: #EEE;
        }
        .doc-tabletd
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 100px;
            text-align: right;
        }
        td.title3
        {
            width: 100px;
            text-align: right;
        }
        td.text
        {
            text-align: left;
            width: 260px;
            padding-left: 10px;
        }
    </style>
    <script src="../../js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/plugins/jquery.datebox.js" type="text/javascript"></script>
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="../../ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script src="js/AppData.js" type="text/javascript"></script>
    <script src="js/ViewPlanCatalog.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="margin: 5px 5px 5px 5px; width: 760px;">
        <table class="doc-table">
            <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                <img src="../../Images/Btn/Insert.gif" style="width: 18px; height: 18px;" />反馈情况</caption>
            <tr>
                <td class="doc-tabletd title">
                    <span style="color: Red;">*</span>&nbsp;计划类型:
                </td>
                <td class="doc-tabletd text">
                    &nbsp;<asp:Label ID="ui_PCModel" runat="server" Width="60px"></asp:Label>
                </td>
                <td class="doc-tabletd title3">
                    <span style="color: Red;">*</span>&nbsp;完成进度:
                </td>
                <td class="doc-tabletd text">
                    &nbsp;<asp:Label ID="ui_FinishPercent" runat="server" Width="60px"></asp:Label>%
                </td>
            </tr>
            <tr>
                <td class="doc-tabletd title">
                    <span style="color: Red;">*</span>&nbsp;计划名称:
                </td>
                <td class="doc-tabletd text" colspan="3">
                    &nbsp;<asp:Label ID="ui_Name" runat="server" Width="90%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="doc-tabletd  title">
                    负责部门:
                </td>
                <td class="doc-tabletd text">
                    &nbsp;<asp:Label ID="ui_FK_DeptText" runat="server" Width="90%"></asp:Label>
                </td>
                <td class="doc-tabletd  title3">
                    负责人:
                </td>
                <td class="doc-tabletd text">
                    &nbsp;<asp:Label ID="ui_PersonCharge" runat="server" Width="90%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="doc-tabletd title">
                    参与人员:
                </td>
                <td class="doc-tabletd text" colspan="3">
                    &nbsp;<asp:Label ID="ui_PlanPartake" runat="server" Width="85%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="doc-tabletd title">
                    <span style="color: Red;">*</span>&nbsp;开始时间:
                </td>
                <td class="doc-tabletd text">
                    &nbsp;<asp:Label ID="ui_StartDate" runat="server" Width="90%"></asp:Label>
                </td>
                <td class="doc-tabletd title3">
                    <span style="color: Red;">*</span>&nbsp;结束时间:
                </td>
                <td class="doc-tabletd text">
                    &nbsp;<asp:Label ID="ui_EndDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="doc-tabletd title">
                    反馈情况:
                </td>
                <td class="doc-tabletd text" colspan="3" style="height: 300px;">
                    <table id="planTickGrid" fit="true" fitcolumns="true" class="easyui-datagrid">
                    </table>
                </td>
            </tr>
        </table>
        <div style="text-align: center; margin-top: 5px;">
            <asp:HiddenField ID="ui_PK_NO" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="ui_IsTick" Value="0" runat="server"></asp:HiddenField>
        </div>
    </div>
</asp:Content>
