﻿//当前节点编号
var curNodeNo = "";

var commonWin = null;
//点击超链接，防止获取不到值
function ShowEditNode(PlanNo) {
    $('#planMianGrid').treegrid('select', PlanNo);
    EditNode();
}
function WindowClose() {
    if (commonWin) commonWin.window('close');
}
function WindowOpen(title, url) {
    AddTab(title, url);
}

//关闭弹出层dialogEnPanel
function Win_Close_Dialog() {
    $('#dialogEnPanel').dialog("close");
}

function ShowDialog(url) {
    var winWidth = document.body.clientWidth;
    //计算显示宽度
    winWidth = winWidth * 0.96;
    if (winWidth > 900) winWidth = 900;

    var winheight = document.body.clientHeight;
    //计算显示高度
    winheight = winheight * 0.98
    if (winheight > 680) winheight = 680;
    $("<div id='dialogEnPanel'></div>").append($("<iframe id='iframeDialog' width='100%' height='100%' frameborder=0 src='" + url + "'/>")).dialog({
        title: "窗口",
        width: winWidth,
        height: winheight,
        autoOpen: true,
        modal: true,
        resizable: true,
        onClose: function () {
            var iframe = document.getElementById("iframeDialog");
            if (iframe) {
                iframe.src = "";
            }
            $("#dialogEnPanel").remove();
            LoadGrid();
        },
        buttons: [{
            text: '关闭',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#dialogEnPanel').dialog("close");
            }
        }]
    });
}

//加载列表
function LoadGrid() {
    var curRootNo = $("#curRootNo").val();
    var treeNo = $("#curTreeNo").val();
    var cb_Catalog = document.getElementById("CB_Catalog").checked;
    var params = {
        method: "getplanmainapps",
        ParentNo: curRootNo,
        TreeNo: treeNo,
        showCatalog: cb_Catalog
    };
    queryData(params, function (js, scope) {
        $("#pageloading").hide();
        if (js) {
            if (js == "") js = "[]";
            //系统错误
            if (js.status && js.status == 500) {
                $("body").html("<b style='color:red;'>返回数据出错。<b>");
                return;
            }

            var pushData = eval('(' + js + ')');
            $('#planMianGrid').treegrid({
                data: pushData,
                idField: 'No',
                treeField: 'Name',
                animate: true,
                fitColumns: true,
                loadMsg: '数据加载中......',
                columns: [[
                   { field: 'Fin', title: '状态', align: 'center', width: 50, formatter: function (value, rowData, rowIndex) {
                       if (rowData.ParentNo == "0") return "";
                       //默认未启动
                       var imgSrc = "js/cdr.png";
                       var imgTip = "未启动";
                       if (rowData.PlanState == "1") {
                           imgSrc = "js/cdr_start.png";
                           imgTip = "进行中";
                       } else if (rowData.PlanState == "2") {
                           imgSrc = "js/cdr_stop.png";
                           imgTip = "已结束";
                       }
                       return "<img title='" + imgTip + "' src='" + imgSrc + "' align='middle'/>";
                   }
                   },
                   { field: 'Name', title: '名称', width: 280, formatter: function (value, rowData, rowIndex) {
                       return rowData.Name
                       //if (rowData.ParentNo == "0") return rowData.Name
                       //return "<a href='javascript:void(0)' onclick=ShowEditNode('" + rowData.No + "');>" + rowData.Name + "</a>";
                   }
                   },
                   { field: 'FK_DeptText', title: '负责部门', width: 120 },
                   { field: 'PersonChargeText', title: '负责人', width: 100 },
                   { field: 'StartDate', title: '开始时间', width: 100 },
                   { field: 'EndDate', title: '结束时间', width: 100 },
                   { field: 'FinishPercent', title: '已完成(%)', align: 'center', width: 60 },
                   { field: 'PlanPartakeText', title: '参与人', width: 200 }
                   ]],
                onContextMenu: function (e, node) {
                    e.preventDefault();
                    $('#planMianGrid').treegrid('select', node.No);
                    SetPlanMenuSate(node.No);
                    $('#mm').menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                },
                onClickRow: function (row) {
                    curNodeNo = row.No;
                    SetPlanMenuSate(curNodeNo);
                },
                onDblClickCell: function (index, field, value) {
                    if (field.ParentNo != "0") {
                        ViewPlanBisc();
                    }
                },
                onBeforeExpand: function (row) {

                }
            });
            if (curNodeNo != "" && curNodeNo != null) {
                ExpendToNode(curNodeNo);
            }
        }
    });
}
//非负责人权限
function UnChargeMenu() {
    var item = $('#mm').menu('findItem', '反馈情况');
    $('#mm').menu('disableItem', item.target);
    item = $('#mm').menu('findItem', '新建');
    $('#mm').menu('disableItem', item.target);
    item = $('#mm').menu('findItem', '编辑');
    $('#mm').menu('disableItem', item.target);
    item = $('#mm').menu('findItem', '删除');
    $('#mm').menu('disableItem', item.target);
    item = $('#mm').menu('findItem', '状态控制');
    $('#mm').menu('disableItem', item.target);
    item = $('#mm').menu('findItem', '上移');
    $('#mm').menu('disableItem', item.target);
    item = $('#mm').menu('findItem', '下移');
    $('#mm').menu('disableItem', item.target);
    item = $('#mm').menu('findItem', '查看');
    $('#mm').menu('enableItem', item.target);
}
//负责人权限
function ChargeMenu() {
    var item = $('#mm').menu('findItem', '反馈情况');
    $('#mm').menu('enableItem', item.target);
    item = $('#mm').menu('findItem', '新建');
    $('#mm').menu('enableItem', item.target);
    item = $('#mm').menu('findItem', '编辑');
    $('#mm').menu('enableItem', item.target);
    item = $('#mm').menu('findItem', '删除');
    $('#mm').menu('enableItem', item.target);
    item = $('#mm').menu('findItem', '状态控制');
    $('#mm').menu('enableItem', item.target);
    item = $('#mm').menu('findItem', '上移');
    $('#mm').menu('enableItem', item.target);
    item = $('#mm').menu('findItem', '下移');
    $('#mm').menu('enableItem', item.target);
    item = $('#mm').menu('findItem', '查看');
    $('#mm').menu('enableItem', item.target);
}

//状态
function SetPlanMenuSate(planNo) {
    ChargeMenu();
    UnChargeMenu();
    var row = $('#planMianGrid').treegrid('find', planNo);
    //根节点控制按钮权限
    if (row.ParentNo != "0") {
        var curUserNo = $("#curPerson").val();
        //参与人
        var planPartake = row.PlanPartake + ',';
        var isPartake = planPartake.indexOf(curUserNo + ",");
        //负责人
        if (row.PersonCharge == curUserNo || curUserNo == "admin") {
            ChargeMenu();
            //可用
            var item = $('#mm').menu('findItem', '同级计划');
            $('#mm').menu('enableItem', item.target);
            item = $('#mm').menu('findItem', '阶段');
            $('#mm').menu('enableItem', item.target);
        } else if (isPartake > -1) { //是参与人
            var item = $('#mm').menu('findItem', '新建');
            $('#mm').menu('enableItem', item.target);
            //不可用
            item = $('#mm').menu('findItem', '同级计划');
            $('#mm').menu('disableItem', item.target);
            item = $('#mm').menu('findItem', '阶段');
            $('#mm').menu('disableItem', item.target);
        }
    } else {
        var item = $('#mm').menu('findItem', '新建');
        $('#mm').menu('enableItem', item.target);
        //不可用
        item = $('#mm').menu('findItem', '同级计划');
        $('#mm').menu('disableItem', item.target);
        item = $('#mm').menu('findItem', '阶段');
        $('#mm').menu('disableItem', item.target);
        item = $('#mm').menu('findItem', '编辑');
        $('#mm').menu('enableItem', item.target);
        item = $('#mm').menu('findItem', '查看');
        $('#mm').menu('disableItem', item.target);
    }

    //启动或结束按钮控制
    var planState = row.PlanState;
    var itemStart = $('#mm').menu('findItem', '启动');
    var itemEnd = $('#mm').menu('findItem', '结束');
    $('#mm').menu('disableItem', itemStart.target);
    $('#mm').menu('disableItem', itemEnd.target);
    //未启动
    if (planState == "0") {
        $('#mm').menu('enableItem', itemStart.target);
        $('#mm').menu('disableItem', itemEnd.target);
    } else if (planState == "1") {
        $('#mm').menu('disableItem', itemStart.target);
        $('#mm').menu('enableItem', itemEnd.target);
    }
}

//启动计划
function StartUpPlan() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        var params = {
            method: "sartupplan",
            nodeNo: curNodeNo
        };
        queryData(params, function (js, scope) {
            if (js) {
                if (js == "no") {
                    $.messager.alert('提示:', '父计划还未启动，所选项不能启动！', 'info');
                    return;
                }
                if (js == "notpersoncharge") {
                    $.messager.alert('提示:', '不是当前计划负责人，所选项不能启动！', 'info');
                    return;
                }
                //刷新列表
                LoadGrid();
                $.messager.alert('提示:', '计划成功启动！', 'info');
            }
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//结束计划
function ShutUpPlan() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        var params = {
            method: "shutdownplan",
            nodeNo: curNodeNo
        };
        queryData(params, function (js, scope) {
            if (js == "no") {
                $.messager.alert('提示:', '子计划或阶段还有未结束项，请先结束子计划或阶段！', 'info');
                return;
            }
            if (js == "notpersoncharge") {
                $.messager.alert('提示:', '不是当前计划负责人，所选项不能结束！', 'info');
                return;
            }
            //刷新列表
            LoadGrid();
            $.messager.alert('提示:', '计划成功结束！', 'info');
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//查看基本信息
function ViewPlanBisc() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        var curUserNo = $("#curPerson").val();
        //参与人
        var planPartake = row.PlanPartake + ',';
        var isPartake = planPartake.indexOf(curUserNo + ",");
        //负责人、参与人
        if (row.PersonCharge == curUserNo || curUserNo == "admin" || isPartake > -1) {
            WindowOpen("基本信息", "/App/PublicPlan/ViewPlan.aspx?PK=" + curNodeNo);
        } else {
            //判断是否上级负责人
            var params = {
                method: "getparentplanpartake",
                nodeNo: curNodeNo
            };
            queryData(params, function (js, scope) {
                if (js) {
                    if (js == "false") {
                        $.messager.alert('提示:', '没有权限查看！', 'info');
                        return;
                    }
                    WindowOpen("基本信息", "/App/PublicPlan/ViewPlan.aspx?PK=" + curNodeNo);
                }
            });
        }
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//查看进度
function PlanProgress() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        AddTab("进度监控", "/App/PublicPlan/GanttPlans.aspx?ParentNo=" + row.ParentNo);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}

//返回上一层
function BackPrePlan() {
    var rootRow = $('#planMianGrid').treegrid('getRoot');
    if (rootRow) {
        //获取上一层数据
        var params = {
            method: "backpreplan",
            nodeNo: rootRow.ParentNo
        };
        queryData(params, function (js, scope) {
            if (js) {
                var pushData = eval('(' + js + ')');
                //查询
                $("#curRootNo").val(pushData.ParentNo);
                $("#curTreeNo").val(pushData.TreeNo);
                LoadGrid();
                if (pushData.ParentNo == "0") {
                    alert('已经返回到最顶层！');
                }
            } else {
                $.messager.alert('提示:', '没有获取到上一层计划或已经是根目录，请刷新页面！', 'info');
            }
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//置顶显示
function SetToTopView() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        $("#curRootNo").val(row.ParentNo);
        $("#curTreeNo").val(row.TreeNo);
        LoadGrid();
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//新增同级
function AddSampleNode() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        if (row.ParentNo == "0") {
            $.messager.alert('提示:', '当前菜单不允许新建同级！', 'info');
            return;
        }
        if (row.PCModel == "1") {
            $.messager.alert('提示:', '阶段不允许创建！', 'info');
            return;
        }
        NodeManage(row.No, "sample", function (js, scop) {
            if (js == "notpersoncharge") {
                $.messager.alert('提示:', '您不是当前计划负责人或参与人，不允许新建！', 'info');
                return;
            }
            LoadGrid();
            ExpendToNode(curNodeNo);
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//新增子级
function AddChildNode() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        if (row.PCModel == "1") {
            $.messager.alert('提示:', '阶段下不允许创建子级！', 'info');
            return;
        }
        NodeManage(row.No, "children", function (js, scop) {
            if (js == "notpersoncharge") {
                $.messager.alert('提示:', '您不是当前计划负责人或参与人，不允许新建！', 'info');
                return;
            }
            LoadGrid();
            ExpendToNode(curNodeNo);
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//添加阶段
function AddStageNode() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        if (row.ParentNo == "0") {
            $.messager.alert('提示:', '当前菜单不允许新建阶段！', 'info');
            return;
        }
        if (row.PCModel == "1") {
            $.messager.alert('提示:', '阶段下不允许创建阶段！', 'info');
            return;
        }
        NodeManage(curNodeNo, "children_stage", function (js, scop) {
            if (js == "notpersoncharge") {
                $.messager.alert('提示:', '您不是当前计划负责人或参与人，不允许添加！', 'info');
                return;
            }
            document.getElementById("CB_Catalog").checked = true;
            LoadGrid();
            ExpendToNode(curNodeNo);
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//上移
function TranUp() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        if (row.ParentNo == "0") {
            $.messager.alert('提示:', '根节点不允许操作！', 'info');
            return;
        }
        NodeManage(row.No, "doup", function (js, scop) {
            curNodeNo = row.No;
            if (js == "notpersoncharge") {
                $.messager.alert('提示:', '您没有权限移动此节点！', 'info');
                return;
            }
            LoadGrid();
            ExpendToNode(curNodeNo);
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//下移
function TranDown() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        if (row.ParentNo == "0") {
            $.messager.alert('提示:', '根节点不允许操作！', 'info');
            return;
        }
        NodeManage(row.No, "dodown", function (js, scop) {
            curNodeNo = row.No;
            if (js == "notpersoncharge") {
                $.messager.alert('提示:', '您没有权限移动此节点！', 'info');
                return;
            }
            LoadGrid();
            ExpendToNode(curNodeNo);
        }, this);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//删除
function DelNode() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        if (row.ParentNo == "0") {
            $.messager.alert('提示:', '根节点不允许操作！', 'info');
            return;
        }
        var msg = "您确定删除所选项？";
        var isLeaf = 0;
        if (row.children) isLeaf = row.children.length;
        if (isLeaf > 0) {
            $.messager.alert('提示:', '所选项包含子项，不允许删除！', 'info');
            return;
        }
        //消息提醒
        $.messager.confirm('提示', msg, function (r) {
            if (r) {
                curNodeNo = row.No;
                NodeManage(row.No, "delete", function (js, scop) {
                    if (js == "notpersoncharge") {
                        $.messager.alert('提示:', '您不是当前计划负责人，不允许删除！', 'info');
                        return;
                    } else if (js == "isstartup") {
                        $.messager.alert('提示:', '计划已启动，不允许删除！', 'info');
                        return;
                    } else if (js == "isend") {
                        $.messager.alert('提示:', '计划已结束，不允许删除！', 'info');
                        return;
                    } else if (js == "havechild") {
                        $.messager.alert('提示:', '所选项包含子项，请先删除子项然后再试！', 'info');
                        return;
                    }
                    $('#planMianGrid').treegrid('remove', curNodeNo);
                    curNodeNo = null;
                }, this);
            }
        });
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//修改
function EditNode() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        if (row.ParentNo == "0") {
            $.messager.prompt('信息', '输入计划根节点名称：', function (r) {
                if (r) {
                    var params = {
                        method: "updaterootname",
                        nodeNo: curNodeNo,
                        rootName: encodeURI(r)
                    };
                    queryData(params, function (js, scope) {
                        LoadGrid();
                    }, this);
                }
            });
            return;
        }
        var url = "NewPlanCatalog.aspx?PK=" + curNodeNo;
        ShowDialog(url);
    } else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}

//展开到或选中行
function ExpendToNode(NodeNo) {
    var node = $('#planMianGrid').treegrid('find', NodeNo);
    if (node) {
        $('#planMianGrid').treegrid('expandTo', NodeNo);
        $('#planMianGrid').treegrid('select', NodeNo);
    }
}

//编辑反馈情况
function EditPlanTick() {
    var row = $('#planMianGrid').treegrid('getSelected');
    if (row) {
        curNodeNo = row.No;
        if (row.ParentNo == "0") {
            $.messager.alert('提示:', '根节点不允许操作！', 'info');
            return;
        }
        if (row.PlanState == "0") {
            $.messager.alert('提示:', '计划还未启动！', 'info');
            return;
        }
        var url = "/App/PublicPlan/ViewPlanCatalog.aspx?PK=" + curNodeNo;
        WindowOpen("反馈情况", url);
    } else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}

//操作节点
function NodeManage(nodeNo, dowhat, callback, scope) {
    var params = {
        method: "plannodemanage",
        nodeNo: nodeNo,
        dowhat: dowhat
    };
    queryData(params, callback, this);
}

//获取当前登录人
function LoadCurLoginUserNo() {
    var params = {
        method: "loadcurloginuserno"
    };
    queryData(params, function (js, scope) {
        $("#curPerson").val(js);
    }, this);
}

//初始页面
$(function () {
    //$('.easyui-layout').layout('collapse', 'south');
    var parentNo = getArgsFromHref("ParentNo");
    if (parentNo) {
        $("#curRootNo").val(parentNo);
    }
    //获取当前用户
    LoadCurLoginUserNo();
    LoadGrid();
});